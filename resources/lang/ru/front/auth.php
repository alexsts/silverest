<?php

return [
    'login' => 'Войти',
    'home' => 'Главная',
    'registered_customers' => 'ЗАРЕГИСТРИРОВАННЫЕ ПОЛЬЗОВАТЕЛИ',
    'if_account' => 'Пожалуйста введите свои данные.',
    'email' => 'Email Адрес',
    'password' => 'Пароль',
    'remember' => 'Запомнить меня',
    'login' => 'Войти',
    'forgot' => 'Забыли Пароль',
    'create' => 'Создать Аккаунт',

    'new_customers' => 'НОВЫЕ ПОЛЬЗОВАТЕЛИ',
    'register' => 'Регистрация',
    'f_name' => 'Имя',
    'l_name' => 'Фамилия',
    'confirm_password' => 'Подтвердить пароль',
    'clear' => 'Очистить',
    'already_account' => 'Уже есть аккаунт',

    'reset_password' => 'Сбросить Пароль',
    'reset_password_up' => 'СБРОСИТЬ ПАРОЛЬ',
    'if_send' => 'Если вы забыли пароль, мы вышлем письмо для сброса пароля',
    'send' => 'Послать письмо',
    'back_to_login' => 'Обратно к входу',

    'reset_the_password' => 'СБРОСИТЬ ПАРОЛЬ',
    'email_here' => 'Email адрес',
    'please_new_password' => 'Пожалуйста введите новые пароль',
];
