<?php

return [
    'wishlist' => 'Избранное',
    'home' => 'Главная',
    'shopping_cart' => 'Корзина',
    'checkout' => 'Посмотреть',
    'order_complete' => 'Заказ Оформлен',
    'product' => 'Изделие',
    'price' => 'Цена',
    'stock_status' => 'В Наличии',
    'add_cart' => 'Добавить в Корзину',
    'remove' => 'Убрать',
    'gem' => 'Камень',
    'size' => 'Размер',
    'back' => 'Назад',
    'next' => 'Вперед',
    'is_empty' => 'Избранное Пусто',
];
