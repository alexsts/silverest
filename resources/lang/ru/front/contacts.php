<?php

return [
    'home' => 'Главная',
    'our_location' => 'Наше местоположение',
    'we_located' => 'Мы находимся в торговом центре Fama Keskus',
    'contacts' => 'Контакты',
    'contacts_1' => 'Компания:',
    'contacts_2' => 'OÜ "SILVEREST" reg.n.11003444',
    'contacts_3' => 'Адрес:',
    'contacts_4' => 'Narva Tallinna mnt 19c FAMA KESKUS, Ida-Virumaa, 20303, Estonia, Phone mob. 59020001, Phone / Fax (35) 62620',
    'contacts_5' => 'Email Адрес:',
    'contacts_6' => 'silverest@silverest.ee',
    'contacts_7' => 'Банковские реквизиты:',
    'contacts_8' => 'Swedbank Eesti, 221024580301, IBAN: EE722200221024580301',
];
