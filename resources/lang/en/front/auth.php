<?php

return [
    'login' => 'Login',
    'home' => 'Home',
    'registered_customers' => 'REGISTERED CUSTOMERS',
    'if_account' => 'If you have an account with us, Please log in.',
    'email' => 'Email Address',
    'password' => 'Password',
    'remember' => 'Remember Me',
    'login' => 'Login',
    'forgot' => 'Forgot Your Password',
    'create' => 'Create an Account',

    'new_customers' => 'NEW CUSTOMERS',
    'register' => 'Register',
    'f_name' => 'First Name',
    'l_name' => 'Last Name',
    'confirm_password' => 'Confirm Password',
    'clear' => 'Clear',
    'already_account' => 'Already have an Account',

    'reset_password' => 'Reset Password',
    'reset_password_up' => 'RESET PASSWORD',
    'if_send' => 'If you forgot password, send a reset email',
    'send' => 'Send Reset Link',
    'back_to_login' => 'Back to Login',

    'reset_the_password' => 'RESET THE PASSWORD',
    'email_here' => 'Email address here',
    'please_new_password' => 'Please type new Password',
];
