<?php

return [
    'home' => 'Home',
    'cart' => 'Cart',
    'shopping_cart' => 'Shopping cart',
    'wishlist' => 'Wishlist',
    'checkout' => 'Checkout',
    'order_complete' => 'Order Complete',
    'product' => 'Product',
    'price' => 'Price',
    'quantity' => 'Quantity',
    'total' => 'Total',
    'remove' => 'Remove',
    'gem' => 'Gem',
    'size' => 'Size',
    'cart_details' => 'Cart Details',
    'vat_already' => 'Vat Already Included In Price',
    'cart_subtotal' => 'Cart Subtotal',
    'next' => 'Next',
    'shopping_cart_is_empty' => 'Shopping Cart is Empty',
    'next' => 'Next',
    'material' => 'Material',
    'gems' => 'Gems',
    'weight' => 'Weight',
    'grams' => 'grams',
    'add_to_cart' => 'Add to Cart',
    'sizes' => 'Sizes'
];
