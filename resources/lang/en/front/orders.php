<?php

return [
    'back' => 'Back',
    'home' => 'Home',
    'account_dashboard' => 'Account Dashboard',
    'orders' => 'Orders',
    'order' => 'Order',
    'gem' => 'Gem',
    'size' => 'Size',
    'quantity' => 'Quantity',
    'there_are_no_products' => 'There are no products in this order',
    'show_more_details' => 'Show More Details',
    'download_invoice' => 'Download Invoice',
    'extra_notes' => 'Extra Notes',
    'cancel_order' => 'Cancel Order',
    'no_orders' => 'You don\'t have any orders.',
    'please_leave' => 'Please leave any extra instructions regarding your order',
    'submit' => 'Submit'
];
