<?php

return [
    'home' => 'Pealeht',
    'warranty' => 'Garantii',
    'warranty_1' => 'Kvaliteedi garantii. Ümbervahetamise garantii!',
    'warranty_2' => 'Riskivaba ostmine!',
    'warranty_3' => 'Garanteerime oma 925 prooviga hõbeehetele kõrge kvaliteedi. Ostes meie tooteid, võite täiel määral kasutada oma õigusi:',
    'warranty_4' => '1. 14 päeva jooksul on Teil õigus toode tagastada või ümber vahetada.',
    'warranty_5' => '2. Anname oma toodetele 1 aastase garantii.',
    'warranty_6' => 'Hinnad',
    'warranty_7' => 'Kõik toodud hinnad on kehtivad ainult meie internetipoes, hinnale lisandub saatmiskulu. E-poest ostes teatatakse toote koguhind tellimuse käigus («kassas»). Kõigile meie e-poes registreerunud ostjatele kehtib tellimuse summast 2,5% hinnaalandus. Lisasoodustused hakkavad kliendile kehtima automaatselt vastavalt kliendistaatuse omandamisele.',
    'warranty_8' => 'Kauba pilt',
    'warranty_9' => 'Ekraanil võivad olla 925 toodet, mis on osaliselt laienenud. Iga kalliskivi ainulaadne - nii võib see pildil võrreldes tootega erineda mõnevõrra. Saadaval olev võrgutoode võib värvi, pinna, suuruse jms osas esialgse toote vormis erineda.',
    'warranty_10' => 'Sissejuhatus välistamistingimustest',
    'warranty_11' => 'Soovime, et teie tooted oleksid rahul. Kui sulle ehteid ei meeldi, saate seda 14 päeva jooksul tagastada või vahetada mis tahes muu kaunistusega.',
    'warranty_12' => 'Tagastamisreeglid',
    'warranty_13' => '1. Kaubad tuleb tagastada pakendis, mis hoiab ära kahju.',
    'warranty_14' => '2. Toode peab olema samas seisukorras nagu ostmise hetkel. Kui toote juures on märke, et ostja on selle kahjustanud (kriimustused, mehaanilised kahjustused jne), ei tagastata kaupa.',
    'warranty_15' => '3. Ostja makstud postikulu tagasisaatmisel.',
    'warranty_16' => '4. Ostja osutatud arvelduspangas loetletud tagastatud kaupade raha hiljemalt 14 päeva jooksul pärast ostjaga lepingu lõpetamist.',
    'warranty_17' => 'Vastutus',
    'warranty_18' => 'Me ei saa garanteerida, et kogu veebikauplus on laos. Esitatava toote veebisait meie veebisaidil www.silverest.ee võib toote värvi, pinna, suuruse jms kohta erineda. Me ei vastuta kaupade sobimatu kasutamise eest.'
];
