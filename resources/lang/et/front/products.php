<?php

return [
    'home' => 'Koduleht',
    'catalogue' => 'Kataloog',
    'sort_by' => 'Sorteeri',
    'sort_by_compact'=>'Sort ',
    'new' => 'uudise järgi',
    'popular' => 'populaarsuse järgi',
    'price_up' => 'kasvav hind',
    'price_down' => 'langev hind',
    'search_here' => 'Tooteotsing',
    'reset_filters' => 'Taasta filtrid',
    'color' => 'Värv',
    'style' => 'Stiil',
    'gem' => 'Kalliskivi',
    'price' => 'Hind',
    'categories' => 'Kategooriad',
    'all' => 'Kõik',
    'colors' => 'Värvid',
    'gems' => 'Kalliskivid',
    'styles' => 'Stiilid',
    'edit_product' => 'Edit Product',
    'your_range' => 'Hind',
    'add_price' => 'Sorteeri hinna järgi',
    'show_all' => 'Näita Kõiki',
    'recent_products' => 'Viimased tooted',
    'add_to_wishlist' => 'Lisa Lemmikutele',
    'add_to_cart' => 'Lisa Ostukorvi',
    'no_results' => 'Teie otsingule ei leitud midagi',
    'bestseller' => 'Parimad tooted',
    'discount' => 'Soodsad tooted'
];
