<?php

return [
    'home' => 'Koduleht',
    'my_account' => 'Minu Konto',
    'account_dashboard' => 'Kasutaja kabinett',
    'orders' => 'MINU TELLIMUSED',
    'no_orders' => 'EI OLE TELLIMUSI',
    'click_to_see_orders' => 'Tellimuste ajaloo vaatamiseks klõpsake',
    'click_to_see_cart' => 'Klõpsa ostukorvi vaatamiseks',
    'click_to_see_wishlist' => 'Vajuta, et näha sooviloendeid',
    'edit_account' => 'KONTO ANDMED MUUTMINE',
    'click_to_edit' => 'Klõpsake oma andmete ja aadressi muutmiseks',
    'cart' => 'OSTUKORV',
    'cart_is_empty' => 'OSTUKORV ON TÜHI',
    'wishlist' => 'LEMMIKUD',
    'wishlist_empty' => 'LEMMIKUD ON TÜHI',
];
