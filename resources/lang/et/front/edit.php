<?php

return [
    'home' => 'Koduleht',
    'my_account' => 'Minu Konto',
    'personal_details' => 'Isiklik Andmed',
    'first_name' => 'Nimi',
    'last_name' => 'Perekonnanimi',
    'email_address' => 'E-posti Aadress',
    'company_name' => 'Firma nimi',
    'optional' => 'Vabatahtlik',
    'phone_number' => 'Telefoninumber',
    'dob' => 'Sünniaeg',
    'delivery_address' => 'Saatmisaadress',
    'find_address' => 'Otsi aadressi või selle käsitsi sisestada:',
    'start_typing' => 'Alusta aadressi kirjutamist...',
    'address_line' => 'Aadressirida',
    'country' => 'Riik',
    'state_region' => 'Piirkond',
    'city' => 'Linn',
    'zip' => 'ZIP (Postiindeks)',
    'change_password' => 'Muuda parool',
    'new_password' => 'Uus parool',
    'confirm_password' => 'Kinnita parool',
    'clear_fields' => 'Kustuta väljad',
    'update_account' => 'Uuenda kontot',
];
