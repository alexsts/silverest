<?php

return [
    'home' => 'Pealeht',
    'about_us' => 'Meist',
    'about_us_1' => 'Tere tulemast juveelifirma SILVEREST kodulehele ja e-poodi!',
    'about_us_2' => 'Juveelifirma SILVEREST tegeleb 925 prooviga hõbeehete valmistamisega alates 2004. aastast.
                     Kõrgekvaliteediliste ja jõukohaste hindadega ehete valmistamine on alati olnud meie eesmärgiks.
                     Toodetes kasutame naturaalseid ja sünteetilisi ehiskive: rauhtopaasi (suitsutopaasi), mäekristalli, ametüsti, tsirkooni, krüsoliiti, turmaliini, akvamariini, malahhiiti, ahhaati, oonüksit ja paljusid teisi.',
    'about_us_3' => 'Juveeliehete tootmiskoht paikneb Kirde-Eestis. Meie firma tooteteostus järgib klassikalise juveelitööstuse parimaid traditsioone ja kaasaegseid moetendentse.
                     Me uuendame pidevalt tootevalikut ning püüame operatiivselt jälgida ostjate maitseeelistusi ja nõudmisi.',
    'about_us_4' => 'Soovime meeldivaid ja Teie nõudmisi rahuldavaid oste!',
];
