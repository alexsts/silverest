<?php

return [
    'home' => 'Pealeht',
    'delivery' => 'Kohaletoimetamine',
    'delivery_1' => 'Kaup toimetatakse EESTI POST või OMNIVA.',
    'delivery_2' => 'Kauba olemasolu ja kohaletoimetamine Eestis 5,00 €',
    'delivery_3' => 'Saadetised on kindlustatud. Kohaletoimetamise aeg Eesti piires on 3-5 päeva.',
    'delivery_4' => 'Paki saamisel on klient kohustatud seda kontrollima. Kui saadetis on kahjustatud, on klient kohustatud sellest postitöötajat teavitama. Postitöötaja peab viivitamatult koostama sellekohase akti.',
];
