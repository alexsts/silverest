<?php

return [
    'code' => 'KOOD',
    'check_out' => 'Vaata',
    'new_products' => 'Uus Tooted',
    'catalogue_update' => 'Kataloog on alati uuendatakse viimaseid trende',
    'production' => 'Tootmine',
    'our_production' => 'Meie tooted koosnevad paljudest astmetest, mida viivad läbi kvalifitseeritud käsitöölised',
    'add_to_wishlist' => 'Lisa lemmikutele',
    'add_to_cart' => 'Lisa ostukorvi',
    'shop_in_fama' => 'Kauplus Fama',
    'read_more' => 'Loe edasi',
    'our_local_shop' => 'Meie kauplus',
    'we_could_gladly' => 'Meil oleks hea meel teiega isiklikult kohtuda, palun tulge Narva Fame Keskus asuvasse kohalikku kauplustesse'
];
