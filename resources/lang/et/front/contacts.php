<?php

return [
    'home' => 'Pealeht',
    'our_location' => 'Meie Asukoht.',
    'we_located' => 'Me asume Fama Keskus',
    'contacts' => 'Kontakt',
    'contacts_1' => 'Ettevõte:',
    'contacts_2' => 'OÜ "SILVEREST" reg.n.11003444',
    'contacts_3' => 'Aadress:',
    'contacts_4' => 'Narva Tallinna mnt 19c FAMA KESKUS, Ida-Virumaa, 20303, Estonia, Phone mob. 59020001, Phone / Fax (35) 62620',
    'contacts_5' => 'E-post:',
    'contacts_6' => 'silverest@silverest.ee',
    'contacts_7' => 'Pangaandmed:',
    'contacts_8' => 'Swedbank Eesti, 221024580301, IBAN: EE722200221024580301',
];
