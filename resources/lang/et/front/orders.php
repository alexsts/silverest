<?php

return [
    'back' => 'Tagasi',
    'home' => 'Koduleht',
    'account_dashboard' => 'Kasutaja kabineti',
    'orders' => 'Tellimused',
    'order' => 'Tellimus',
    'gem' => 'Kivi',
    'size' => 'Suurus',
    'quantity' => 'Kogus',
    'there_are_no_products' => 'Selles tellimuses pole tooteid',
    'show_more_details' => 'Näita üksikasju',
    'download_invoice' => 'Lae arvel',
    'extra_notes' => 'Lisamärkused',
    'cancel_order' => 'Tühista tellimus',
    'no_orders' => 'Teil pole veel tellimusi',
    'please_leave' => 'Palun kirjutage tellimuse täiendavate taotluste kohta',
    'submit' => 'Saada'
];
