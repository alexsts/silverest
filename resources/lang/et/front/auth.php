<?php

return [
    'home' => 'Koduleht',
    'registered_customers' => 'REGISTREERITUD KASUTAJAID',
    'if_account' => 'Palun sisestage oma andmed.',
    'email' => 'e-posti aadress',
    'password' => 'Salasõna',
    'remember' => 'Mäleta mind',
    'login' => 'Logi sisse',
    'forgot' => 'Unustatud salasõna',
    'create' => 'Loo konto',

    'new_customers' => 'UUED KASUTAJAID',
    'register' => 'Registreeru',
    'f_name' => 'Nimi',
    'l_name' => 'Perekonnanimi',
    'confirm_password' => 'Kinnita salasõna',
    'clear' => 'Kustuta',
    'already_account' => 'Juba on konto',

    'reset_password' => 'Kustuta salasõna',
    'reset_password_up' => 'KUSTUTA SALASONA',
    'if_send' => 'Kui unustasite salasõna, saadame Teile parooli lähtestamiseks e-kirjaga',
    'send' => 'Saada kirja',
    'back_to_login' => 'Tagasi logisisse',

    'reset_the_password' => 'Eemalda salasõna',
    'email_here' => 'E-posti aadress',
    'please_new_password' => 'Palun sisesta uus salasõna',
];
