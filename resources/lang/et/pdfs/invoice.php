<?php

return [
    'invoice_to' => 'ARVE',
    'invoice' => 'ARVE',
    'date_of_invoice' => 'Arve kuupäev',
    'description' => 'KIRJELDUS',
    'unit_price' => 'ÜHIKUHIND',
    'quantity' => 'ARV',
    'total' => 'KOKKU',
    'size' => 'Suurus',
    'cart_total' => 'KORV KOKKU',
    'shipping_total' => 'LAEVANDUS KOKKU',
    'thank_you' => 'Tänan teid',
    'notice' => 'TEADE',
    'order_will_be_dispatched' => 'Tellimus saadetakse pärast eduka makse saamist.',
    'invoice_was_created' => 'Arve loodi arvutis ja see kehtib ilma allkirja ja pitsatita.',
    'please_send' => 'Palun saatke oma makse, kasutades allolevaid pangaandmeid',
    'bank_details' => 'Pangaandmed',
];
