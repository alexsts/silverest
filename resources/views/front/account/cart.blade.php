@extends('layouts.front')

@section('title',"Silverest | Cart")

@section('styles')

    <!-- Custom Styling -->
    <style>
        .cart_product_img {
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.15);
        }

        .breadcrumbs {
            background: #f6f6f6 url("{{asset('assets/front/images/banners/banner.jpg')}}") no-repeat scroll center center / cover;
        }

        .overlay-bg::before {
            background: #f6f6f6 none repeat scroll 0 0;
            content: "";
            height: 100%;
            left: 0;
            opacity: 0.3;
            position: absolute;
            top: 0;
            transition: all 0.3s ease 0s;
            width: 100%;
        }

        .breadcrumbs-title {
            color: transparent;
        }

        .breadcrumb-list > li::before {
            color: #666666;
            content: "";
        }

        .breadcrumb-list > li {
            font-size: 1.5rem;
        }

        .qtybutton {
            cursor: default;
        }

        td.cart_thumbnail {
            padding: 20px 30px 20px 20px !important;
        }

        .old-price {
            color: #a6a6a6;
            font-size: 12px;
            text-decoration: line-through;
        }

        .new-price {
            color: #666;
        }

        a.previous, a.next {
            text-decoration: none;
            display: inline-block;
            padding: 8px 16px;
        }

        a.previous:hover, a.next:hover {
            background-color: #ddd;
            color: black;
        }

        .previous {
            background-color: #f1f1f1;
            color: black;
        }

        a.next {
            background-color: #1a237e;
            color: white;
        }
    </style>

@endsection

@section('content')

    <!-- Start page content -->
    <section id="page-content" class="page-wrapper">

        <!-- SHOP SECTION START -->
        <div class="shop-section mb-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-12">
                        <ul class="cart-tab">
                            <li>
                                <a class="active" href="{{route('account.cart')}}">
                                    <span>01</span>
                                    {{trans('front/cart.shopping_cart')}}
                                </a>
                            </li>
                            <li>
                                <a href="{{route('account.wishlist')}}">
                                    <span>02</span>
                                    {{trans('front/cart.wishlist')}}
                                </a>
                            </li>
                            <li>
                                <a href="{{route('account.order.checkout')}}">
                                    <span>03</span>
                                    {{trans('front/cart.checkout')}}
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span>04</span>
                                    {{trans('front/cart.order_complete')}}
                                </a>
                            </li>
                        </ul>
                    </div>

                    @if($cart_amount > 0)
                        <div class="col-md-10 col-sm-12">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- shopping-cart start -->
                                <div class="tab-pane active" id="shopping-cart">
                                    <div class="shopping-cart-content">
                                        <div class="table-content table-responsive mb-20">
                                            <table class="text-center">
                                                <thead>
                                                <tr>
                                                    <th class="product-thumbnail">{{trans('front/cart.product')}}</th>
                                                    <th class="product-price" style="width: 130px;">{{trans('front/cart.price')}}</th>
                                                    <th class="product-quantity">{{trans('front/cart.quantity')}}</th>
                                                    <th class="product-subtotal" style="min-width: 130px;">{{trans('front/cart.total')}}</th>
                                                    <th class="product-remove">{{trans('front/cart.remove')}}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($cart_products as $cart_product)
                                                    <!-- tr -->
                                                    <tr>
                                                        <td class="product-thumbnail cart_thumbnail">
                                                            <div class="pro-thumbnail-img">
                                                                <img src="{{$cart_product->photos()->count() > 0 ?  $cart_product->photos->first()->getPreviewPath() : ''}}" alt="" class="cart_product_img">
                                                            </div>
                                                            <div class="pro-thumbnail-info text-left">
                                                                <h6 class="product-title-2">
                                                                    <a>{{$cart_product->name}} ( {{$cart_product->code}} )</a>
                                                                </h6>
                                                                <p>
                                                                    <span>{{trans('front/cart.gem')}} <strong>: </strong></span>
                                                                    @foreach($cart_product->gems as $gem)
                                                                        @if($loop->last)
                                                                            {{$gem->name}}
                                                                        @else
                                                                            {{$gem->name}},
                                                                        @endif
                                                                    @endforeach
                                                                </p>
                                                                @if($cart_product->pivot->size > 0)
                                                                    <p>
                                                                        <span>{{trans('front/cart.size')}} <strong>: </strong></span>{{$cart_product->pivot->size}}
                                                                    </p>
                                                                @endif
                                                            </div>
                                                        </td>
                                                        <td class="product-price">
                                                            @if($cart_product->getDiscountPrice())
                                                                <span class="new-price">&euro; {{number_format ($cart_product->getDiscountPrice(),2)}}</span><br>
                                                                <span class="old-price">&euro; {{number_format ($cart_product->price,2)}}</span>
                                                            @else
                                                                <span class="new-price">&euro; {{number_format ($cart_product->price,2)}}</span>
                                                            @endif
                                                        </td>
                                                        <td class="product-quantity">
                                                            <div class="cart-plus-minus f-left">
                                                                <input type="text" value="{{$cart_product->pivot->quantity}}" max="{{$cart_product->quantity}}" min="0" disabled name="qtybutton" data-product="{{$cart_product->id}}" class="cart-plus-minus-box">
                                                            </div>
                                                        </td>
                                                        <td class="product-subtotal">&euro; {{($cart_product->getDiscountPrice() ? number_format ($cart_product->getDiscountPrice() * $cart_product->pivot->quantity,2) : number_format ($cart_product->price * $cart_product->pivot->quantity,2))}}</td>
                                                        <td class="product-remove">
                                                            {!! Form::open(['method'=>'POST','route'=>['account.cart.remove']]) !!}
                                                            <input type="hidden" name="product_id" value="{{$cart_product->id}}">
                                                            <button type="submit" class="cart_remove">
                                                                <i class="zmdi zmdi-close"></i>
                                                            </button>
                                                            {!! Form::close() !!}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row mb-50">
                                            <div class="col-md-12">
                                                {{$cart_products->links('vendor.pagination.front')}}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="payment-details box-shadow p-30 mb-50">
                                                    <h6 class="widget-title border-left mb-20">{{trans('front/cart.cart_details')}}</h6>
                                                    <table>
                                                        <tr>
                                                            <td class="td-title-1">{{trans('front/cart.vat_already')}}</td>
                                                            <td class="td-title-2">20 %</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="order-total">{{trans('front/cart.cart_subtotal')}}</td>
                                                            <td class="order-total-price">&euro; {{number_format ($cart_subtotal,2)}}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- shopping-cart end -->
                            </div>

                            <div class="mt-20">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('account.order.checkout')}}" class="next f-right">{{trans('front/cart.next')}} &nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @else
                        <div class="col-md-10">
                            <div class="tab-content mb-40">
                                <!-- checkout start -->
                                <div class="tab-pane active" id="checkout">
                                    <div class="checkout-content box-shadow p-30">
                                        <div class="payment-method">
                                            <!-- our order -->
                                            <div class="payment-details">
                                                <h4 style="color: #1a237e;margin-bottom: 0px;font-size: 20px;text-align: center"> {{trans('front/cart.shopping_cart_is_empty')}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="mt-50">
                                <div class="row">
                                    <div class="col-md-12">
                                        <a href="{{route('account.order.checkout')}}" class="next f-right">{{trans('front/cart.next')}} &nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <!-- SHOP SECTION END -->
    </section>
    <!-- End page content -->

@endsection

@section('scripts')

    <!-- Quantity -->
    <script>
        /* ********************************************
		   Cart Plus Minus Button
	    ******************************************** */
        $(".cart-plus-minus").prepend('<div class="dec qtybutton">-</div>');
        $(".cart-plus-minus").append('<div class="inc qtybutton">+</div>');
        $(".qtybutton").on("click", function () {
            var $button = $(this);
            var oldValue = $button.parent().find("input").val();
            if ($button.text() == "+") {
                var newVal = parseFloat(oldValue) + 1;
            }
            else {
                // Don't allow decrementing below zero
                if (oldValue > 0) {
                    var newVal = parseFloat(oldValue) - 1;
                }
                else {
                    newVal = 0;
                }
            }

            var product_id = $button.parent().find("input").attr('data-product');
            var data = {
                _token: '{!! csrf_token() !!}',
                product_id: product_id,
                quantity: newVal
            }

            $.ajax({
                'url': "{{route('account.cart.quantity.ajax')}}",
                'data': data,
                'method': 'POST',
                success: function (response) {
                    location.reload();
                }
            });

            $button.parent().find("input").val(newVal);
        });
    </script>

@endsection
