@extends('layouts.front')

@section('title',"Silverest | Account Dashboard")

@section('styles')

    <!-- Custom Styling -->
    <style>
        #cancel_search:hover {
            color: #1a237e;
        }

        .breadcrumbs {
            background: #f6f6f6 url("{{asset('assets/front/images/banners/banner.jpg')}}") no-repeat scroll center center / cover;
        }

        .overlay-bg::before {
            background: #f6f6f6 none repeat scroll 0 0;
            content: "";
            height: 100%;
            left: 0;
            opacity: 0.3;
            position: absolute;
            top: 0;
            transition: all 0.3s ease 0s;
            width: 100%;
        }

        .breadcrumbs-title {
            color: transparent;
        }

        .breadcrumb-list > li::before {
            color: #666666;
            content: "";
        }

        .breadcrumb-list > li {
            font-size: 1.5rem;
        }

        .date_size {
            width: 100%;
            line-height: 33px;
            padding-left: 20px;
        }

        input, textarea {
            font-family: 'Pathway Gothic One', sans-serif;
            font-size: 16px !important;
            color: #1a237e !important;
        }

        .btn-large {
            border: 2px solid #1a237e;
            padding: 20px;
            margin-bottom: 20px;
        }

        .btn-large:hover {
            border: 2px solid #2529c3;
            color: #1a237e;
        }

        a:hover{
            color: #1a237e;
        }

        .btn-large p {
            font-size: 20px;
            margin-top: 20px;
        }

        i {
            color: #1a237e;
        }
    </style>

@endsection

@section('content')

    <!-- Start page content -->
    <div id="page-content" class="page-wrapper">

        <!-- LOGIN SECTION START -->
        <div class="login-section mb-80">
            <div class="container">

                <h1 class="mb-50">{{trans('front/dashboard.account_dashboard')}}</h1>

                <div class="row">
                    <div class="col-md-6">
                        <a href="{{route('account.orders')}}">
                            <div class="btn-large">
                                @if($orders_amount)
                                    <h2>
                                        <i class="fa fa-tasks" aria-hidden="true"></i>&nbsp; {{trans('front/dashboard.orders')}} ( {{$orders_amount}} )
                                    </h2>
                                @else
                                    <h2>
                                        <i class="fa fa-tasks" aria-hidden="true"></i>&nbsp; {{trans('front/dashboard.no_orders')}}
                                    </h2>
                                @endif
                                <p>{{trans('front/dashboard.click_to_see_orders')}}</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('account.edit')}}">
                            <div class="btn-large">
                                <h2><i class="fa fa-user" aria-hidden="true"></i>&nbsp; {{trans('front/dashboard.edit_account')}}</h2>
                                <p>{{trans('front/dashboard.click_to_edit')}}</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('account.cart')}}">
                            <div class="btn-large">
                                @if($cart_amount)
                                    <h2>
                                        <i class="zmdi zmdi-shopping-cart-plus"></i>&nbsp; {{trans('front/dashboard.cart')}} ( {{$cart_amount}} )
                                    </h2>
                                @else
                                    <h2>
                                        <i class="zmdi zmdi-shopping-cart-plus"></i>&nbsp; {{trans('front/dashboard.cart_is_empty')}}
                                    </h2>
                                @endif
                                <p>{{trans('front/dashboard.click_to_see_cart')}}</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('account.wishlist')}}">
                            <div class="btn-large">
                                @if($wishlist_amount)
                                    <h2>
                                        <i class="zmdi zmdi-favorite" style="font-size: 25px !important;"></i>&nbsp; {{trans('front/dashboard.wishlist')}} ( {{$wishlist_amount}} )
                                    </h2>
                                @else
                                    <h2>
                                        <i class="zmdi zmdi-favorite" style="font-size: 25px !important;"></i>&nbsp; {{trans('front/dashboard.wishlist_empty')}}
                                    </h2>
                                @endif
                                <p>{{trans('front/dashboard.click_to_see_wishlist')}}</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- LOGIN SECTION END -->
    </div>
    <!-- End page content -->

@endsection

