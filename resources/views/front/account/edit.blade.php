@extends('layouts.front')

@section('title',"Silverest | Account Edit")

@section('styles')

    <!-- Custom Styling -->
    <style>
        #cancel_search:hover {
            color: #1a237e;
        }

        .breadcrumbs {
            background: #f6f6f6 url("{{asset('assets/front/images/banners/banner.jpg')}}") no-repeat scroll center center / cover;
        }

        .overlay-bg::before {
            background: #f6f6f6 none repeat scroll 0 0;
            content: "";
            height: 100%;
            left: 0;
            opacity: 0.3;
            position: absolute;
            top: 0;
            transition: all 0.3s ease 0s;
            width: 100%;
        }

        .breadcrumbs-title {
            color: transparent;
        }

        .breadcrumb-list > li::before {
            color: #666666;
            content: "";
        }

        .breadcrumb-list > li {
            font-size: 1.5rem;
        }

        .date_size {
            width: 100%;
            line-height: 33px;
            padding-left: 20px;
        }

        input, textarea {
            font-family: 'Pathway Gothic One', sans-serif;
            font-size: 16px !important;
            color: #1a237e !important;
        }

        .widget-title {
            font-size: 15px;
        }

        span.required {
            color: red;
            font-size: 20px;
        }

        .ap-input-icon svg {
            transform: translateY(-100%);
        }

        input {
            border-radius: 0px !important;
        }

        .ap-input-icon.ap-icon-clear svg {
            transform: translateY(-130%);
        }

        .submit-btn-1 {
            margin-right: 10px;
            margin-top: 0px;
        }

        .error{
            border: 1px solid red !important;
            color: red !important;
        }
    </style>

@endsection

@section('content')

    <section id="page-content" class="page-wrapper">

        <!-- SHOP SECTION START -->
        <div class="shop-section mb-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        {!! Form::open(['method'=>'POST','route'=>'account.update','class'=>'form-horizontal','id'=>'update_personal_info']) !!}

                        <div class="tab-content">
                            <!-- checkout start -->
                            <div class="tab-pane active" id="checkout">
                                <div class="checkout-content box-shadow p-30">
                                    <div class="row">
                                        <!-- billing details -->
                                        <div class="col-md-12">
                                            <div class="billing-details pr-10">
                                                <h6 class="widget-title border-left mb-20">
                                                    <i class="fa fa-user" aria-hidden="true"></i>&nbsp; {{trans('front/edit.personal_details')}}
                                                </h6>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="billing-details pr-10">
                                                <label>{{trans('front/edit.first_name')}} <span class="required">*</span></label>
                                                <input type="text" placeholder="{{trans('front/edit.first_name')}}" name="first_name" value="{{$user->first_name}}" class="{{$errors->has('first_name') ? 'error':''}}" required>
                                                <label>{{trans('front/edit.email_address')}} <span class="required">*</span></label>
                                                <input type="text" placeholder="{{trans('front/edit.email_address')}}" name="email" value="{{$user->email}}" class="{{$errors->has('email') ? 'error':''}}" required>
                                                <label>{{trans('front/edit.company_name')}} ({{trans('front/edit.optional')}})</label>
                                                <input type="text" placeholder="{{trans('front/edit.company_name')}}" name="company_name" value="{{$user->company_name}}" class="{{$errors->has('company_name') ? 'error':''}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="billing-details pr-10">
                                                <label>{{trans('front/edit.last_name')}} <span class="required">*</span></label>
                                                <input type="text" placeholder="{{trans('front/edit.last_name')}}" name="last_name" value="{{$user->last_name}}" class="{{$errors->has('last_name') ? 'error':''}}" required>
                                                <label>{{trans('front/edit.phone_number')}} <span class="required">*</span></label>
                                                <input type="text" placeholder="{{trans('front/edit.phone_number')}}" name="phone_number" value="{{$user->phone_number}}" class="{{$errors->has('phone_number') ? 'error':''}}" required>
                                                <label>{{trans('front/edit.dob')}} ({{trans('front/edit.optional')}})</label>
                                                <input type="text" class="dob" name="dob" class="date_size" placeholder="{{trans('front/edit.dob')}}" value="{{$user->dob != '0000-00-00' ? $user->dob : ''}}" class="{{$errors->has('dob') ? 'error':''}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- checkout end -->
                        </div>

                        <div class="tab-content mt-40">
                            <!-- checkout start -->
                            <div class="tab-pane active" id="checkout">
                                <div class="checkout-content box-shadow p-30">
                                    <div class="row">
                                        <!-- billing details -->
                                        <div class="col-md-12">
                                            <div class="billing-details pr-10">
                                                <h6 class="widget-title border-left mb-20">
                                                    <i class="fa fa-address-card-o" aria-hidden="true"></i>&nbsp; {{trans('front/edit.delivery_address')}}
                                                </h6>
                                                <label>{{trans('front/edit.find_address')}}</label>
                                                <input type="text" placeholder="{{trans('front/edit.start_typing')}}" id="form-address">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="billing-details pr-10">
                                                <label>{{trans('front/edit.address_line')}} <span class="required">*</span></label>
                                                <input type="text" placeholder="{{trans('front/edit.address_line')}}" id="form-address2" name="address_line" value="{{$delivery_address ? $delivery_address->address_line : '' }}" class="{{$errors->has('address_line') ? 'error':''}}"  required>
                                                <label>{{trans('front/edit.country')}} <span class="required">*</span></label>
                                                <input type="text" placeholder="{{trans('front/edit.country')}}" id="form-country" name="country" value="{{$delivery_address ? $delivery_address->country : '' }}" class="{{$errors->has('country') ? 'error':''}}" required>
                                                <label>{{trans('front/edit.state_region')}} <span class="required">*</span></label>
                                                <input type="text" placeholder="{{trans('front/edit.state_region')}}" id="form-state" name="state" value="{{$delivery_address ? $delivery_address->state : '' }}" class="{{$errors->has('state') ? 'error':''}}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="billing-details pr-10">
                                                <label>{{trans('front/edit.city')}} <span class="required">*</span></label>
                                                <input type="text" placeholder="{{trans('front/edit.city')}}" id="form-city" name="city" value="{{$delivery_address ? $delivery_address->city : '' }}" class="{{$errors->has('city') ? 'error':''}}" required>
                                                <label>{{trans('front/edit.zip')}} <span class="required">*</span></label>
                                                <input type="text" placeholder="{{trans('front/edit.zip')}}" id="form-zip" name="zip" value="{{$delivery_address ? $delivery_address->zip : '' }}" class="{{$errors->has('zip') ? 'error':''}}" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- checkout end -->
                        </div>

                        <div class="tab-content mt-40">
                            <!-- checkout start -->
                            <div class="tab-pane active" id="checkout">
                                <div class="checkout-content box-shadow p-30">
                                    <div class="row">
                                        <!-- billing details -->
                                        <div class="col-md-12">
                                            <div class="billing-details pr-10">
                                                <h6 class="widget-title border-left mb-20">
                                                    <i class="fa fa-unlock-alt" aria-hidden="true"></i>&nbsp; {{trans('front/edit.change_password')}}
                                                </h6>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="billing-details pr-10">
                                                <label>{{trans('front/edit.new_password')}}</label>
                                                <input name="password" type="password" placeholder="{{trans('front/edit.new_password')}}">
                                                <label>{{trans('front/edit.confirm_password')}}</label>
                                                <input name="confirm_password" type="password" placeholder="{{trans('front/edit.confirm_password')}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- checkout end -->
                            </div>
                        </div>

                        <div class="tab-content mt-40">
                            <!-- checkout start -->
                            <div class="tab-pane active" id="checkout">
                                <div class="checkout-content box-shadow p-30">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button id="clear_myaccount" class="submit-btn-1 mt-20 btn-hover-1" type="reset">{{trans('front/edit.clear_fields')}}</button>
                                        </div>
                                        <div class="col-md-6">
                                            <button class="submit-btn-1 mt-20 btn-hover-1 f-right" type="submit" value="register">{{trans('front/edit.update_account')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- checkout end -->
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
        <!-- SHOP SECTION END -->
    </section>

@endsection

@section('scripts')

    <!-- Javascript files -->
    <script src="{{asset('assets/front/js/cleave.min.js')}}"></script>
    <script src="{{asset('assets/front/js/places.js')}}"></script>

    <!-- Autocomplete date -->
    <script>
        var cleave = new Cleave('.dob', {
            date: true,
            datePattern: ['Y', 'm', 'd']
        });
    </script>

    <!-- Autocomplete address -->
    <script>
        (function () {
            var placesAutocomplete = places({
                container: document.querySelector('#form-address')
            });
            placesAutocomplete.on('change', function resultSelected(e) {
                document.querySelector('#form-address2').value = e.suggestion.name || '';
                document.querySelector('#form-state').value = e.suggestion.administrative || '';
                document.querySelector('#form-country').value = e.suggestion.country || '';
                document.querySelector('#form-city').value = e.suggestion.city || '';
                document.querySelector('#form-zip').value = e.suggestion.postcode || '';
            });
        })();
    </script>

@endsection
