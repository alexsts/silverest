@extends('layouts.front')

@section('title',"Silverest | Order")

@section('styles')

    <!-- Custom Styling -->
    <style>
        .cart_product_img {
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.15);
        }

        .breadcrumbs {
            background: #f6f6f6 url("{{asset('assets/front/images/banners/banner.jpg')}}") no-repeat scroll center center / cover;
        }

        .overlay-bg::before {
            background: #f6f6f6 none repeat scroll 0 0;
            content: "";
            height: 100%;
            left: 0;
            opacity: 0.3;
            position: absolute;
            top: 0;
            transition: all 0.3s ease 0s;
            width: 100%;
        }

        .breadcrumbs-title {
            color: transparent;
        }

        .breadcrumb-list > li::before {
            color: #666666;
            content: "";
        }

        .breadcrumb-list > li {
            font-size: 1.5rem;
        }

        .qtybutton {
            cursor: default;
        }

        td.cart_thumbnail {
            padding: 20px 30px 20px 20px !important;
        }

    </style>

    <!-- Style Tweaks -->
    <style>
        a.previous_bt {
            text-decoration: none;
            display: inline-block;
            padding: 8px 16px;
        }

        a.previous_bt:hover {
            background-color: #ddd;
            color: black;
        }

        .previous_bt {
            background-color: #1a237e;
            color: white;
        }
    </style>

@endsection

@section('content')

    <!-- Start page content -->
    <section id="page-content" class="page-wrapper">

        <!-- SHOP SECTION START -->
        <div class="shop-section mb-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- order-complete start -->
                            <div class="tab-pane active" id="order-complete">
                                <div class="order-complete-content box-shadow">
                                    @if(Session::has('thank_you'))
                                        <div class="thank-you p-30 text-center" style="    border-bottom: 1px solid #eee">
                                            <h3 class="text-black-5 mb-0" style="color: #1a237e">{{trans('front/order.thank_you')}} </h3>
                                        </div>
                                    @endif
                                    <div class="order-info p-30" style="border: none">
                                        <ul class="order-info-list">
                                            <li>
                                                <h3 style="margin-bottom: 20px">order #{{$order->id}}</h3>
                                                <h3>{{trans('front/order.order_status')}}
                                                    <span style="color: #1a237e">{{$order->status? $order->status->name: 'Error'}}</span>
                                                </h3>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">

                                            @if($order->items()->count() > 0)

                                                <div class="payment-method" style="border-bottom: 1px solid #dfdfdf; border-top:1px solid #dfdfdf ">
                                                    <!-- our order -->
                                                    <div class="payment-details">
                                                        <div class="row">
                                                            <div class="col-md-6" style="border-right: 1px solid #dfdfdf;padding:20px;padding-left: 35px">
                                                                <div class="billing-details pr-10">
                                                                    <h5 class="widget-title mb-20 mt-10">
                                                                        <i class="fa fa-truck" aria-hidden="true"></i>&nbsp; {{trans('front/order.delivery')}}
                                                                    </h5>
                                                                </div>
                                                                <table>
                                                                    @if($order->delivery)
                                                                        <tr>
                                                                            <td class="td-title-1">{{trans('front/order.delivery_method')}}</td>
                                                                            <td class="td-title-2">{{ucwords ($order->delivery->method)}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td-title-1">
                                                                                {{($order->delivery->method == 'omniva') ? trans('front/order.shipping_notes') : trans('front/order.delivery_address') }}
                                                                            </td>
                                                                            <td class="td-title-2">
                                                                                {{$order->delivery->customer_address}}
                                                                                {{--{{$user_address->address_line}}, {{$user_address->city}}, {{$user_address->state}}, {{$user_address->country}}, {{$user_address->zip}}--}}
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td-title-1">{{trans('front/order.phone_number')}}</td>
                                                                            <td class="td-title-2">{{$order->delivery->customer_phone}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="order-total">{{trans('front/order.approximate_delivery')}}</td>
                                                                            <td class="order-total-price">{{date('j F Y', strtotime("+7 day", strtotime($order->created_at)))}}</td>
                                                                        </tr>
                                                                    @else
                                                                        @php
                                                                            $order->status_id = \Illuminate\Support\Facades\Config::get('constants.STATUS.ERROR');
                                                                            $order->save();
                                                                        @endphp
                                                                        <tr>
                                                                            <td class="td-title-1">{{trans('front/order.delivery')}}</td>
                                                                            <td class="td-title-2" style="color: red">Error</td>
                                                                        </tr>
                                                                    @endif
                                                                </table>
                                                            </div>
                                                            <div class="col-md-6" style="padding:20px;padding-right: 35px">
                                                                <div class="billing-details pr-10">
                                                                    <h5 class="widget-title mb-20 mt-10">
                                                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp; {{trans('front/order.payment')}}
                                                                    </h5>
                                                                </div>
                                                                <table>
                                                                    @if($order->payment)
                                                                        <tr>
                                                                            <td class="td-title-1">{{trans('front/order.payment_method')}}</td>
                                                                            <td class="td-title-2">{{ucwords($order->payment->method)}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td-title-1">{{trans('front/order.cart_subtotal')}}</td>
                                                                            <td class="td-title-2">&euro; {{number_format ($order->payment->cart_total,2)}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="td-title-1">{{trans('front/order.shipping_handling')}}</td>
                                                                            <td class="td-title-2">&euro;
                                                                                <span id="shipping_price">{{number_format (($order->payment->shipping_total),2)}}</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="order-total">{{trans('front/order.order_total')}}</td>
                                                                            <td class="order-total-price">&euro;
                                                                                <span id="total_price">{{number_format (($order->payment->subtotal) ,2)}}</span>
                                                                            </td>
                                                                        </tr>
                                                                    @else
                                                                        @php
                                                                            $order->status_id = \Illuminate\Support\Facades\Config::get('constants.STATUS.ERROR');
                                                                            $order->save();
                                                                        @endphp
                                                                        <tr>
                                                                            <td class="td-title-1">{{trans('front/order.payment')}}</td>
                                                                            <td class="td-title-2" style="color: red">Error</td>
                                                                        </tr>
                                                                    @endif
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="billing-details pr-10 pt-10">
                                                    <h5 class="widget-title mb-20 mt-10 ml-20">
                                                        <i class="fa fa-tasks" aria-hidden="true"></i>&nbsp; {{trans('front/order.order_items')}}
                                                    </h5>
                                                </div>

                                                <div class="tab-content" style="padding: 20px;padding-top: 0px">
                                                    <!-- shopping-cart start -->
                                                    <div class="tab-pane active" id="shopping-cart">
                                                        <div class="shopping-cart-content">
                                                            <div class="table-content table-responsive">
                                                                <table class="text-center">
                                                                    <thead>
                                                                    <tr>
                                                                        <th class="product-thumbnail">{{trans('front/order.product')}}</th>
                                                                        <th class="product-price" style="width: 130px;">{{trans('front/order.price')}}</th>
                                                                        <th class="product-quantity">{{trans('front/order.quantity')}}</th>
                                                                        <th class="product-subtotal" style="min-width: 130px;">{{trans('front/order.total')}}</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($order->items as $item)
                                                                        @php
                                                                            $product = \App\Product::withTrashed()->find($item->product_id);
                                                                        @endphp
                                                                        @if($product)
                                                                            <tr>
                                                                                <td class="product-thumbnail cart_thumbnail">
                                                                                    <div class="pro-thumbnail-img">
                                                                                        <img src="{{$product->photos()->count() > 0 ?  $product->photos->first()->getPreviewPath() : ''}}" alt="" class="cart_product_img">
                                                                                    </div>
                                                                                    <div class="pro-thumbnail-info text-left">
                                                                                        <h6 class="product-title-2">
                                                                                            <a>{{$product->name}} ( {{$product->code}} )</a>
                                                                                        </h6>
                                                                                        @if($product->gems()->count() > 0)
                                                                                            <p>
                                                                                                <span>{{trans('front/order.gem')}}
                                                                                                    <strong>: </strong></span>
                                                                                                @foreach($product->gems as $gem)
                                                                                                    @if($loop->last)
                                                                                                        {{$gem->name}}
                                                                                                    @else
                                                                                                        {{$gem->name}},
                                                                                                    @endif
                                                                                                @endforeach
                                                                                            </p>
                                                                                        @endif
                                                                                        @if($item->size > 0)
                                                                                            <p>
                                                                                                <span>{{trans('front/order.size')}}
                                                                                                    <strong>: </strong></span>{{$item->size}}
                                                                                            </p>
                                                                                        @endif
                                                                                    </div>
                                                                                </td>
                                                                                <td class="product-price">
                                                                                    @if($item->discount_price > 0)
                                                                                        <span class="new-price">&euro; {{number_format ($item->discount_price,2)}}</span>
                                                                                    @else
                                                                                        <span class="new-price">&euro; {{number_format ($product->price,2)}}</span>
                                                                                    @endif
                                                                                </td>
                                                                                <td class="product-price">
                                                                                    <span class="new-price">{{$item->quantity}}</span>
                                                                                </td>
                                                                                <td class="product-subtotal">&euro; {{($item->discount_price > 0 ? number_format ($item->discount_price * $item->quantity,2) : number_format ($product->price * $item->quantity,2))}}</td>
                                                                            </tr>
                                                                        @else
                                                                            <tr>
                                                                                <td>Error</td>
                                                                                <td>Error</td>
                                                                                <td>Error</td>
                                                                                <td>Error</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- shopping-cart end -->

                                                </div>

                                            @else
                                                <div class="payment-method">
                                                    <!-- our order -->
                                                    <div class="payment-details">
                                                        <h4 style="color: #1a237e;margin-bottom: 0px"> {{trans('front/order.there_are_no_products')}} (ERROR)</h4>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- order-complete end -->
                        </div>
                        <div class="mt-50">
                            <a href="{{route('account.orders')}}" class="previous_bt f-left"><i class="fa fa-angle-left" aria-hidden="true"></i>&nbsp; {{trans('front/order.back')}}
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- SHOP SECTION END -->

    </section>
    <!-- End page content -->

@endsection
