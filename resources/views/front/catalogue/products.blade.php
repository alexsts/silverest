@extends('layouts.front')

@section('title',"Silverest | Catalogue")

@section('styles')

    <!-- Custom Styling -->
    <style>
        .pro-tab-menu {
            margin: 0px;
        }

        .product-color li::before {
            background-color: transparent;
        }

        .widget-color ul li::before {
            background: transparent;
            border-radius: 50%;
            content: "";
            height: 12px;
            left: 0;
            margin-top: -6px;
            position: absolute;
            top: 50%;
            width: 12px;
        }

        .product-color li span {
            border-radius: 50%;
            content: "";
            height: 12px;
            left: 0;
            margin-top: -6px;
            position: absolute;
            top: 50%;
            width: 12px;
        }

        .product_styles ul li {
            margin-top: 5px;
        }

        .product_styles ul li:nth-child(n+10) {
            display: none;
        }

        .product_styles span {
            cursor: pointer;
            color: #1a237e;
            margin-top: 15px;
            border: 1px solid #1a237e;
            text-align: center;
            width: 100%;
            padding: 5px 10px;
        }

        .product-color .fa-times {
            color: #1a237e !important;
            font-size: 1.4rem;
            margin-left: -10px;
        }

        .reset-filters .fa-times {
            color: #1a237e !important;
            font-size: 1.5rem;
            margin-left: 10px;
        }

        .ui-slider-handle:active,
        .ui-slider-handle {
            outline: none;
            border: none;
        }

        .separator-filter {
            background: #1a237e;
            height: 1px;
            width: 100%;
        }

        ul.filter-reset li {
            margin: 7px 0;
        }

        .reset-all-filters {
            color: #1a237e !important;
            margin: 0px;
        }

        .product-gem img {
            height: 42px;
            width: 42px;
            padding: 0.1rem;
            border-radius: 50px;
            margin-right: 5px;
            margin-bottom: 10px
        }

        #cancel_search {
            background: transparent none repeat scroll 0 0;
            color: #999;
            font-size: 20px;
            position: absolute;
            right: 0px;
            top: 12px;
            transition: all 0.3s ease 0s;
            width: 30px;
        }

        #cancel_search:hover {
            color: #1a237e;
        }

        .breadcrumbs {
            background: #f6f6f6 url("{{asset('assets/front/images/banners/banner.jpg')}}") no-repeat scroll center center / cover;
        }

        .overlay-bg::before {
            background: #f6f6f6 none repeat scroll 0 0;
            content: "";
            height: 100%;
            left: 0;
            opacity: 0.3;
            position: absolute;
            top: 0;
            transition: all 0.3s ease 0s;
            width: 100%;
        }

        .breadcrumbs-title {
            color: transparent;
        }

        .breadcrumb-list > li::before {
            color: #666666;
            content: "";
        }

        .breadcrumb-list > li {
            font-size: 1.5rem;
        }

        .shop-tab, .short-by, .showing {
            width: 15.3337%;
        }

        .pro-tab-menu ul li.active a {
            background-color: rgba(228, 229, 230, 0.5) !important;
            padding: 2px 10px;
            border: none !important;
        }

        .product-item {
            border: 1px solid #f3f3f3;
            border-radius: 16px;
        }

        .product-item:hover {
            border: 1px solid #cdcdcd;
            border-radius: 16px;
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.1);
        }

    </style>

    <!-- Discount -->
    <style>
        .product-item-discount::before {
            background: rgba(0, 0, 0, 0) url(../img/bg/ribbon_blue.png) repeat scroll 0 0;
            content: "";
            height: 138px;
            left: 2px;
            position: absolute;
            top: -11px;
            width: 145px;
        }

        .ribbon-price {
            left: 40px;
            position: absolute;
            top: 29px;
            transform: rotate(-46deg);
        }

        .discount_list_view {
            margin-left: 30px;
            border: 2px solid #1a237e;
            color: #1a237e;
            padding: 5px 10px
        }

        .widget-product {
            padding-bottom: 30px;
        }

        /*.widget-product .product-item {*/
        /*box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.08);*/
        /*}*/

        /*.widget-product .product-item:hover {*/
        /*box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.15);*/
        /*}*/

    </style>

    <!-- Desktop Version -->
    <style>
        a.aside-filters, input.aside-filters {
            /*max-width: 300px;*/
            font-size: 1.8rem;
            line-height: 32px;
        }

        .product-cat .product_category_selected {
            color: #1a237e !important;
            font-size: 1.8rem;
        }

        .product-cat .fa-chevron-right {
            margin-left: 7px;
            color: #1a237e !important;
            font-size: 1.2rem;
        }

        .border-left:before {
            height: 13px;
            margin-top: -7px;
        }

        .widget {
            padding: 40px;
        }

        .widget-search input {
            padding-left: 40px;
        }

        .shop-list .product-img {
            padding-right: 0;
            width: 26%;
        }

        .product-info.recent {
            margin-top: -10px;
        }

        .shop-list.product-item {
            background: #f9f9f9 none repeat scroll 0 0;
        }

        .recent-product-img {
            width: 100%;
            height: auto;
            border: 1px solid #f0f0f0;
            border-top-left-radius: 16px;
            border-bottom-left-radius: 16px;
        }

        .product-info{
            border-bottom-left-radius: 16px;
            border-bottom-right-radius: 16px;
            padding-top: 10px;
        }

        .product-img a img.loading {
            border-top-left-radius: 16px;
            border-top-right-radius: 16px;
        }

        .pro-tab-menu ul li.active a{
            border-radius: 8px;
        }

        .shop-pagination > li > a{
            border-radius: 4px;
        }

        .shop-pagination > li > span{
            border-radius: 4px;
        }

    </style>

    <!-- Tweaks -->
    <style>
        .widget-search input {
            height: 52px;
        }

        .widget-search input[type=text] {
            border: 1px solid #f3f3f3;
            border-radius: 16px;
        }

        .box-shadow {
            box-shadow: none;
            border: 1px solid #f3f3f3;
            border-radius: 16px;
        }
    </style>

@endsection

@section('content')

    <!-- Start page content -->
    <div id="page-content" class="page-wrapper">

        <!-- SHOP SECTION START -->
        <div class="shop-section mb-80">
            <div class="plr-200">
                <div class="row">
                    <div class="col-md-9 col-md-push-3 col-sm-12 ">
                        <div class="shop-content">

                            <!-- shop-option start -->
                            <div class="shop-option box-shadow mb-30 clearfix">
                                <!-- Nav tabs -->
                                <ul class="shop-tab f-left" role="tablist">
                                    <li class="{{request('view')=='grid' || !request('view') ?'active':''}}">
                                        <a href="{{route('catalogue', ['search'=>request('search'),'min_price'=>request('min_price'),'max_price'=>request('max_price'),'category'=>request('category'),'color'=>request('color'),'style'=>request('style'),'gem'=>request('gem'),'sort'=>request('sort'),'view'=>'grid'])}}"><i class="zmdi zmdi-view-module"></i></a>
                                    </li>
                                    <li class="{{request('view')=='list'?'active':''}}">
                                        <a href="{{route('catalogue', ['search'=>request('search'),'min_price'=>request('min_price'),'max_price'=>request('max_price'),'category'=>request('category'),'color'=>request('color'),'style'=>request('style'),'gem'=>request('gem'),'sort'=>request('sort'),'view'=>'list'])}}"><i class="zmdi zmdi-view-list-alt"></i></a>
                                    </li>
                                </ul>
                                <!-- sort-by -->
                                <div class="pro-tab-menu text-right">
                                    <!-- Nav tabs -->
                                    <ul class="">
                                        <li>
                                            {{trans('front/products.sort_by')}}:
                                        </li>
                                        <li class="{{(request('sort') == 'popular') || !request('sort')?'active':''}}">
                                            <a href="{{route('catalogue', ['view'=>request('view'),'search'=>request('search'),'category'=>request('category'),'color'=>request('color'),'style'=>request('style'),'gem'=>request('gem'),'sort'=>'popular'])}}">{{trans('front/products.popular')}} </a>
                                        </li>
                                        <li class="{{(request('sort') == 'new')?'active':''}}">
                                            <a href="{{route('catalogue', ['view'=>request('view'),'search'=>request('search'),'category'=>request('category'),'color'=>request('color'),'style'=>request('style'),'gem'=>request('gem'),'sort'=>'new'])}}">{{trans('front/products.new')}} </a>
                                        </li>
                                        <li class="{{(request('sort') == 'price_up')?'active':''}}">
                                            <a href="{{route('catalogue', ['view'=>request('view'),'search'=>request('search'),'category'=>request('category'),'color'=>request('color'),'style'=>request('style'),'gem'=>request('gem'),'sort'=>'price_up'])}}">{{trans('front/products.price_up')}} </a>
                                        </li>
                                        <li class="{{(request('sort') == 'price_down')?'active':''}}">
                                            <a href="{{route('catalogue', ['view'=>request('view'),'search'=>request('search'),'category'=>request('category'),'color'=>request('color'),'style'=>request('style'),'gem'=>request('gem'),'sort'=>'price_down'])}}">{{trans('front/products.price_down')}} </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- shop-option end -->

                            <!-- Product Catalogue start -->
                            <div id="product_catalogue">
                                @include('partials.front._product_catalogue')
                            </div>
                            <!-- Product Catalogue end -->
                        </div>
                    </div>
                    <div class="col-md-3 col-md-pull-9 col-sm-12 aside-filters">
                        <!-- widget-search -->
                        <aside class="widget-search mb-30">
                            {!! Form::open(['method'=>'GET','route'=>'catalogue']) !!}
                            <input id="ajax_search" type="text" name="search" placeholder="{{trans('front/products.search_here')}}..." value="{{isset($search_field) ? $search_field : ''}}" class="aside-filters">
                            <button id="search_product" type="submit"><i class="zmdi zmdi-search"></i></button>
                            <a id="cancel_search" type="submit" hidden><i class="zmdi zmdi-close"></i></a>
                            {!! Form::close() !!}
                        </aside>
                        <!-- widget-reset-filters -->
                        @if(request('category') || request('style') || request('color') || request('sort') || request('min_price') || request('max_price') || request('gem') || request('tag') )
                            <aside class="widget box-shadow mb-30 reset-filters">
                                <h4 class="widget-title border-left mb-20">{{trans('front/products.reset_filters')}}</h4>
                                <ul class="filter-reset">
                                    @if(request('color'))
                                        <li>
                                            <a href="{{route('catalogue')}}" class="aside-filters">
                                                {{trans('front/products.color')}}: {{request('color',['view'=>request('view')])}}
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    @endif
                                    @if(request('style'))
                                        <li>
                                            <a href="{{route('catalogue')}}" class="aside-filters">
                                                @php
                                                    $style_code = request('style');
                                                    $style_temp = \App\Style::where('code',$style_code)->first();
                                                    $style_name = $style_temp ? $style_temp->name : 'style not found';
                                                @endphp
                                                {{trans('front/products.style')}}: {{$style_name}}
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    @endif
                                    @if(request('category'))
                                        <li>
                                            <a href="{{route('catalogue')}}" class="aside-filters">
                                                {{$category_name}}
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    @endif
                                    @if(request('gem'))
                                        <li>
                                            <a href="{{route('catalogue')}}" class="aside-filters">
                                                @php
                                                    $gem_code = request('gem');
                                                    $gem_temp = \App\Gem::where('code',$gem_code)->first();
                                                    $gem_name = $gem_temp ? $gem_temp->name : 'gem not found';
                                                @endphp
                                                {{trans('front/products.gem')}}: {{$gem_name}}
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    @endif
                                    @if(request('sort'))
                                        <li>
                                            <a href="{{route('catalogue', ['view'=>request('view'),'search'=>request('search'),'min_price'=>request('min_price'),'max_price'=>request('max_price'),'color'=>request('color'),'category'=>request('category'),'style'=>request('style')])}}" class="aside-filters">
                                                {{trans('front/products.sort_by_compact')}}:
                                                @if(request('sort') == 'popular')
                                                    {{str_limit(trans('front/products.popular'),14)}}
                                                @elseif(request('sort') == 'new')
                                                    {{str_limit(trans('front/products.new'),14)}}
                                                @elseif(request('sort') == 'price_up')
                                                    {{str_limit(trans('front/products.price_up'),14)}}
                                                @elseif(request('sort') == 'price_down')
                                                    {{str_limit(trans('front/products.price_down'),14)}}
                                                @elseif(request('sort') == 'bestseller')
                                                    {{str_limit(trans('front/products.bestseller'),14)}}
                                                @elseif(request('sort') == 'discount')
                                                    {{str_limit(trans('front/products.discount'),18)}}
                                                @endif
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    @endif
                                    @if(request('min_price') && request('max_price'))
                                        <li>
                                            <a href="{{route('catalogue', ['view'=>request('view'),'search'=>request('search'),'color'=>request('color'),'sort'=>request('sort'),'category'=>request('category'),'style'=>request('style')])}}" class="aside-filters">
                                                {{trans('front/products.price')}}: {{request('min_price')}} - {{request('max_price')}}
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </aside>
                        @endif
                    <!-- widget-categories -->
                        <aside class="widget widget-categories box-shadow mb-30">
                            <h4 class="widget-title border-left mb-20">{{trans('front/products.categories')}}</h4>
                            <div id="cat-treeview" class="product-cat">
                                <ul>
                                    @foreach($categories as $category)
                                        <li class="closed">
                                            <a {{ (request('category') == $category->code) ?"class=product_category_selected":'' }} href="{{route('catalogue', ['category'=>$category->code,'view'=>request('view')])}}" class="aside-filters">
                                                {{$category->name}}

                                                @if(!($category->subcategories()->count() > 0))
                                                    @if(request('category') == $category->code)
                                                        <i class="fa fa-chevron-right " aria-hidden="true"></i>
                                                    @endif
                                                @endif
                                            </a>
                                            @if($category->subcategories()->count() > 0)
                                                <ul>
                                                    <li>
                                                        <a {{ (request('category') == $category->code) ?"class=product_category_selected":'' }} href="{{route('catalogue', ['category'=>$category->code,'view'=>request('view')])}}" class="aside-filters">
                                                            {{trans('front/products.all')}} {{$category->name}}

                                                            @if(request('category') == $category->code)
                                                                <i class="fa fa-chevron-right " aria-hidden="true"></i>
                                                            @endif
                                                        </a>
                                                    </li>
                                                    @foreach($category->subcategories()->whereHas('products',function ($query){$query->where('is_visible','1'); })->get() as $subcategory)
                                                        <li>
                                                            <a {{ (request('category') == ($category->code.'_'.$subcategory->code)) ?"class=product_category_selected":'' }} href="{{route('catalogue', ['category'=>$category->code.'_'.$subcategory->code,'view'=>request('view')])}}" class="aside-filters">
                                                                {{$subcategory->name}}
                                                                @if(request('category') == ($category->code.'_'.$subcategory->code))
                                                                    <i class="fa fa-chevron-right " aria-hidden="true"></i>
                                                                @endif
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @else
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </aside>
                        <!-- shop-filter -->
                        <aside class="widget shop-filter box-shadow mb-30">
                            <h4 class="widget-title border-left mb-20">{{trans('front/products.price')}}</h4>
                            <div class="price_filter">
                                <div class="price_slider_amount">
                                    <input type="submit" value="{{trans('front/products.your_range')}} :" class="aside-filters" style="line-height: 26px"/>
                                    <input type="text" id="amount" name="price" placeholder="{{trans('front/products.add_price')}}" class="aside-filters"/>
                                </div>
                                <div id="slider-range"></div>
                            </div>
                        </aside>
                        <!-- widget-color -->
                        <aside class="widget widget-color box-shadow mb-30">
                            <h4 class="widget-title border-left mb-20">{{trans('front/products.colors')}}</h4>
                            <ul class="product-color">
                                @foreach($colors as $color)
                                    <li>
                                        <span style="background: {{$color->value}} none repeat scroll 0 0;{{($color->value == 'transparent' || $color->value ==  '#fff') ?'border: 1px solid black;':''}}"></span><a href="{{route('catalogue', ['color'=>$color->name,'view'=>request('view')])}}" class="aside-filters">{{$color->name}} </a>
                                    </li>
                                @endforeach
                            </ul>
                        </aside>
                        <!-- widget-gem -->
                        <aside class="widget widget-gem box-shadow mb-30">
                            <h4 class="widget-title border-left mb-20">{{trans('front/products.gems')}}</h4>
                            <ul class="product-gem">
                                @foreach($gems as $gem)
                                    <a class="tippy-gem" title="{{$gem->name}}" href="{{route('catalogue', ['gem'=>$gem->code,'view'=>request('view')])}}"><img src="{{$gem->image}}" style="" alt="{{$gem->name}}" class="img-thumbnail"></a>
                                @endforeach
                            </ul>
                        </aside>
                        <!-- operating-system -->
                        <aside class="widget operating-system box-shadow mb-30 product_styles">
                            <h4 class="widget-title border-left mb-20">{{trans('front/products.styles')}}</h4>
                            <ul>
                                @foreach($styles as $style)
                                    <li>
                                        <a class="aside-filters" href="{{route('catalogue', ['style'=>$style->code,'view'=>request('view')])}}">{{$style->name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                            <div style="margin-top: 15px;margin-bottom: 5px" hidden>
                                <span class="aside-filters">{{trans('front/products.show_all')}}</span>
                            </div>
                        </aside>

                        @if($recent_products)
                        <!-- widget-product -->
                            <aside class="widget widget-product box-shadow">
                                <h4 class="widget-title border-left mb-20">{{trans('front/products.recent_products')}}</h4>
                            @foreach($recent_products as $recent_product)
                                <!-- product-item start -->
                                    <div class="product-item">
                                        <div class="product-img">
                                            <a href="{{route('catalogue.product',$recent_product->id)}}">
                                                <img class="recent-product-img" src="{{$recent_product->photos()->count() > 0 ?$recent_product->photos->first()->getPreviewPath() : ''}}" alt=""/>
                                            </a>
                                        </div>
                                        <div class="product-info recent">
                                            <h4 class="product-title">
                                                <a class="aside-filters" href="{{route('catalogue.product',$recent_product->id)}}">{{$recent_product->name}}</a>
                                            </h4>
                                            <h3 class="pro-price">
                                                &euro; {{$recent_product->price}}
                                            </h3>
                                        </div>
                                    </div>
                                    <!-- product-item end -->
                                @endforeach
                            </aside>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- SHOP SECTION END -->

    </div>
    <!-- End page content -->

@endsection

@section('scripts')

    <!-- AJAX functions -->
    <script>
        $(function () {

            if ($('input[id = ajax_search]').val().length > 0) {
                $('#search_product').hide();
                $('#cancel_search').show();
            }


            if ($('.product_styles li').length != $('.product_styles li:visible').length) {
                $('.product_styles div').show();
            }

            $('input[id = ajax_search]').on("input", function () {

                var search = $(this).val();
                var data = {
                    _token: '{!! csrf_token() !!}',
                    search: search,
                    category: '{{request('category')}}',
                    style: '{{request('style')}}',
                    color: '{{request('color')}}',
                    sort: '{{request('sort')}}',
                    gem: '{{request('gem')}}',
                    view: '{{request('view')}}'
                };

                if (search.length > 0) {
                    $('#search_product').hide();
                    $('#cancel_search').show();
                } else {
                    $('#search_product').show();
                    $('#cancel_search').hide();
                }

                $.ajax({
                    'url': "{{route('catalogue.ajax')}}",
                    'method': 'POST',
                    'data': data,
                    success: function (response) {
                        var catalogue = $('#product_catalogue');
                        catalogue.html(response);
                        $('.pro-rating').each(function () {
                            var rating = $(this).attr('data-rating');
                            $(this).html(getStars(rating));
                        });
                    }
                });
            });

            var min_price = parseInt({{$min_price}}) - 1;
            var max_price = parseInt({{$max_price}}) + 1;

            var min_value_price = (parseInt({{request('min_price')}})) ? (parseInt({{request('min_price')}})) : min_price;
            var max_value_price = (parseInt({{request('max_price')}})) ? (parseInt({{request('max_price')}})) : max_price;

            $("#slider-range").slider({
                range: true,
                min: min_price,
                max: max_price,
                values: [min_value_price, max_value_price],
                slide: function (event, ui) {
                    var search = $('input[id = ajax_search]').val();
                    var sort_price = $(this).val();
                    var data = {
                        _token: '{!! csrf_token() !!}',
                        min_price: ui.values[0],
                        max_price: ui.values[1],
                        category: '{{request('category')}}',
                        style: '{{request('style')}}',
                        color: '{{request('color')}}',
                        sort: '{{request('sort')}}',
                        gem: '{{request('gem')}}',
                        view: '{{request('view')}}',
                        search: search
                    };

                    $.ajax({
                        'url': "{{route('catalogue.ajax')}}",
                        'method': 'POST',
                        'data': data,
                        success: function (response) {
                            var catalogue = $('#product_catalogue');
                            catalogue.html(response);
                            $('.pro-rating').each(function () {
                                var rating = $(this).attr('data-rating');
                                $(this).html(getStars(rating));
                            });
                        }
                    });

                    $("#amount").val("€" + ui.values[0] + " - €" + ui.values[1]);
                }
            });

            $("#amount").val("€" + $("#slider-range").slider("values", 0) + " - €" + $("#slider-range").slider("values", 1));

            $('.product_styles div').click(function () {
                $('.product_styles li:hidden').show();
                if ($('.product_styles li').length == $('.product_styles li:visible').length) {
                    $('.product_styles div').hide();
                }
            });

            $('#cancel_search').click(function (event) {
                event.preventDefault();
                $('input[id = ajax_search]').val('');

                $('#search_product').show();
                $('#cancel_search').hide();


                var data = {
                    _token: '{!! csrf_token() !!}',
                    search: ''
                };

                $.ajax({
                    'url': "{{route('catalogue.ajax')}}",
                    'method': 'POST',
                    'data': data,
                    success: function (response) {
                        var catalogue = $('#product_catalogue');
                        catalogue.html(response);
                        $('.pro-rating').each(function () {
                            var rating = $(this).attr('data-rating');
                            $(this).html(getStars(rating));
                        });
                    }
                });
            });

            // product cart preview:
            $('#product_catalogue').on('click', 'a.cart_product', function () {

                var product_id = $(this).attr('data-product');
                var data = {
                    _token: '{!! csrf_token() !!}',
                    product_id: product_id
                }

                $.ajax({
                    'url': "{{route('catalogue.product.preview.ajax')}}",
                    'data': data,
                    'method': 'POST',
                    success: function (response) {
                        var cart_preview = $('.modal-product');
                        cart_preview.html(response);
                    }
                });
            });
        });
    </script>

@endsection
