@extends('layouts.front')

@section('title',"Silverest | Warranty")

@section('styles')

    <!-- Custom Styling -->
    <style>
        .subcribe .submit-btn-2 {
            border-radius: 0px;
        }

        .up-comming-pro-img img {
            border: 1px solid #f1f1f1;
        }

        .banner-img img {
            border: 1px solid #e1e1e1;
        }

        .gray-bg {
            background: #f6f6f6;
        }

        .slider-img img {
            display: block;
            max-width: 400px;
            max-height: 400px;
            min-width: 400px;
            min-height: 400px;
            border: 1px solid #f1f3f4;
        }

        .slider-info {
            width: 65%;
        }

        @media (min-width: 1441px) and (max-width: 1920px) {
            .slider-info {
                width: 75%;
            }
        }

        .slick-arrow-2 .arrow-next.slick-arrow,.slick-arrow-2 .arrow-prev.slick-arrow {
            border-radius: 0px;
        }

        .product-item-2 img,.product-item-2 .product-info,.product-item-2 .action-button{
            border-radius: 0px;
            box-shadow: 0px 2px 2px 2px rgba(0, 0, 0, 0.05);
        }


    </style>

    <!-- Style Tweaks -->
    <style>
        #cancel_search:hover {
            color: #1a237e;
        }

        .section-bg-tb::before {
            transform: skewY(0deg);
        }

        .breadcrumbs {
            background: #f6f6f6 url("{{asset('assets/front/images/banners/banner.jpg')}}") no-repeat scroll center center / cover;
        }

        .overlay-bg::before {
            background: #f6f6f6 none repeat scroll 0 0;
            content: "";
            height: 100%;
            left: 0;
            opacity: 0.3;
            position: absolute;
            top: 0;
            transition: all 0.3s ease 0s;
            width: 100%;
        }

        .breadcrumbs-title {
            color: transparent;
        }

        .breadcrumb-list > li::before {
            color: #666666;
            content: "";
        }

        .breadcrumb-list > li {
            font-size: 1.5rem;
        }

        .qtybutton {
            cursor: default;
        }

        .widget-color li span {
            border-radius: 50%;
            content: "";
            height: 12px;
            left: 0;
            margin-top: -6px;
            position: absolute;
            top: 50%;
            width: 12px;
        }

        .widget-color ul li::before {
            background: transparent;
            border-radius: 50%;
            content: "";
            height: 12px;
            left: 0;
            margin-top: -6px;
            position: absolute;
            top: 50%;
            width: 12px;
        }

        a.button.button-black span, a.button span {
            z-index: 0;
        }

        #zoom_03 {
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.25);
        }

        .p-c a.active img {
            border: none;
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.35);
        }

        .fancybox-wrap.fancybox-desktop.fancybox-type-image.fancybox-opened {
            z-index: 100000;
        }

        .slick-arrow-2 .arrow-next.slick-arrow, .slick-arrow-2 .arrow-prev.slick-arrow {
            border-radius: 0px;
        }

        .product-item {
            margin-top: 5px;
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.15);
        }

        .product-item:hover {
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.25);
        }
    </style>

@endsection

@section('content')

    <div class="breadcrumbs-section plr-200 mb-50">
        <div class="breadcrumbs overlay-bg">
            <div class="p-20-40">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs-inner">
                            <h1 class="breadcrumbs-title">Warranty</h1>
                            <ul class="breadcrumb-list">
                                <li><a href="{{route('root')}}">{{trans('front/warranty.home')}}</a></li>
                                <li>{{trans('front/warranty.warranty')}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- START PAGE CONTENT -->
    <section id="page-content" class="page-wrapper">

        <!-- PRODUCT TAB SECTION START -->
        <div class="product-tab-section section-bg-tb pt-80 pb-55" style="margin-bottom: 0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title text-left mb-40">
                            <h2 class="uppercase">{{trans('front/warranty.warranty')}}</h2>
                            <div class="reviews-tab-desc mt-40">
                                <h3 style="margin-top: 40px;color: #1a237e">{{trans('front/warranty.warranty_1')}} <br>{{trans('front/warranty.warranty_2')}}</h3>
                                <h3 style="margin-top: 40px;color: #1a237e">{{trans('front/warranty.warranty_3')}}</h3>
                                <p style="text-align: justify;font-size: 1.8rem" class="mt-20">{{trans('front/warranty.warranty_4')}}</p>
                                <p style="text-align: justify;font-size: 1.8rem" class="mt-20">{{trans('front/warranty.warranty_5')}}</p>
                                <h3 style="margin-top: 40px;color: #1a237e">{{trans('front/warranty.warranty_6')}}</h3>
                                <p style="text-align: justify;font-size: 1.8rem" class="mt-20">{{trans('front/warranty.warranty_7')}}</p>
                                <h3 style="margin-top: 40px;color: #1a237e">{{trans('front/warranty.warranty_8')}}</h3>
                                <p style="text-align: justify;font-size: 1.8rem" class="mt-20">{{trans('front/warranty.warranty_9')}}</p>
                                <h3 style="margin-top: 40px;color: #1a237e">{{trans('front/warranty.warranty_10')}}</h3>
                                <p style="text-align: justify;font-size: 1.8rem" class="mt-20">{{trans('front/warranty.warranty_11')}}</p>
                                <h3 style="margin-top: 40px;color: #1a237e">{{trans('front/warranty.warranty_12')}}</h3>
                                <p style="text-align: justify;font-size: 1.8rem" class="mt-20">{{trans('front/warranty.warranty_13')}}</p>
                                <p style="text-align: justify;font-size: 1.8rem" class="mt-20">{{trans('front/warranty.warranty_14')}}</p>
                                <p style="text-align: justify;font-size: 1.8rem" class="mt-20">{{trans('front/warranty.warranty_15')}}</p>
                                <p style="text-align: justify;font-size: 1.8rem" class="mt-20">{{trans('front/warranty.warranty_16')}}</p>
                                <h3 style="margin-top: 40px;color: #1a237e">{{trans('front/warranty.warranty_17')}}</h3>
                                <p style="text-align: justify;font-size: 1.8rem" class="mt-20">{{trans('front/warranty.warranty_18')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- PRODUCT TAB SECTION END -->

    </section>
    <!-- END PAGE CONTENT -->

@endsection

@section('scripts')

    <!-- Javascript files -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuU_0_uLMnFM-2oWod_fzC0atPZj7dHlU"></script>
    <script src="{{asset('assets/front/js/map.js')}}"></script>

@endsection
