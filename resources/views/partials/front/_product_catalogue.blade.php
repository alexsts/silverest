<div class="tab-content">

@if($products && $products->count() > 0)
    @if(request('view')=='list')
        <!-- list-view -->
            <div>
                <div class="row">
                    <!-- product-item start -->
                    @foreach($products as $product)
                        <div class="col-md-12">
                            <div class="shop-list product-item">
                                <div class="product-img">
                                    <a href="{{route('catalogue.product',$product->id)}}">
                                        @if($product->photos->count() > 0)
                                            <img class="loading" src="{{$product->photos->first()->getPreviewPath()}}" style="width: 100%;height: 28rem" alt="">
                                        @else
                                            <img class="loading" src="{{asset('img/product/7.jpg')}}" style="width: 100% ; height: 28rem" alt=""/>
                                        @endif
                                    </a>
                                </div>
                                <div class="product-info">
                                    <div class="clearfix">
                                        <h6 class="product-title f-left">
                                            <a style="font-size: 18px" href="{{route('catalogue.product',$product->id)}}">{{$product->name}} &nbsp;&nbsp;&nbsp;&nbsp;</a>
                                        </h6>
                                    </div>
                                    <h6 class="brand-name mb-20" style="font-size: 18px; color: #1a237e;font-style: normal;">{{$product->code}}</h6>
                                    <div class="row s-price-box" style="margin: 0;padding: 15px 0">
                                        <div class="col-md-6" style="padding-left: 0">
                                            <div class="sin-pro-color f-left">
                                                <p class="color-title f-left" style="margin-bottom: 0px;margin-top: 2px;">Material: </p>
                                                <div class="f-left" style="font-size: 16px;margin-top: 2px">
                                                    &nbsp;&nbsp;&nbsp;{{$product->material->name}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6" style="padding-right: 0">
                                            @if($product->gems->count() > 0)
                                                <div class="f-right" style="margin-right: 15px;margin-top: -3px">
                                                    <p class="color-title f-left" style="margin-top: 4px;margin-bottom: 0px">Gem: </p>
                                                    <div class="f-left">
                                                        <ul class="product-gem" style="    margin-bottom: -4px;">
                                                            @foreach($product->gems->take(3) as $gem)
                                                                <img src="{{$gem->image}}" alt="{{$gem->name}}" class="img-thumbnail tippy-gem" title="{{$gem->name}}" style="margin-bottom: 0px;height: 35px; width: 35px;">
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="s-price-box mb-15">
                                        @if($product->getDiscountPrice())
                                            <span class="new-price">&euro; {{number_format ($product->getDiscountPrice(),2)}}</span>
                                            <span class="old-price">&euro; {{number_format ($product->price,2)}}</span>
                                            <span class="discount_list_view">- {{$product->discount_rate}} %</span>
                                        @else
                                            <span class="new-price">&euro; {{number_format ($product->price,2)}}</span>
                                        @endif
                                    </div>

                                    <ul class="action-button clearfix">
                                        <li>
                                            {!! Form::open(['method'=>'POST','route'=>['account.wishlist.change']]) !!}
                                            <input type="hidden" name="product_id" value="{{$product->id}}">
                                            <button class="cart_wishlist {{(Auth::check() && Auth::user()->wishlists->contains($product->id))?'active':''}} tippy" title="{{trans('front/products.add_to_wishlist')}}" data-product="{{$product->id}}">
                                                <i class="zmdi zmdi-favorite"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        </li>
                                        <li>
                                            <a class="cart_product tippy" data-toggle="modal" data-target="#productModal" title="{{trans('front/products.add_to_cart')}}" data-product="{{$product->id}}"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                        </li>
                                        @if(Auth::check())
                                            @role('superadministrator|administrator|editor')
                                            <li>
                                                <a class="cart_product tippy" title="{{trans('front/products.edit_product')}}" href="{{route('products.edit',$product->id)}}"><i class="zmdi zmdi-edit"></i></a>
                                            </li>
                                            @endrole
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- product-item end -->
                    @endforeach
                </div>
            </div>
    @else
        <!-- grid-view -->
            <div>
                <div class="row">
                @foreach($products as $product)
                    <!-- product-item start -->
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <div class="product-item {{($product->getDiscountPrice())?'product-item-discount':''}}">
                                @if($product->getDiscountPrice())
                                    <div class="ribbon-price">
                                        <span>-&nbsp;{{$product->discount_rate < 10?'&nbsp;':''}}{{$product->discount_rate}}%</span>
                                    </div>
                                @endif
                                <div class="product-img">
                                    <a href="{{route('catalogue.product',$product->id)}}">
                                        @if($product->photos->count() > 0)
                                            <img class="loading" src="{{$product->photos->first()->getPreviewPath()}}" alt="">
                                        @else
                                            <img class="loading" src="{{asset('img/product/7.jpg')}}" style="width: 100% ; height: 28rem" alt=""/>
                                        @endif
                                    </a>
                                </div>
                                <div class="product-info">
                                    <h6 class="product-title" style="font-size: 18px">
                                        <a href="{{route('catalogue.product',$product->id)}}">{{$product->name}} - <span style="font-family:sans-serif;color: #1a237e">{{$product->code}}</span></a>
                                    </h6>
                                    <div class="s-price-box">
                                        @if($product->getDiscountPrice())
                                            <span class="new-price">&euro; {{number_format ($product->getDiscountPrice(),2)}}</span>
                                            <span class="old-price">&euro; {{number_format ($product->price,2)}}</span>
                                        @else
                                            <span class="new-price">&euro; {{number_format ($product->price,2)}}</span>
                                        @endif
                                    </div>

                                    <ul class="action-button">
                                        <li>
                                            {!! Form::open(['method'=>'POST','route'=>['account.wishlist.change']]) !!}
                                            <input type="hidden" name="product_id" value="{{$product->id}}">
                                            <button class="cart_wishlist {{(Auth::check() && Auth::user()->wishlists->contains($product->id))?'active':''}} tippy" title="{{trans('front/products.add_to_wishlist')}}" data-product="{{$product->id}}">
                                                <i class="zmdi zmdi-favorite"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        </li>
                                        <li>
                                            <a class="cart_product tippy" title="{{trans('front/products.add_to_cart')}}" data-toggle="modal" data-target="#productModal" data-product="{{$product->id}}"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                        </li>
                                        @if(Auth::check())
                                            @role('superadministrator|administrator|editor')
                                            <li>
                                                <a class="cart_product tippy" title="{{trans('front/products.edit_product')}}" href="{{route('products.edit',$product->id)}}"><i class="zmdi zmdi-edit"></i></a>
                                            </li>
                                            @endrole
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- product-item end -->
                    @endforeach
                </div>
            </div>
        @endif
    @else
        <div class="shop-option box-shadow mb-30 clearfix">
            <h2 class="mt-10" style="text-align: center">{{trans('front/products.no_results')}}</h2>
        </div>
    @endif
</div>
<!-- Tab Content end -->
<!-- shop-pagination start -->
{{$products->links('vendor.pagination.front')}}
