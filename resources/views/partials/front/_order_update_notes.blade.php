{!! Form::open(['method'=>'POST','route'=>'account.order.notes.update']) !!}

<div class="note-info">
    <h3>{{trans('front/orders.order')}}: #{{$order->id}}</h3>
    <input type="hidden" name="order_id" value="{{$order->id}}">

    <div class="row">
        <div class="col-md-12 mt-20">
            <p style="font-size: 16px;color:#5d5d5d"> {{trans('front/orders.please_leave')}}</p>
        </div>
        <div class="col-md-12">
            <textarea id="note-textarea" name="notes" style="font-size: 17px !important"> {{$order->notes}}</textarea>
        </div>
        <div class="col-md-12">
            <div class="quick-add-to-cart">
                <button class="single_add_to_cart_button" type="submit">{{trans('front/orders.submit')}}</button>
            </div>
        </div>
    </div>


</div><!-- .product-info -->

{!! Form::close() !!}
