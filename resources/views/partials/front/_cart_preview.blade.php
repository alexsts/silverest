<div class="product-images">
    <div class="main-image images">
        <img class="loading" src="{{$cart_product->photos()->count() > 0 ? $cart_product->photos->first()->getPhotoPath() : '' }}">
    </div>
</div><!-- .product-images -->

{!! Form::open(['method'=>'POST','route'=>'account.cart.add','class'=>'form-horizontal']) !!}

<div class="product-info">
    <h1>{{$cart_product->name}} ({{$cart_product->code}})</h1>
    <div class="price-box-3">
        <div class="s-price-box">
            @if($cart_product->getDiscountPrice())
                <span class="new-price">&euro; {{$cart_product->getDiscountPrice()}}</span>
                <span class="old-price">&euro; {{$cart_product->price}}</span>
            @else
                <span class="new-price">&euro; {{$cart_product->price}}</span>
            @endif
        </div>
    </div>
    <input type="hidden" name="product_id" value="{{$cart_product->id}}">
    @if($sizes->count() > 0)
        <div class="single-pro-color-rating sizes-border clearfix">
            <div class="sin-pro-color">
                <p class="color-title f-left" style="font-weight: 600;margin-right: 12px">{{trans('front/cart.sizes')}}: </p>
                <div class="sizes-list f-left">
                    <ul>
                        @foreach($sizes as $size)
                            <li>
                                <input type="radio" name="size" id="size_{{$size->id}}" value="{{$size->value}}"/>
                                <label for="size_{{$size->id}}"><span>{{ ( ($size->value < 10)||(strlen(strval($size->value))) < 3 )  ? '&nbsp;'.$size->value : $size->value}}</span></label>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
    <div class="row s-price-box" style="margin: 0;padding: 15px 0;border-top: none;    border-bottom: 1px solid #e5e5e5;">
        <div class="col-md-6" style="padding-left: 0">
            <div class="sin-pro-color f-left">
                <p class="color-title f-left" style="margin-bottom: 0px;margin-top: 2px;">{{trans('front/cart.material')}}:</p>
                <div class="f-left" style="font-size: 16px;margin-top: 2px">
                    &nbsp;&nbsp;&nbsp;{{$cart_product->material->name}}
                </div>
            </div>
        </div>
        <div class="col-md-6" style="padding-right: 0">
            @if($cart_product->gems->count() > 0)
                <div class="f-right" style="margin-right: 15px;margin-top: -3px">
                    <p class="color-title f-left" style="margin-top: 4px;margin-bottom: 0px">{{trans('front/cart.gems')}}: </p>
                    <div class="f-left">
                        <ul class="product-gem" style="    margin-bottom: -4px;">
                            @foreach($cart_product->gems->take(3) as $gem)
                                <img src="{{$gem->image}}" alt="{{$gem->name}}" class="img-thumbnail tippy-gem" title="{{$gem->name}}" style="margin-bottom: 0px;height: 35px; width: 35px;">
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="row s-price-box" style="margin: 0;padding: 15px 0;border-top: none;    border-bottom: 1px solid #e5e5e5;">
        <div class="col-md-6" style="padding-left: 0">
            <div class="sin-pro-color f-left">
                <p class="color-title f-left" style="margin-bottom: 0px;margin-top: 2px;">{{trans('front/cart.weight')}}: </p>
                <div class="f-left" style="font-size: 16px;margin-top: 2px">
                    &nbsp;&nbsp;&nbsp;{{$cart_product->weight}} {{trans('front/cart.grams')}}
                </div>
            </div>
        </div>
        <div class="col-md-6" style="padding-right: 0">

        </div>
    </div>
    <div class="quick-add-to-cart" style="margin-bottom: -15px; margin-top: 20px;">
        <div class="numbers-row">
            <input type="number" name="quantity" id="french-hens" value="1" min="0" max="{{$cart_product->quantity}}">
        </div>
        <button class="single_add_to_cart_button" type="submit">{{trans('front/cart.add_to_cart')}}</button>
    </div>
</div><!-- .product-info -->

{!! Form::close() !!}
