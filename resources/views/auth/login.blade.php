@extends('layouts.front')

@section('styles')

    <style>
        #cancel_search:hover {
            color: #1a237e;
        }

        .breadcrumbs {
            background: #f6f6f6 url("{{asset('assets/front/images/banners/banner.jpg')}}") no-repeat scroll center center / cover;
        }

        .overlay-bg::before {
            background: #f6f6f6 none repeat scroll 0 0;
            content: "";
            height: 100%;
            left: 0;
            opacity: 0.3;
            position: absolute;
            top: 0;
            transition: all 0.3s ease 0s;
            width: 100%;
        }

        .breadcrumbs-title {
            color: transparent;
        }

        .breadcrumb-list > li::before {
            color: #666666;
            content: "";
        }

        .plr-200 {
            padding-left: 150px;
            padding-right: 150px;
        }

        .breadcrumb-list > li {
            font-size: 1.5rem;
        }

        .error-fieldset-login{
            padding: 10px 20px;
            margin: 20px 0px;
            border: 1px solid red;
        }
    </style>

@endsection

@section('content')

    <!-- BREADCRUMBS SECTION START -->
    <div class="breadcrumbs-section plr-200 mb-80">
        <div class="breadcrumbs overlay-bg">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs-inner">
                            <h1 class="breadcrumbs-title">{{trans('front/auth.login')}}</h1>
                            <ul class="breadcrumb-list">
                                <li><a href="{{route('root')}}">{{trans('front/auth.home')}}</a></li>
                                <li>{{trans('front/auth.login')}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BREADCRUMBS SECTION END -->

    <!-- PAGE CONTENT START -->
    <div id="page-content" class="page-wrapper">

        <!-- LOGIN SECTION START -->
        <div class="login-section mb-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-3 col-md-6">
                        <div class="registered-customers">
                            <h6 class="widget-title border-left mb-50">{{trans('front/auth.registered_customers')}}</h6>


                            <form method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="login-account p-30 box-shadow">
                                    <p style="font-size: 16px">{{trans('front/auth.if_account')}}</p>
                                    @if($errors->count())
                                        <div class="error-fieldset-login">
                                            @foreach ($errors->all() as $error)
                                                <div style="color: red">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    @endif
                                    <div class="form-group float-label {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input type="text" name="email" id="email"  value="{{ old('email') }}" required >
                                        <label for="email" style="color: {{$errors->has('email') ? 'red':''}}">{{trans('front/auth.email')}}</label>
                                    </div>
                                    <div class="form-group float-label {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <input type="password" name="password" id="password" required autocomplete="false">
                                        <label for="password" style="color: {{$errors->has('password') ? 'red':''}}">{{trans('front/auth.password')}}</label>
                                    </div>
                                    <div class="checkbox">
                                        <label class="mr-10">
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>&nbsp;&nbsp; {{trans('front/auth.remember')}}
                                        </label>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button class="submit-btn-1 mt-20 btn-hover-1" type="submit" value="register">{{trans('front/auth.login')}}</button>
                                        </div>
                                    </div>
                                    <p>
                                        <a class="btn btn-link" href="{{ route('password.request') }}" style="padding: 0px; margin-top: 20px">{{trans('front/auth.forgot')}}?</a>
                                    </p>
                                </div>
                            </form>
                        </div>

                        <h4 style="text-align: center;margin-top: 20px">
                            <a href="{{ route('register') }}">{{trans('front/auth.create')}}</a>
                        </h4>

                    </div>
                </div>
            </div>
        </div>
        <!-- LOGIN SECTION END -->
    </div>
    <!-- PAGE CONTENT END -->

@endsection

@section('sripts')

    <script>
        $(document).ready(function () {
            // Floating Labels
            //==============================================================
            $('[data-toggle="floatLabel"]').attr('data-value', $(this).val()).on('keyup change', function () {
                $(this).attr('data-value', $(this).val());
            });
        });
    </script>

@endsection