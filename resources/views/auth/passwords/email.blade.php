@extends('layouts.front')

@section('styles')

    <style>
        #cancel_search:hover {
            color: #1a237e;
        }

        .breadcrumbs {
            background: #f6f6f6 url("{{asset('assets/front/images/banners/banner.jpg')}}") no-repeat scroll center center / cover;
        }

        .overlay-bg::before {
            background: #f6f6f6 none repeat scroll 0 0;
            content: "";
            height: 100%;
            left: 0;
            opacity: 0.3;
            position: absolute;
            top: 0;
            transition: all 0.3s ease 0s;
            width: 100%;
        }

        .breadcrumbs-title {
            color: transparent;
        }

        .breadcrumb-list > li::before {
            color: #666666;
            content: "";
        }

        .plr-200 {
            padding-left: 150px;
            padding-right: 150px;
        }

        .breadcrumb-list > li {
            font-size: 1.5rem;
        }

        .error-fieldset-reset{
            padding: 10px 20px;
            margin: 20px 0px;
            border: 1px solid red;
        }
    </style>

@endsection

@section('content')

    <!-- BREADCRUMBS SECTION START -->
    <div class="breadcrumbs-section plr-200 mb-80">
        <div class="breadcrumbs overlay-bg">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs-inner">
                            <h1 class="breadcrumbs-title">{{trans('front/auth.reset_password')}}</h1>
                            <ul class="breadcrumb-list">
                                <li><a href="{{route('root')}}">{{trans('front/auth.home')}}</a></li>
                                <li>{{trans('front/auth.reset_password')}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BREADCRUMBS SECTION END -->

    <!-- PAGE CONTENT START -->
    <div id="page-content" class="page-wrapper">

        <!-- LOGIN SECTION START -->
        <div class="login-section mb-80">
            <div class="container">
                <div class="row">
                    <!-- new-customers -->
                    <div class="col-md-offset-3 col-md-6">
                        <div class="new-customers">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form method="POST" action="{{ route('password.email') }}">
                                {{ csrf_field() }}
                                <h6 class="widget-title border-left mb-50">{{trans('front/auth.reset_password_up')}}</h6>
                                <div class="login-account p-30 box-shadow">
                                    <p style="font-size: 16px">{{trans('front/auth.if_send')}}</p>

                                    @if($errors->count())
                                        <div class="error-fieldset-reset">
                                            @foreach ($errors->all() as $error)
                                                <div style="color: red">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    @endif

                                    <div class="form-group float-label {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input id="email" type="text" name="email" value="{{ old('email') }}" required>
                                        <label for="email" style="color: {{$errors->has('email') ? 'red':''}}">{{trans('front/auth.email')}}</label>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button class="submit-btn-1 mt-20 btn-hover-1" type="submit" value="register">{{trans('front/auth.send')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <h4 style="text-align: center;margin-top: 20px">
                            <a href="{{ route('login') }}">{{trans('front/auth.back_to_login')}}</a>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- LOGIN SECTION END -->

    </div>
    <!-- PAGE CONTENT END -->

@endsection

