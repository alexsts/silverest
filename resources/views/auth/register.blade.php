@extends('layouts.front')

@section('styles')

    <style>
        #cancel_search:hover {
            color: #1a237e;
        }

        .breadcrumbs {
            background: #f6f6f6 url("{{asset('assets/front/images/banners/banner.jpg')}}") no-repeat scroll center center / cover;
        }

        .overlay-bg::before {
            background: #f6f6f6 none repeat scroll 0 0;
            content: "";
            height: 100%;
            left: 0;
            opacity: 0.3;
            position: absolute;
            top: 0;
            transition: all 0.3s ease 0s;
            width: 100%;
        }

        .breadcrumbs-title {
            color: transparent;
        }

        .breadcrumb-list > li::before {
            color: #666666;
            content: "";
        }

        .plr-200 {
            padding-left: 150px;
            padding-right: 150px;
        }

        .breadcrumb-list > li {
            font-size: 1.5rem;
        }
        .error-fieldset-register{
            padding: 10px 20px;
            margin: 20px 0px;
            border: 1px solid red;
        }
    </style>

@endsection

@section('content')

    <!-- BREADCRUMBS SETCTION START -->
    <div class="breadcrumbs-section plr-200 mb-80">
        <div class="breadcrumbs overlay-bg">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="breadcrumbs-inner">
                            <h1 class="breadcrumbs-title">{{trans('front/auth.register')}}</h1>
                            <ul class="breadcrumb-list">
                                <li><a href="{{route('root')}}">{{trans('front/auth.home')}}</a></li>
                                <li>{{trans('front/auth.register')}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BREADCRUMBS SETCTION END -->

    <!-- PAGE CONTENT START -->
    <div id="page-content" class="page-wrapper">

        <!-- LOGIN SECTION START -->
        <div class="login-section mb-80">
            <div class="container">
                <div class="row">
                    <!-- new-customers -->
                    <div class="col-md-offset-3 col-md-6">
                        <div class="new-customers">
                            <form method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}
                                <h6 class="widget-title border-left mb-50">{{trans('front/auth.new_customers')}}</h6>
                                <div class="login-account p-30 box-shadow">
                                    <p style="font-size: 16px">{{trans('front/auth.create')}}</p>
                                    @if($errors->count())
                                        <div class="error-fieldset-register">
                                            @foreach ($errors->all() as $error)
                                                <div style="color: red">{{ $error }}</div>
                                            @endforeach
                                        </div>
                                    @endif
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}" style="margin-bottom: 0">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="float-label">
                                                    <input id="first_name" type="text" name="first_name" value="{{ old('first_name') }}" required>
                                                    <label for="first_name" style="color: {{$errors->has('first_name') ? 'red':''}}">{{trans('front/auth.f_name')}}</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="float-label">
                                                    <input id="last_name" type="text" name="last_name" value="{{ old('last_name') }}" required>
                                                    <label for="last_name" style="color: {{$errors->has('last_name') ? 'red':''}}">{{trans('front/auth.l_name')}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group float-label {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input id="email" type="text" name="email" value="{{ old('email') }}" required>
                                        <label for="email" style="color: {{$errors->has('email') ? 'red':''}}">{{trans('front/auth.email')}}</label>
                                    </div>

                                    <div class="form-group float-label {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <input id="password" type="password" name="password" required>
                                        <label for="password" style="color: {{$errors->has('password') ? 'red':''}}">{{trans('front/auth.password')}}</label>
                                    </div>

                                    <div class="form-group float-label {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <input id="password-confirm" type="password" name="password_confirmation"   required>
                                        <label for="password-confirm" style="color: {{$errors->has('password_confirmation') ? 'red':''}}">{{trans('front/auth.confirm_password')}}</label>
                                    </div>

                                    <div class="g-recaptcha" data-sitekey="6Lc1d3EaAAAAABTInQi4HbPzlIvyNn8rV-JHpHlT"></div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <button class="submit-btn-1 mt-20 btn-hover-1 " type="reset">{{trans('front/auth.clear')}}</button>
                                        </div>
                                        <div class="col-md-6">
                                            <button class="submit-btn-1 mt-20 btn-hover-1 f-right" type="submit" value="register">{{trans('front/auth.register')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <h4 style="text-align: center;margin-top: 20px">
                            <a href="{{ route('login') }}">{{trans('front/auth.already_account')}}?</a>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- LOGIN SECTION END -->

    </div>
    <!-- PAGE CONTENT END -->

@endsection


