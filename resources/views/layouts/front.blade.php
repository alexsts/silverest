<!doctype html>
<html class="no-js" lang="en">

<head>
    <title>@yield('title','Silverest')</title>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Silverest manufacture high quality silver and gold products with natural stones">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Ювелирные украшения с натуральными  камнями,    ювелирные украшения с камнями,    изделия из драгоценных металлов в Эстонии,    изготовитель золотых и серебряных украшений в Эстонии,    украшения из золота и серебра,    магазин ювелирных украшений Эстония,    ювелирные изделия с натуральными  камнями,    ювелирные магазины Эстонии,    ЭКСКЛЮЗИВНЫЕ УКРАШЕНИЯ,    Подарочная коробочка,    Украшения из натурального жемчуга,    Серебряные кольца с натуральным камнем,    Серебряные серьги с натуральным камнем,    Серебряные комплекты с натуральными камнями в Эстонии,    Серебряные кольца, серьги, браслеты, кулоны  925 пробы,    Большой Ассортимент Украшений  с натуральными  камнями,    изготовление ювелирных изделий на заказ,    Кольца любых размеров на заказ,    уникальные серебряные украшения ручной работы,    ювелирный интернет магазин,    интернет магазин ювелирных украшений с  натуральными  камнями,    Украшения для женщин,    Большой ассортимент качественных ювелирных изделий на любой случай,    Ehted looduslike kividega,    Ehted kividega,    väärismetalltooted Eestis,    kuld- ja hõbeehete tootja Eestis,    kuld ja hõbe ehted,    Ehtedepood Eesti,    ehted looduslike kividega,    Ehted kauplustes Eestis,    EXCLUSIVE JEWELRY,    Kinkekarp,    Mageveepärlitest valmistatud ehted,    Hõbe rõngad looduskiviga,    Hõbedased kõrvarõngad loodusliku kiviga,    Hõbedakomplektid looduslike kividega Eestis,    Hõbe rõngad, kõrvarõngad, käevõrud, ripatsid 925 proovi,    Suur valik ehteid looduslike kividega,    ehteid tellima,    Mistahes suurusega rõngad tellimiseks,    unikaalne käsitsi valmistatud hõbe ehted,    Ehted e-poes,    ehted e-poest looduslike kividega,    Ehted naistele,    Laia valikut kvaliteetseid ehteid iga kord,    Jewelry with natural stones,    jewelry with stones,    precious metal products in Estonia,    manufacturer of gold and silver jewelry in Estonia,    gold and silver jewelry,    jewelry shop Estonia,    jewelry with natural stones,    jewelry stores in Estonia,    Gift box,    Jewelry made from freshwater pearls,    Silver rings with natural stone,    Silver earrings with natural stone,    Silver sets with natural stones in Estonia,    Silver rings, earrings, bracelets, pendants 925 samples,    Large Assortment of Jewelry with natural stones,    making jewelry to order,    Rings of any sizes to order,    unique handmade silver jewelry,    jewelry online store,    online jewelry store with natural stones,    Jewelry for women,    A wide range of quality jewelry for any occasion">
    <meta name="robots" content="index, follow">
    <meta name="language" content="English">
    <meta name="author" content="Silverest">

    <!-- CSS Files -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/front/img/icon/favicon_blue.png')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/lib/css/nivo-slider.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/core.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/shortcode/shortcodes.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}">
    <link href="{{asset('assets/front/css/color/color-core.css')}}" data-style="styles" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/front/css/custom.css')}}">
    <link href="{{asset('assets/admin/css/font-awesome.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.0.0/css/flag-icon.css"/>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- Loading Image -->
    <style>
        img.loading {
            background: transparent url({{asset('images/gifs/loading.gif')}}) center no-repeat;
        }
    </style>

    <!-- Menu -->
    <style>
        .single-mega-item li a {
            margin-bottom: 10px;
            font-size: 20px;
        }

        .single-mega-item .menu-title {
            font-size: 32px;
            margin-bottom: 20px;
        }

        .mega-menu-photo img {
            height: 300px;
            width: 300px;
            box-shadow: 0px 2px 2px 2px rgba(0, 0, 0, 0.04);
        }

        .categories .fa-chevron-right {
            margin-left: 10px;
            font-size: 14px;
            color: #1a237e;
        }

        .top-search-box > input {
            padding-left: 30px;
        }

        .sticky .logo, .sticky .logo img {
            padding-top: 9px;
        }

        #primary-menu {
            padding-top: 4px;
        }

        .search-top-cart {
            padding-top: 3px;
        }

        .slick-slide.slick-active {
            outline: none !important;
        }

        .cart_product_dropdown {
            width: 100px;
            height: 111px;
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.15);
        }

        .modal {
            z-index: 1000000;
        }

        .main-image img {
            border: 1px solid #e9e9e9;
        }

        .discount_cart_view {
            margin-left: 5px;
            border: 2px solid #1a237e;
            color: #1a237e;
            padding: 2px 2px;
            width: 46px !important;
        }

    </style>

    <!-- Modal Cart Add -->
    <style>
        .sizes-list ul {
            list-style-type: none;
        }

        .sizes-list li {
            display: inline-block;
        }

        .sizes-list input[type="radio"][id^="size_"] {
            display: none;
        }

        .sizes-list label:hover {
            border-color: #1a237e;
        }

        .sizes-list label {
            border: 1px solid #dddddd;
            padding: 1px 11px;
            border-width: 2px;
            color: #000;
            font-family: arial;
            display: block;
            position: relative;
            cursor: pointer;
            width: 54px;
            height: 30px;
            margin-right: 6px;
            margin-bottom: 0px;
        }

        .sizes-list label:before {
            background-color: white;
            color: white;
            content: " ";
            display: block;
            border-radius: 50%;
            border: 1px solid grey;
            position: absolute;
            top: -5px;
            left: -5px;
            width: 25px;
            height: 25px;
            text-align: center;
            line-height: 28px;
            transition-duration: 0.4s;
            transform: scale(0);
        }

        .sizes-list label span {
            height: 94px;
            width: 94px;
            transition-duration: 0.2s;
            transform-origin: 50% 50%;
        }

        .sizes-list :checked + label {
            border-color: #1a237e;
        }

        .sizes-list :checked + label:before {
            /*content: "✓";*/
            /*background-color: grey;*/
            /*transform: scale(1);*/
        }

        .sizes-list :checked + label span {
            /*transform: scale(0.9);*/
            /*box-shadow: 0 0 1px #333;*/
            /*z-index: -1;*/
        }

        .sizes-list ul li {
            display: inline-block;
        }

        .sizes-border {
            border-bottom: 1px solid #e5e5e5;
            display: block;
            padding: 15px 0;
        }

        .price-box-3 .s-price-box {
            border-bottom: 1px solid #e5e5e5;
        }

        .user-greetings {
            height: 40px;
            line-height: 30px;
            padding: 5px 20px;
            color: #666666;
            font-size: 15px;
            display: block;
        }

        .shop-pagination > li > span {
            border: 1px solid #767676;
            color: #999999;
            display: block;
            font-family: roboto;
            font-size: 13px;
            font-weight: 400;
            height: 30px;
            line-height: 30px;
            text-align: center;
            width: 30px;
        }

        .shop-pagination > li > a {
            border: 1px solid #b9b9b9;
        }

        button.cart_wishlist {
            background: transparent;
            border: 1px solid #ddd;
            border-radius: 50%;
            color: #999999;
            display: block;
            font-size: 14px;
            height: 30px;
            line-height: 27px;
            text-align: center;
            width: 30px;
            cursor: inherit;
        }

        button.cart_wishlist:hover {
            color: #ffffff;
            background: #1a237e;
            border: 1px solid #1a237e;
        }

        button.cart_wishlist.active {
            color: #1a237e;
            border-color: #1a237e63;
        }

        button.cart_wishlist.active:hover {
            color: #ffffff;
            background: #656565;
            border: 1px solid #656565;
        }

        button.cart_wishlist_home {
            background: transparent;
            border: none;
        }

        button.cart_wishlist_home.active {
            color: #1a237e;
        }

        button.cart_wishlist_home.active:hover {
            color: #656565;
        }

        .header-middle-area {
            box-shadow: 0 1px 3px rgba(50, 50, 50, 0.3);
            margin-bottom: 50px;
        }

    </style>

    <!-- Notification -->
    <style>
        #snackbar {
            opacity: 0;
            min-width: 250px;
            background-color: #ffffff;
            color: #1a237e;
            text-align: center;
            border-radius: 2px;
            padding: 16px 40px;
            position: fixed;
            z-index: 100000000;
            left: 50%;
            bottom: 0px;
            margin-left: -120px;
            font-size: 17px;
            border-radius: 40px;
            box-shadow: 0px 2px 2px 2px rgba(0, 0, 0, 0.30);
        }
    </style>

    <!-- Rating -->
    <style>
        .pro-rating i {
            color: #1a237e !important;
        }
    </style>

    <!-- Floating Labels -->
    <style>
        .float-label {
            margin-bottom: 15px;
            height: 3.5em;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            -webkit-box-shadow: none;
            box-shadow: none;
            position: relative;
        }

        .float-label label {
            z-index: 0;
            height: 0;
            position: absolute;
            top: 11px;
            left: 20px;
            font-weight: 500;
            color: #777777;
            -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
            -webkit-transition: all 0.2s ease-in-out;
            transition: all 0.2s ease-in-out;
        }

        .float-label input {
            z-index: 1;
            height: 3.5em;
            padding-left: 20px;
        }

        .float-label input:valid,
        .float-label input:focus {
            padding: 20px 0px 5px 20px;
        }

        .float-label input:focus {
            background: #ffffff;
        }

        .float-label input:valid + label,
        .float-label input:focus + label {
            color: #1a237e;
            font-size: 12px;
            font-weight: bold;
            -webkit-transform: translate3d(0, -10px, 0);
            transform: translate3d(0, -10px, 0);
        }

        .has-error input, .fieldset.has-error input {
            border-color: red;
        }

        .float-label-modal {
            top: 17px !important;
            padding-left: 40px !important;
        }
    </style>

    <!-- Tippy Styling -->
    <style>
        .tippy-content {
            font-size: 15px;
            padding: 2px;
        }
    </style>

    <!-- Dropdown Menu -->
    <style>
        .mega-menu-area {
            left: -5%;
            width: 110%;
        }

        .main-menu > li > a {
            font-size: 2rem;
        }

        .mega-menu-link {
            /*width: 70%;*/
            width: 450px;
        }

        .mega-menu-photo {
            /*width: 30%;*/
            padding-right: 15px;
        }

        .mega-menu-photo img {
            width: 280px;
        }
    </style>

    <!-- Breadcrump -->
    <style>
        .p-20-40 {
            padding: 20px 40px
        }

        .breadcrumbs-title {
            padding: 9% 0;
        }

        .breadcrumbs-title, .breadcrumb-list > li {
            font-size: 2rem;
        }
    </style>

    <!-- Media Fixes -->
    <style>
        @media (max-width: 1400px ) {
            .footer-bottom-inner {
                padding: 10px 48px;
            }
        }

        @media (min-width: 1651px) {
            .footer-top-inner {
                padding: 60px 200px;
            }

            .footer-bottom-inner {
                padding: 10px 184px;
            }

            #cookie_consent {
                min-width: 40% !important;
            }
        }

        @media (max-width: 1650px) and (min-width: 1370px) {

            .col-lg-3 {
                width: 33.33333333% !important;
            }

            .plr-200 {
                padding: 0 100px;
            }

            .widget {
                padding: 25px 25px;
            }

        }

        @media (max-width: 1370px) and (min-width: 991px) {

            .col-lg-3 {
                width: 33.33333333% !important;
            }

            .plr-200 {
                padding: 0 100px;
            }

            .widget {
                padding: 25px 25px;
            }

        }

        @media (max-width: 990px) {

            .case-sm {
                margin-top: 20px;
            }
        }
    </style>

    <!-- Style Tweaks #1 -->
    <style>
        .option-btn.option-btn-local {
            line-height: 40px;
            color: #cccccc;
            margin: 0;
            font-size: 18px;
            background: #f9f9f9;
            border-radius: 4px;
            border: 1px solid #1a237e;
            padding: 0px 15px;
        }

        .flag-icon {
            font-size: 18px;
        }

        .local_choice_span {
            margin-right: 20px;
        }

        .local_choice {
            margin: 10px;
        }

        .local_choice a {
            color: #666666;
        }

        .widget.widget-tags {
            padding: 5px 5px;
        }

        .dropdown-flags {
            width: 200px !important;
        }

        .error-fieldset {
            border: 1px solid red;
            padding: 14px 20px;
        }

    </style>

    <!-- Style Tweaks #2 -->
    <style>
        button, th, td {
            font-family: 'Raleway', sans-serif;
        }

        .breadcrumbs {
            border: 1px solid lightgray;
        }
    </style>

    <!-- Style Tweaks #3 -->
    <style>
        i.zmdi.zmdi-favorite {
            font-size: 13px !important;
        }

        .product-gem img {
            height: 42px;
            width: 42px;
            padding: 0.1rem;
            border-radius: 50px;
            margin-right: 5px;
            margin-bottom: 10px
        }

        .sticky .header-search-inner a.search-toggle {
            padding: 10px 15px;
        }

        .header-search-inner a.search-toggle {
            color: #666;
            display: block;
            font-size: 20px;
            line-height: 1;
            padding: 15px 20px;
            text-align: center;
            transition: all 0.3s ease 0s;
        }

        i.fa.fa-heart-o:hover {
            color: #1a237e;
        }

        .header-top-bar {
            background: white;
            border-bottom: 1px solid #ccc;
        }

        ul.link li a:hover {
            color: #1a237e;
        }

        .total-cart-in .cart-toggler > a {
            color: #1a237e;
        }

        .total-cart-in:hover .cart-toggler > a {
            color: #666666;
        }

        .header-search-inner a.search-toggle {
            color: #1a237e;
        }

        .header-search-inner a.search-toggle:hover, .header-search-inner a.search-toggle i:hover {
            color: #666666 !important;
        }

        .top-link > ul.link > li a i {
            color: #1a237e;
        }

    </style>

    <!-- EU Cookie Consent -->
    <style>
        #cookie_consent {
            opacity: 0;
            min-width: 50%;
            background-color: #ffffff;
            color: #1a237e;
            text-align: center;
            border-radius: 2px;
            padding: 16px 40px;
            position: fixed;
            z-index: 100000000;
            left: 50%;
            bottom: 0px;
            margin-left: -25%;
            font-size: 17px;
            border-radius: 40px;
            box-shadow: 0px 2px 2px 2px rgba(0, 0, 0, 0.30);
        }

        #cookie_consent_txt {
            text-align: center;
            font-size: 16px;
            margin: 5px 0;
            color: #1a237e;
        }

        #cookie_consent_btn {
            border: 1px solid #1a237e;
            border-radius: 20px;
            padding: 0px 20px;
            color: #1a237e;
            font-size: 16px;
        }

        #cookie_consent_btn:hover {
            border: 1px solid #6f6f6f;
            border-radius: 20px;
            padding: 0px 20px;
            color: #6f6f6f;
        }

        .top-link > ul.link > li.user-greetings:hover {
            color: #666666;
        }
    </style>

    @yield('styles')

</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please
    <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Body main wrapper start -->
<div class="wrapper">

@php
    $categories_with_subcategories = \App\Category::whereHas('subcategories', function ($query) {
        $query->whereHas('products', function ($query) {
        $query->where('is_visible', '1');
         });
    })->get();

    // solo categories with products which are visible:
    $categories = \App\Category::whereHas('products', function ($query) {
        $query->where('is_visible', '1');
    })->get();

    // combine categories and subcategories:
    $categories = $categories_with_subcategories->merge($categories);
@endphp

<!-- START HEADER AREA -->
    <header class="header-area header-wrapper">
        <!-- header-top-bar -->
        <div class="header-top-bar">
            <div class="plr-200">
                <div class="row" style="padding-top: 10px;padding-bottom: 10px;">
                    <div class="col-sm-6 hidden-xs">
                        <div class="call-us">
                            <div class=" dropdown f-left">
                                <button class="option-btn option-btn-local">
                                    @if(LaravelLocalization::getCurrentLocale() == 'ru')
                                        <span class="flag-icon flag-icon-ru"></span>
                                    @elseif(LaravelLocalization::getCurrentLocale() == 'et')
                                        <span class="flag-icon flag-icon-ee"></span>
                                    @else
                                        <span class="flag-icon flag-icon-gb"></span>
                                    @endif
                                    <span style="color: #666666;font-size: 16px;margin-left: 4px">{{trans('front/layout.language')}}</span>
                                    <i class="zmdi zmdi-chevron-down" style="color: #666666"></i>
                                </button>
                                <div class="dropdown-menu dropdown-flags dropdown-width mt-10">
                                    <aside class="widget widget-tags box-shadow">
                                        <ul>
                                            <ul>
                                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                                    @if(LaravelLocalization::getCurrentLocale() != $localeCode)
                                                        <li class="local_choice">
                                                            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                                                @if($localeCode == 'ru')
                                                                    <span class="flag-icon flag-icon-ru local_choice_span"></span>{{ $properties['native'] }} &nbsp; ( RUS )
                                                                @elseif($localeCode == 'et')
                                                                    <span class="flag-icon flag-icon-ee local_choice_span"></span>{{ $properties['native'] }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ( EST )
                                                                @else
                                                                    <span class="flag-icon flag-icon-gb local_choice_span"></span>{{ $properties['native'] }} &nbsp;&nbsp;&nbsp; ( ENG )
                                                                @endif
                                                            </a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </ul>
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="top-link clearfix">
                            <ul class="link f-right">
                                @if(Auth::check())
                                    <li class="user-greetings">
                                    <span>
                                            {{trans('front/layout.hello')}} {{ Auth::user()->first_name }}
                                    </span>
                                    </li>
                                @else
                                    <li class="user-greetings">
                                    <span>
                                            {{trans('front/layout.hello_guest')}}
                                    </span>
                                    </li>
                                @endif
                                @if(Auth::check())
                                    <li>
                                        <a href="{{route('account')}}">
                                            <i class="zmdi zmdi-account"></i>
                                            {{trans('front/layout.my_account')}}
                                        </a>
                                    </li>
                                    @role('superadministrator|administrator|editor')
                                    <li>
                                        <a href="{{route('admin')}}">
                                            <i class="zmdi zmdi-settings"></i>
                                            {{trans('front/layout.admin')}}
                                        </a>
                                    </li>
                                    @endrole
                                @endif
                                <li>
                                    @if(Auth::check())
                                        <a href="{{route('logout')}}">
                                            <i class="fa fa-sign-out"></i>
                                            {{trans('front/layout.logout')}}
                                        </a>
                                    @else
                                        <a class="cd-signin" href="#">
                                            <i class="zmdi zmdi-lock"></i>
                                            {{trans('front/layout.login')}}  |  {{trans('front/auth.register')}}
                                        </a>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- header-middle-area -->
        <div id="sticky-header" class="header-middle-area">
            <div class="plr-200">
                <div class="full-width-mega-dropdown">
                    <div class="row">
                        <!-- logo -->
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            <div class="logo">
                                <a href="{{route('root')}}">
                                    <img src="{{asset('img/banner/logo_silverest.png')}}" alt="main logo" style="width: 210px">
                                </a>
                            </div>
                        </div>
                        <!-- primary-menu -->
                        <div class="col-md-8 hidden-sm hidden-xs">
                            <nav id="primary-menu">
                                <ul class="main-menu text-center">
                                    <li>
                                        <a href="{{route('root')}}">{{trans('front/layout.home')}}</a>
                                    </li>
                                    {{-- Get categories: --}}
                                    @if($categories)
                                        <li class="mega-parent">
                                            <a href="{{route('catalogue')}}">{{trans('front/layout.catalogue')}}</a>
                                            <div class="mega-menu-area clearfix">
                                                <div class="mega-menu-link f-left" style="position: relative">
                                                    <h2 style="margin-bottom: 40px;margin-left: 15px">
                                                        <a href="{{route('catalogue')}}">{{trans('front/layout.catalogue')}}</a>
                                                    </h2>
                                                    <div class="row clearfix" style="margin-left: 0px">
                                                        <ul class="col-md-5 single-mega-item categories">
                                                            @foreach($categories as $category)
                                                                <li class="category-{{$category->id}}">
                                                                    <a href="{{route('catalogue', ['category'=>$category->code])}}">{{$category->name}}</a>
                                                                    @if($loop->first)
                                                                        <i class="fa fa-chevron-right" name="category-{{$category->id}}" aria-hidden="true"></i>
                                                                    @else
                                                                        <i class="fa fa-chevron-right" name="category-{{$category->id}}" aria-hidden="true" style="display: none"></i>
                                                                    @endif
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                        <ul class="col-md-7 single-mega-item subcategories">
                                                            @foreach($categories as $category)
                                                                @if($loop->first)
                                                                    @foreach($category->subcategories()->whereHas('products',function ($query){$query->where('is_visible','1'); })->get() as $subcategory)
                                                                        <li>
                                                                            <a class="category-{{$category->id}}" href="{{route('catalogue', ['category'=>$category->code.'_'.$subcategory->code])}}">{{$subcategory->name}}</a>
                                                                        </li>
                                                                    @endforeach
                                                                @else
                                                                    @foreach($category->subcategories()->whereHas('products',function ($query){$query->where('is_visible','1'); })->get() as $subcategory)
                                                                        <li>
                                                                            <a class="category-{{$category->id}}" style="display: none" href="{{route('catalogue', ['category'=>$category->code.'_'.$subcategory->code])}}">{{$subcategory->name}}</a>
                                                                        </li>
                                                                    @endforeach
                                                                @endif
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="mega-menu-photo f-right" style="position: relative;">
                                                    @foreach($categories as $category)
                                                        @php
                                                            $product = $category->products()->where('is_visible','1')->orderBy('views_number','desc')->first();
                                                        @endphp

                                                        @if($product)
                                                            @if($loop->first)
                                                                <a href="{{route('catalogue', ['category'=>$category->code])}}" class="category-{{$category->id}}">
                                                                    <img src="{{$product->photos()->count() > 0 ? $product->photos->first()->getPreviewPath() : ''}}" alt="mega menu image">
                                                                </a>
                                                            @else
                                                                <a href="{{route('catalogue', ['category'=>$category->code])}}" class="category-{{$category->id}}" style="display: none">
                                                                    <img src="{{$product->photos()->count() > 0 ? $product->photos->first()->getPreviewPath() : ''}}" alt="mega menu image">
                                                                </a>
                                                            @endif
                                                        @endif

                                                    @endforeach
                                                </div>
                                            </div>
                                        </li>
                                    @endif

                                    <li>
                                        <a href="{{route('warranty')}}">{{trans('front/layout.warranty')}}</a>
                                    </li>
                                    <li>
                                        <a href="{{route('delivery')}}">{{trans('front/layout.delivery')}}</a>
                                    </li>
                                    <li>
                                        <a href="{{route('about-us')}}">{{trans('front/layout.about_us')}}</a>
                                    </li>
                                    <li>
                                        <a href="{{route('contacts')}}">{{trans('front/layout.contacts')}}</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <!-- header-search & total-cart -->
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            <div class="search-top-cart f-right">
                                <!-- header-search -->
                                <div class="header-search f-left">
                                    <div class="header-search-inner">
                                        <button class="search-toggle">
                                            <i class="zmdi zmdi-search" style="color: #1a237e;"></i>
                                        </button>
                                        {!! Form::open(['method'=>'GET','route'=>'catalogue']) !!}
                                        <div class="top-search-box">
                                            <input type="text" name="search" placeholder="{{trans('front/layout.search')}}..." value="{{isset($search_field) ? $search_field : ''}}">
                                            <button type="submit">
                                                <i class="zmdi zmdi-search"></i>
                                            </button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>

                                <div class="header-search f-left">
                                    <div class="header-search-inner">
                                        <a class="search-toggle" href="{{route('account.wishlist')}}" title="{{trans('front/layout.wishlist')}}">
                                            <i class="fa fa-heart-o"></i>
                                        </a>
                                    </div>
                                </div>

                                @if(Auth::check())

                                    @php
                                        $cart_subtotal = 0;
                                        $cart_amount = 0;
                                        $cart_products = Auth::user()->carts()->get()->keyBy('id');
                                        foreach ($cart_products as $cart_product) {
                                            // Check if product is in stock and visible:
                                            if ($cart_product->quantity < $cart_product->pivot->quantity || !$cart_product->is_visible) {
                                                Auth::user()->carts()->detach($cart_product);
                                            } else {
                                                $cart_subtotal += (($cart_product->getDiscountPrice()) ? $cart_product->getDiscountPrice() : $cart_product->price) * $cart_product->pivot->quantity;
                                                $cart_amount += $cart_product->pivot->quantity;
                                            }
                                        }
                                    @endphp
                                    <div class="total-cart f-left">
                                        <div class="total-cart-in">
                                            <div class="cart-toggler">
                                                <a href="{{route('account.cart')}}">
                                                    <span class="cart-quantity">{{($cart_amount < 10) ? '0' . $cart_amount : $cart_amount}}</span><br>
                                                    <span class="cart-icon"><i class="zmdi zmdi-shopping-cart-plus"></i></span>
                                                </a>
                                            </div>
                                            <ul>
                                                <li>
                                                    <div class="top-cart-inner your-cart">
                                                        <h5 class="text-capitalize">{{trans('front/layout.your_cart')}}</h5>
                                                    </div>
                                                </li>
                                                @if($cart_amount > 0)
                                                    <li>
                                                        <div class="total-cart-pro pre-scrollable">
                                                            @foreach($cart_products as $cart_product)
                                                                <div class="single-cart clearfix">
                                                                    <div class="cart-img f-left">
                                                                        <a href="{{route('catalogue.product',$cart_product->id)}}">
                                                                            <img src="{{$cart_product->photos()->count() > 0 ? $cart_product->photos->first()->getPreviewPath() : ''}}" class="cart_product_dropdown" alt="Cart Product"/>
                                                                        </a>
                                                                        {!! Form::open(['method'=>'POST','route'=>['account.cart.remove']]) !!}
                                                                        <div class="del-icon">
                                                                            <input type="hidden" name="product_id" value="{{$cart_product->id}}">
                                                                            <button type="submit">
                                                                                <i class="zmdi zmdi-close"></i>
                                                                            </button>
                                                                        </div>
                                                                        {!! Form::close() !!}
                                                                    </div>
                                                                    <div class="cart-info f-left">
                                                                        <h6 class="text-capitalize">
                                                                            <a href="{{route('catalogue.product',$cart_product->id)}}">{{str_limit($cart_product->name,15)}} ({{$cart_product->code}})</a>
                                                                        </h6>
                                                                        @if($cart_product->pivot->size > 0)
                                                                            <p>
                                                                                <span>{{trans('front/layout.size')}}</span>
                                                                                <strong>:</strong>&nbsp;&nbsp;&nbsp; {{$cart_product->pivot->size}}
                                                                            </p>
                                                                        @endif
                                                                        <p>
                                                                            <span>{{trans('front/layout.q_ty')}}</span>
                                                                            <strong>:</strong>&nbsp;&nbsp;&nbsp; {{$cart_product->pivot->quantity}}
                                                                        </p>
                                                                        <p>
                                                                            <span>{{trans('front/layout.price')}} </span>
                                                                            <strong>:</strong>&nbsp;&nbsp;&nbsp; {{$cart_product->getDiscountPrice() ? number_format ($cart_product->getDiscountPrice(),2) : number_format ($cart_product->price,2)}} &euro;
                                                                            @if($cart_product->getDiscountPrice())
                                                                                <span class="discount_cart_view">- {{$cart_product->discount_rate}} %</span>
                                                                            @endif
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="top-cart-inner subtotal">
                                                            <h4 class="text-uppercase g-font-2">
                                                                {{trans('front/layout.total')}} =
                                                                <span>&euro; {{number_format ($cart_subtotal,2)}}</span>
                                                            </h4>
                                                        </div>
                                                    </li>
                                                @else
                                                    <li>
                                                        <div class="top-cart-inner check-out">
                                                            <h4 class="text-uppercase" style="color: #1a237e">
                                                                {{trans('front/layout.your_cart_is_empty')}}
                                                            </h4>
                                                        </div>
                                                    </li>
                                                @endif
                                                <li>
                                                    <div class="top-cart-inner view-cart">
                                                        <h4 class="text-uppercase">
                                                            <a href="{{route('account.cart')}}">{{trans('front/layout.view_cart')}}</a>
                                                        </h4>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="top-cart-inner check-out">
                                                        <h4 class="text-uppercase">
                                                            <a href="{{route('account.order.checkout')}}">{{trans('front/layout.check_out')}}</a>
                                                        </h4>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                @else
                                    <div class="total-cart f-left">
                                        <div class="total-cart-in" style="padding: 7px 0 12px;">
                                            <div class="cart-toggler">
                                                <a href="{{route('account.cart')}}">
                                                    <br>
                                                    <span class="cart-icon">
                                                        <i class="zmdi zmdi-shopping-cart-plus"></i>
                                                    </span>
                                                </a>
                                            </div>
                                            <ul>
                                                <li>
                                                    <div class="top-cart-inner your-cart">
                                                        <h5 class="text-capitalize">{{trans('front/layout.your_cart')}}</h5>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="top-cart-inner view-cart">
                                                        <h4 class="text-uppercase">
                                                            <a href="{{route('account.cart')}}">{{trans('front/layout.login_to_see')}}</a>
                                                        </h4>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- END HEADER AREA -->

    <!-- START MOBILE MENU AREA -->
    <div class="mobile-menu-area hidden-lg hidden-md">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="mobile-menu">
                        <nav id="dropdown">
                            <ul>
                                <li>
                                    <a href="{{route('root')}}">{{trans('front/layout.home')}}</a>
                                </li>
                                <li><a href="{{route('catalogue')}}">{{trans('front/layout.catalogue')}}</a>
                                    @if($categories)
                                        <ul>
                                            @foreach($categories as $category)
                                                <li>
                                                    <a href="{{route('catalogue', ['category'=>$category->code])}}">{{$category->name}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                                <li>
                                    <a href="{{route('warranty')}}">{{trans('front/layout.warranty')}}</a>
                                </li>
                                <li>
                                    <a href="{{route('delivery')}}">{{trans('front/layout.delivery')}}</a>
                                </li>
                                <li>
                                    <a href="{{route('about-us')}}">{{trans('front/layout.about_us')}}</a>
                                </li>
                                <li>
                                    <a href="{{route('contacts')}}">{{trans('front/layout.contacts')}}</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MOBILE MENU AREA -->

@yield('content')

<!-- START FOOTER AREA -->
    <footer id="footer" class="footer-area">
        <div class="footer-top" style="border-top: 1px solid #ccc">
            <div>
                <div class="footer-top-inner">
                    <div class="row">
                        <div class="col-lg-7 col-md-7 col-sm-12">
                            <div class="single-footer footer-about">
                                <div class="footer-logo">
                                    <img src="{{asset('img/banner/logo_silverest.png')}}" alt="" style="width: 210px;float: none">
                                </div>
                                <div class="footer-brief">
                                    <p>{{trans('front/layout.welcome_to_the_website')}}</p>
                                    <p style="text-align: justify">{{trans('front/layout.jewelry_company')}}
                                    <p style="text-align: justify">{{trans('front/layout.manufacture_of_jewelry')}}</p>
                                </div>
                                <ul class="footer-social">
                                    <li>
                                        <a class="facebook" href="https://www.facebook.com/silverest/" title="Facebook"><i class="zmdi zmdi-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a class="instagram" href="https://www.instagram.com/_silverest/" title="Google Plus"><i class="zmdi zmdi-instagram"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-12 case-sm">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="single-footer">
                                        <h4 class="footer-title border-left">{{trans('front/layout.catalogue')}}</h4>
                                        <ul class="footer-menu">
                                            <li>
                                                <a href="{{route('catalogue', ['sort'=>'new'])}}"><i class="zmdi zmdi-circle"></i><span>{{trans('front/layout.new_products')}}</span></a>
                                            </li>
                                            <li>
                                                <a href="{{route('catalogue', ['sort'=>'discount'])}}"><i class="zmdi zmdi-circle"></i><span>{{trans('front/layout.discount_products')}}</span></a>
                                            </li>
                                            <li>
                                                <a href="{{route('catalogue', ['sort'=>'bestseller'])}}"><i class="zmdi zmdi-circle"></i><span>{{trans('front/layout.best_sell_products')}}</span></a>
                                            </li>
                                            <li>
                                                <a href="{{route('catalogue', ['sort'=>'popular'])}}"><i class="zmdi zmdi-circle"></i><span>{{trans('front/layout.popular_products')}}</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="single-footer">
                                        <h4 class="footer-title border-left">{{trans('front/layout.my_account')}}</h4>
                                        <ul class="footer-menu">
                                            <li>
                                                <a href="{{route('account')}}"><i class="zmdi zmdi-circle"></i><span>{{trans('front/layout.my_account')}}</span></a>
                                            </li>
                                            <li>
                                                <a href="{{route('account.wishlist')}}"><i class="zmdi zmdi-circle"></i><span>{{trans('front/layout.my_wishlist')}}</span></a>
                                            </li>
                                            <li>
                                                <a href="{{route('account.cart')}}"><i class="zmdi zmdi-circle"></i><span>{{trans('front/layout.my_cart')}}</span></a>
                                            </li>
                                            <li>
                                                <a href="{{route('account.orders')}}"><i class="zmdi zmdi-circle"></i><span>{{trans('front/layout.my_orders')}}</span></a>
                                            </li>
                                            <li>
                                                <a href="{{route('account.order.checkout')}}"><i class="zmdi zmdi-circle"></i><span>{{trans('front/layout.check_out')}}</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom" style="border-top: 1px solid #ccc">
            <div class="container-fluid">
                <div class="footer-bottom-inner">
                    <div>
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="copyright-text">
                                    <p style="color: #5d5d5d;">&copy; SILVEREST {{date("Y")}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- END FOOTER AREA -->

    <!-- START QUICKVIEW PRODUCT -->
    <div id="quickview-wrapper">
        <!-- Modal -->
        <div class="modal fade" id="productModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-product clearfix">

                        </div><!-- .modal-product -->
                    </div><!-- .modal-body -->
                </div><!-- .modal-content -->
            </div><!-- .modal-dialog -->
        </div>
        <!-- END Modal -->
    </div>
    <!-- END QUICKVIEW PRODUCT -->

@if(!Auth::check())
    <!-- START LOGIN MODAL -->
        <div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
            <div class="cd-user-modal-container"> <!-- this is the container wrapper -->
                <ul class="cd-switcher">
                    <li><a href="#0">{{trans('front/layout.sign_in')}}</a></li>
                    <li><a href="#0">{{trans('front/layout.new_account')}}</a></li>
                </ul>

                <div id="cd-login"> <!-- log in form -->


                    <form class="cd-form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        @if($errors->count())
                            <div class="error-fieldset">
                                @foreach ($errors->all() as $error)
                                    <div style="color: red">{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif

                        <p class="fieldset float-label {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="image-replace cd-username" for="signin-email">{{trans('front/layout.email_address')}}</label>
                            <input class="full-width has-padding has-border" id="signin-email" type="text" name="email" value="{{ old('email') }}" style="padding-left: 60px !important;" required>
                            <label class="float-label-modal" for="signin-email" style="color: {{$errors->has('email') ? 'red':''}}">{{trans('front/layout.email_address')}}</label>
                        </p>

                        <p class="fieldset float-label {{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="image-replace cd-password" for="signin-password">{{trans('front/layout.password')}}</label>
                            <input class="full-width has-padding has-border" id="signin-password" type="password" name="password" style="padding-left: 60px !important;" required>
                            <label class="float-label-modal" for="signin-password" style="color: {{$errors->has('password') ? 'red':''}}">{{trans('front/layout.password')}}</label>
                        </p>

                        <p class="fieldset">
                            <input type="checkbox" id="remember-me" checked>
                            <label for="remember-me">&nbsp;&nbsp; {{trans('front/layout.remember_me')}}</label>
                        </p>

                        <p class="fieldset">
                            <input class="full-width" type="submit" value="{{trans('front/auth.login')}}">
                        </p>
                    </form>
                    <p class="cd-form-bottom-message"><a href="#0">{{trans('front/layout.forgot_your_password')}}?</a>
                    </p>

                    <!-- <a href="#0" class="cd-close-form">Close</a> -->
                </div> <!-- cd-login -->

                <div id="cd-signup"> <!-- sign up form -->

                    <form class="cd-form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        @if($errors->count())
                            <div class="error-fieldset">
                                @foreach ($errors->all() as $error)
                                    <div style="color: red">{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        <p class="fieldset float-label {{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label class="image-replace cd-username" for="signup-fname">{{trans('front/layout.first_name')}}</label>
                            <input class="full-width has-padding has-border" id="signup-fname" type="text" name="first_name" value="{{ old('first_name') }}" style="padding-left: 60px !important;" required>
                            <label class="float-label-modal" for="signup-fname" style="color: {{$errors->has('first_name') ? 'red':''}}">{{trans('front/layout.first_name')}}</label>
                        </p>
                        <p class="fieldset float-label {{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label class="image-replace cd-username" for="signup-lname">{{trans('front/layout.last_name')}}</label>
                            <input class="full-width has-padding has-border" id="signup-lname" type="text" name="last_name" value="{{ old('last_name') }}" style="padding-left: 60px !important;" required>
                            <label class="float-label-modal" for="signup-lname" style="color: {{$errors->has('last_name') ? 'red':''}}">{{trans('front/layout.last_name')}}</label>
                        </p>
                        <p class="fieldset float-label {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="image-replace cd-username" for="signup-email">{{trans('front/layout.email_address')}}</label>
                            <input class="full-width has-padding has-border" id="signup-email" type="text" name="email" value="{{ old('email') }}" style="padding-left: 60px !important;" required>
                            <label class="float-label-modal" for="signup-email" style="color: {{$errors->has('email') ? 'red':''}}">{{trans('front/layout.email_address')}}</label>
                        </p>

                        <p class="fieldset float-label {{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="image-replace cd-password" for="signup-password">{{trans('front/layout.password')}}</label>
                            <input class="full-width has-padding has-border" id="signup-password" type="password" name="password" style="padding-left: 60px !important;" required>
                            <label class="float-label-modal" for="signup-password" style="color: {{$errors->has('password') ? 'red':''}}">{{trans('front/layout.password')}}</label>
                        </p>

                        <p class="fieldset float-label {{ $errors->has('password_confirmation') ? ' has-error' : '' }}" style="margin-bottom: 40px">
                            <label class="image-replace cd-password" for="signup-password-confirm">{{trans('front/layout.confirm_password')}}</label>
                            <input class="full-width has-padding has-border" id="signup-password-confirm" type="password" name="password_confirmation" style="padding-left: 60px !important;" required>
                            <label class="float-label-modal" for="signup-password-confirm" style="color: {{$errors->has('password_confirmation') ? 'red':''}}">{{trans('front/layout.confirm_password')}}</label>
                        </p>

                        <div class="g-recaptcha" data-sitekey="6Lc1d3EaAAAAABTInQi4HbPzlIvyNn8rV-JHpHlT"></div>

                        <p class="fieldset">
                            <input class="full-width has-padding" type="submit" value="{{trans('front/auth.create')}}">
                        </p>
                    </form>

                    <!-- <a href="#0" class="cd-close-form">Close</a> -->
                </div> <!-- cd-signup -->

                <div id="cd-reset-password"> <!-- reset password form -->
                    <p class="cd-form-message">{{trans('front/layout.lost_your_password')}}</p>

                    <form class="cd-form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <p class="fieldset float-label mb-50 {{ $errors->has('email') ? ' has-error' : '' }}" style="margin-top: -20px">
                            <label class="image-replace cd-username" for="signup-email">{{trans('front/layout.email_address')}}</label>
                            <input class="full-width has-padding has-border" id="signup-email2" type="text" name="email" value="{{ old('email') }}" style="padding-left: 60px !important;" required>
                            <label class="float-label-modal" for="signup-email2" style="color: {{$errors->has('email') ? 'red':''}}">{{trans('front/layout.email_address')}}</label>
                        </p>

                        <p class="fieldset" style="margin-top: 40px">
                            <input class="full-width has-padding" type="submit" value="Reset password">
                        </p>
                    </form>

                    <p class="cd-form-bottom-message"><a href="#0">{{trans('front/layout.back_to_login')}}</a></p>
                </div> <!-- cd-reset-password -->
                <a href="#0" class="cd-close-form">{{trans('front/layout.close')}}</a>
            </div> <!-- cd-user-modal-container -->
        </div>
        <!-- END LOGIN MODAL -->
    @endif

</div>

<!-- Notification -->
@if(Session::has('notification'))
    <div id="snackbar">
        {{session('notification')}}
    </div>
@endif

<!-- EU Cookie Compliance -->
<div id="cookie_consent">
    <div class="row">
        <div class="col-md-9">
            <p id="cookie_consent_txt">{{trans('front/layout.cookie_text')}}.</p>
        </div>
        <div class="col-md-3">
            <a class="btn btn-normal" id="cookie_consent_btn">{{trans('front/layout.cookie_btn')}}</a>
        </div>
    </div>
</div>

<!-- Body main wrapper end -->

<!-- Javascript files -->
<script src="{{asset('assets/front/js/vendor/modernizr-2.8.3.min.js')}}"></script>
<script src="{{asset('assets/front/js/vendor/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/front/lib/js/jquery.nivo.slider.js')}}"></script>
<script src="{{asset('assets/front/js/plugins.js')}}"></script>
<script src="{{asset('assets/front/js/main.js')}}"></script>
<script src="{{asset('assets/front/js/login.js')}}"></script>
<script src="{{asset('assets/front/js/tippy.js')}}"></script>
<script src="{{asset('assets/front/js/cookie.js')}}"></script>

<!-- Categories -->
<script>
    $(".categories li[class^='category-']").mouseover(function () {
        var categoryId = $(this).attr("class");
        $(".subcategories a").hide();
        $(".subcategories a[class=" + categoryId + "]").show();
        $(".categories i").hide();
        $(".categories i[name=" + categoryId + "]").show();
        $(".mega-menu-photo a[class^='category-']").hide();
        $(".mega-menu-photo a[class=" + categoryId + "]").show();
    })

    // attach nice scroll to poducts list in cart:
    $(".total-cart-pro").niceScroll({
        cursorcolor: '#1a237e',
        cursorwidth: '3px',
        cursorborder: 'none',
        scrollspeed: 20,
        mousescrollstep: 60,
        autohidemode: false
    });

    // get product cart preview:
    $('a.cart_product').click(function () {

        var product_id = $(this).attr('data-product');
        var data = {
            _token: '{!! csrf_token() !!}',
            product_id: product_id
        }

        $.ajax({
            'url': "{{route('catalogue.product.preview.ajax')}}",
            'data': data,
            'method': 'POST',
            success: function (response) {
                var cart_preview = $('.modal-product');
                cart_preview.html(response);
            }
        });
    });
</script>

<!-- Snackbar -->
<script>
    $(document).ready(function () {

        $("#snackbar").animate({bottom: 30, opacity: 1}, 'fast');

        setTimeout(function () {
            $("#snackbar").animate({bottom: 0, opacity: 0}, 'fast');
        }, 5000);
    });
</script>

<!-- Ratings -->
<script>
    $('.pro-rating').each(function () {
        var rating = $(this).attr('data-rating');
        $(this).html(getStars(rating));
    });

    function getStars(rating) {
        rating = Math.round(rating * 2) / 2;
        let output = [];
        for (var i = rating; i >= 1; i--) {
            output.push('<i class="fa fa-star" aria-hidden="true" style="color: #1a237e;"></i>&nbsp;');
        }
        if (i == .5) {
            output.push('<i class="fa fa-star-half-o" aria-hidden="true" style="color: #1a237e;"></i>&nbsp;');
        }
        for (let i = (5 - rating); i >= 1; i--) {
            output.push('<i class="fa fa-star-o" aria-hidden="true" style="color: #1a237e;"></i>&nbsp;');
        }
        return output.join('');
    }
</script>

<!-- Form Label -->
<script>
    $('.float-label-modal').click(function () {
        labelID = $(this).attr('for');
        $('#' + labelID).trigger('click');
    });
</script>

<!-- Tippy JS for buttons -->
<script>
    tippy('.tippy', {
        placement: 'bottom',
        animation: 'perspective',
        interactiveBorder: 5,
        animateFill: true,
        size: 'large',
        arrow: true,
    });

    tippy('.tippy-gem', {
        placement: 'bottom',
        animation: 'perspective',
        interactiveBorder: 5,
        animateFill: true,
        size: 'large',
    })
</script>

<!-- EU Cookie Compliance -->
<script>
    $(document).ready(function () {
        if (!Cookies.get('cookie_consent')) {
            $("#cookie_consent").animate({bottom: 30, opacity: 1}, 'fast');

            $('#cookie_consent_btn').click(function () {
                Cookies.set('cookie_consent', 'Customer agreed with cookie compliance', {expires: 365});
                setTimeout(function () {
                    $("#cookie_consent").animate({bottom: 0, opacity: 0}, 'fast');
                }, 500);
            });
        }
    });
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115092211-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-115092211-1');
</script>


@yield('scripts')

<!-- Licenses -->
<script>

    /* Licenses */

    /*!
    * Sweetalert (https://sweetalert.js.org/docs/)
    * Copyright (c) 2014-present Tristan Edwards
    * Licensed under MIT (https://github.com/t4t5/sweetalert/blob/master/LICENSE.md)
    */

    /*!
    * Cleave.js (https://nosir.github.io/cleave.js/)
    * Licensed under APACHE Version 2.0, January 2004 (https://github.com/nosir/cleave.js/blob/master/LICENSE)
    */

    /*!
    * rateYo (http://rateyo.fundoocode.ninja/)
    * Copyright (c) 2014 prashanth pamidi
    * Licensed under MIT (https://github.com/prrashi/rateYo/blob/master/LICENSE)
    */

    /*!
    * Animate.css (https://daneden.github.io/animate.css/)
    * Copyright (c) 2018 Daniel Eden
    * Licensed under MIT (https://github.com/daneden/animate.css/blob/master/LICENSE)
    */

    /*!
    * Flag-icon-css (http://flag-icon-css.lip.is/)
    * Copyright (c) 2013 Panayiotis Lipiridis
    * Licensed under MIT (https://github.com/lipis/flag-icon-css/blob/master/LICENSE)
    */

    /*!
    * # Icons: CC BY 4.0 License (https://creativecommons.org/licenses/by/4.0/)
    * In the Font Awesome Free download, the CC BY 4.0 license applies to all icons
    * packaged as SVG and JS file types.

    * # Fonts: SIL OFL 1.1 License (https://scripts.sil.org/OFL)
    * In the Font Awesome Free download, the SIL OLF license applies to all icons
    * packaged as web and desktop font files.

    * # Code: MIT License (https://opensource.org/licenses/MIT)
    * In the Font Awesome Free download, the MIT license applies to all non-font and
    * non-icon files.
    */

    /*!
    * Tippy.js (https://atomiks.github.io/tippyjs/)
    * Copyright (c) 2017 atomiks
    * Licensed under MIT (https://github.com/atomiks/tippyjs/blob/master/LICENSE)
    */

    /*!
    * rateYo.js (https://github.com/prrashi/rateYo/)
    * Copyright (c) 2014 prashanth pamidi
    * Licensed under MIT (https://github.com/prrashi/rateYo/blob/master/LICENSE)
    */

    /*!
    * Google recaptcha.js (https://github.com/google/recaptcha/)
    * Copyright 2014, Google Inc.
    * Licensed under BSD 3-Clause (https://github.com/google/recaptcha/blob/master/LICENSE)
    */

    /*!
    * Cookie.js (https://github.com/js-cookie/js-cookie/)
    * Copyright (c) 2018 Copyright 2018 Klaus Hartl, Fagner Brack, GitHub Contributors
    * Licensed under MIT (https://github.com/js-cookie/js-cookie/blob/master/LICENSE)
    */

    /*!
    * elevateZoom (http://www.elevateweb.co.uk/image-zoom)
    */
</script>

</body>
</html>
