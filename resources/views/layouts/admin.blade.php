<!DOCTYPE html>
<html>
<head>
    <title>@yield('title','Silverest')</title>

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Silverest manufacture high quality silver and gold products with natural stones">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Ювелирные украшения с натуральными  камнями,    ювелирные украшения с камнями,    изделия из драгоценных металлов в Эстонии,    изготовитель золотых и серебряных украшений в Эстонии,    украшения из золота и серебра,    магазин ювелирных украшений Эстония,    ювелирные изделия с натуральными  камнями,    ювелирные магазины Эстонии,    ЭКСКЛЮЗИВНЫЕ УКРАШЕНИЯ,    Подарочная коробочка,    Украшения из натурального жемчуга,    Серебряные кольца с натуральным камнем,    Серебряные серьги с натуральным камнем,    Серебряные комплекты с натуральными камнями в Эстонии,    Серебряные кольца, серьги, браслеты, кулоны  925 пробы,    Большой Ассортимент Украшений  с натуральными  камнями,    изготовление ювелирных изделий на заказ,    Кольца любых размеров на заказ,    уникальные серебряные украшения ручной работы,    ювелирный интернет магазин,    интернет магазин ювелирных украшений с  натуральными  камнями,    Украшения для женщин,    Большой ассортимент качественных ювелирных изделий на любой случай,    Ehted looduslike kividega,    Ehted kividega,    väärismetalltooted Eestis,    kuld- ja hõbeehete tootja Eestis,    kuld ja hõbe ehted,    Ehtedepood Eesti,    ehted looduslike kividega,    Ehted kauplustes Eestis,    EXCLUSIVE JEWELRY,    Kinkekarp,    Mageveepärlitest valmistatud ehted,    Hõbe rõngad looduskiviga,    Hõbedased kõrvarõngad loodusliku kiviga,    Hõbedakomplektid looduslike kividega Eestis,    Hõbe rõngad, kõrvarõngad, käevõrud, ripatsid 925 proovi,    Suur valik ehteid looduslike kividega,    ehteid tellima,    Mistahes suurusega rõngad tellimiseks,    unikaalne käsitsi valmistatud hõbe ehted,    Ehted e-poes,    ehted e-poest looduslike kividega,    Ehted naistele,    Laia valikut kvaliteetseid ehteid iga kord,    Jewelry with natural stones,    jewelry with stones,    precious metal products in Estonia,    manufacturer of gold and silver jewelry in Estonia,    gold and silver jewelry,    jewelry shop Estonia,    jewelry with natural stones,    jewelry stores in Estonia,    Gift box,    Jewelry made from freshwater pearls,    Silver rings with natural stone,    Silver earrings with natural stone,    Silver sets with natural stones in Estonia,    Silver rings, earrings, bracelets, pendants 925 samples,    Large Assortment of Jewelry with natural stones,    making jewelry to order,    Rings of any sizes to order,    unique handmade silver jewelry,    jewelry online store,    online jewelry store with natural stones,    Jewelry for women,    A wide range of quality jewelry for any occasion">
    <meta name="robots" content="index, follow">
    <meta name="language" content="English">
    <meta name="author" content="Silverest">
    
    <!-- CSS Files -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" href="{{asset('assets/admin/css/style.default.css')}}" id="theme-stylesheet">
    <link rel="stylesheet" href="{{asset('assets/admin/css/grasp_mobile_progress_circle-1.0.0.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/sweetalert2.min.css')}}">
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/icons.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.0.0/css/flag-icon.css"/>

    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

    <!-- Loading Image -->
    <style>
        img.loading {
            background: transparent url({{asset('images/gifs/loading.gif')}}) center no-repeat;
        }
    </style>

    <!-- Notification -->
    <style>
        #snackbar {
            opacity: 0;
            min-width: 250px;
            background-color: #333;
            color: #fff;
            text-align: center;
            border-radius: 2px;
            padding: 16px 40px;
            position: fixed;
            z-index: 100000000;
            left: 50%;
            bottom: 60px;
            margin-left: -60px;
            font-size: 17px;
            border: 2px solid #2b90d9;
            box-shadow: 0px 2px 2px 2px rgba(0, 0, 0, 0.30);
            /*border-radius: 40px;*/
        }

        .badge-warning-new {
            background-color: #3044ff;
            margin-left: 32px;
            border: 2px solid #b4b4b4;
        }

        h1, .h1 {
            font-size: 1.25rem;
        }

    </style>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.0.0/css/flag-icon.css"/>

    <style>

        .flag-icon {
            font-size: 17px;
        }

        .local_choice_span {
            margin-right: 20px;
        }

        .local_choice {
            margin: 10px;
        }

        .local_choice a {
            color: #cccccc;
        }

    </style>

    @yield('styles')
</head>
<body>
<!-- Side Navbar -->
<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <div class="sidenav-header-inner text-center">
                <img src="{{asset('img/user_white.png')}}" alt="person" class="img-fluid rounded-circle">
                <h2 class="h6 text-uppercase">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h2>
                <span class="text-uppercase">{{Auth::user()->roles->first()->name}}</span>
            </div>
            <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center">
                    <strong>B</strong><strong class="text-primary">D</strong></a></div>
        </div>
        <div class="main-menu">
            <ul id="side-main-menu" class="side-menu list-unstyled">
                <li class="active">
                    <a href="{{route('admin')}}"><i class="fa fa-home fa-lg" aria-hidden="true"></i><span>{{trans('admin/layout.home')}}</span></a>
                </li>
                <li>
                    <a href="{{route('catalogue')}}">
                        <i class="fa fa-bandcamp" aria-hidden="true"></i><span>{{trans('admin/layout.catalogue')}}</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('orders.index')}}">
                        <i class="fa fa-tasks fa-lg" aria-hidden="true"></i><span>{{trans('admin/layout.orders')}}</span>
                        @if(\App\Order::all()->where('is_viewed',false)->count() > 0)
                            <div class="badge badge-warning badge-warning-new">{{\App\Order::all()->where('is_viewed',false)->count()}} New</div>
                        @endif
                    </a>
                </li>
                <li>
                    <a href="{{route('products.index')}}">
                        <i class="fa fa-diamond fa-lg" aria-hidden="true"></i><span>{{trans('admin/layout.products')}}</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('customers.index')}}">
                        <i class="fa fa-address-book fa-lg" aria-hidden="true"></i><span>{{trans('admin/layout.customers')}}</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('photos.index')}}">
                        <i class="fa fa-picture-o fa-lg" aria-hidden="true"></i><span>{{trans('admin/layout.photos')}}</span>
                    </a>
                </li>
                <li>
                    <a href="#ecom-nav-list" data-toggle="collapse" aria-expanded="false"><i class="fa fa-bar-chart fa-lg" aria-hidden="true"></i><span>{{trans('admin/layout.marketing')}}</span>
                        <div class="arrow pull-right"><i class="fa fa-angle-down"></i></div>
                    </a>
                    <ul id="ecom-nav-list" class="collapse list-unstyled">
                        <li><a href="{{route('reviews.index')}}">{{trans('admin/layout.reviews')}}</a></li>
                        {{--<li><a href="#">Email Subscriptions</a></li>--}}
                        {{--<li><a href="#">Questions</a></li>--}}
                        {{--<li><a href="#">Vouchers</a></li>--}}
                    </ul>
                </li>
                <li>
                    <a href="#pages-nav-list" data-toggle="collapse" aria-expanded="false"><i class="fa fa-cubes fa-lg" aria-hidden="true"></i><span>{{trans('admin/layout.extra')}}</span>
                        <div class="arrow pull-right"><i class="fa fa-angle-down"></i></div>
                    </a>
                    <ul id="pages-nav-list" class="collapse list-unstyled">
                        <li><a href="{{route('gems.index')}}">{{trans('admin/layout.stones')}}</a></li>
                        <li><a href="{{route('tags.index')}}">{{trans('admin/layout.tags')}}</a></li>
                        <li><a href="{{route('sets.index')}}">{{trans('admin/layout.sets')}}</a></li>
                        <li><a href="{{route('styles.index')}}">{{trans('admin/layout.styles')}}</a></li>
                        <li><a href="{{route('categories.index')}}">{{trans('admin/layout.categories')}}</a></li>
                        <li><a href="{{route('materials.index')}}">{{trans('admin/layout.materials')}}</a></li>
                        <li><a href="{{route('colors.index')}}">{{trans('admin/layout.colors')}}</a></li>
                        <li><a href="{{route('sizes.index')}}">{{trans('admin/layout.sizes')}}</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        @role('superadministrator')
        <div class="admin-menu">
            <ul id="side-admin-menu" class="side-menu list-unstyled">
                <li>
                    <a href="#admin-nav-list" data-toggle="collapse" aria-expanded="false"><i class="fa fa-user-o fa-lg" aria-hidden="true"></i><span>{{trans('admin/layout.admin')}}</span>
                        <div class="arrow pull-right"><i class="fa fa-angle-down"></i></div>
                    </a>
                    <ul id="admin-nav-list" class="collapse list-unstyled">
                        <li><a href="{{route('users.index')}}">{{trans('admin/layout.users')}}</a></li>
                        <li><a href="{{route('roles.index')}}">{{trans('admin/layout.roles')}}</a></li>
                        <li><a href="{{route('permissions.index')}}">{{trans('admin/layout.permissions')}}</a></li>
                        <li><a href="{{route('statuses.index')}}">{{trans('admin/layout.statuses')}}</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#system-nav-list" data-toggle="collapse" aria-expanded="false"><i class="fa fa-cogs fa-lg" aria-hidden="true"></i><span>{{trans('admin/layout.system')}}</span>
                        <div class="arrow pull-right"><i class="fa fa-angle-down"></i></div>
                    </a>
                    <ul id="system-nav-list" class="collapse list-unstyled">
                        <li><a href="{{route('system.logs')}}">{{trans('admin/layout.logs')}}</a></li>
                        <li><a href="{{route('system.info')}}">{{trans('admin/layout.info')}}</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        @endrole
    </div>
</nav>

<!-- Main Content -->
<div class="page home-page">
    <!-- navbar-->
    <header class="header">
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-holder d-flex align-items-center justify-content-between">
                    <div class="navbar-header">
                        <a href="index.html" class="navbar-brand" style="margin-left: 0px;">
                            <div class="brand-text hidden-xs-down">
                                <strong class="text-primary">Dashboard</strong>
                            </div>
                        </a>
                    </div>
                    <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                        <li class="nav-item">
                            <img id="ajax_loader" src="{{asset('images/gifs/ajax_load.gif')}}" style="height: 30px; width: auto;margin-right: 10px;border: none !important">
                        </li>
                        <li class="nav-item dropdown">
                            @php
                                $order_number = \App\Order::where('is_viewed', 0)->count();
                                $reviews_number = \App\Review::where('is_visible', 0)->count();
                                $pending_orders = \App\Order::where('is_viewed', 0)->get();
                                $pending_reviews = \App\Review::where('is_visible', 0)->get();
                                $overall_notifications = $order_number + $reviews_number;
                            @endphp
                            <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
                                <i class="fa fa-bell"></i>
                                @if($overall_notifications > 0)
                                    <span class="badge badge-info">{{$overall_notifications}}</span>
                                @endif
                            </a>
                            <ul aria-labelledby="notifications" class="dropdown-menu total-notifications pre-scrollable">
                                @if($overall_notifications > 0)
                                    @foreach($pending_reviews as $review)
                                        <li>
                                            <a rel="nofollow" href="{{route('reviews.index')}}" class="dropdown-item">
                                                <div class="notification d-flex justify-content-between">
                                                    <div class="notification-content" style="margin-top: 3px">
                                                        <i class="fa fa-tasks" style="color: #197a0f"></i>&nbsp; Review : #{{$review->id}}
                                                    </div>
                                                    <div class="notification-time">
                                                        <small style="color: #197a0f">{{($review->created_at  ? $review->created_at->diffForHumans():'No Date')}}</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                    @if($reviews_number > 0)
                                        <li style="margin: 20px 0;{{($order_number  <= 0)?'margin-bottom:0':''}}">
                                            <a rel="nofollow" style="border: 1px solid #197a0f" href="{{route('reviews.index')}}" class="dropdown-item all-notifications text-center">
                                                <strong>
                                                    <i class="fa fa-tasks fa-sm" style="font-size: 14px"></i>View All Reviews</strong>
                                            </a>
                                        </li>
                                    @endif
                                    @foreach($pending_orders as $pending_order)
                                        <li>
                                            <a rel="nofollow" href="{{route('orders.index')}}" class="dropdown-item">
                                                <div class="notification d-flex justify-content-between">
                                                    <div class="notification-content" style="margin-top: 3px">
                                                        <i class="fa fa-tasks" style="color: #1a237e"></i>&nbsp; Order : #{{$pending_order->id}} ({{$pending_order->status? $pending_order->status->name: ''}})
                                                    </div>
                                                    <div class="notification-time">
                                                        <small style="color: #1a237e">{{($pending_order->created_at  ? $pending_order->created_at->diffForHumans():'No Date')}}</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                    @if($order_number  > 0)
                                        <li style="margin-top: 20px">
                                            <a rel="nofollow" style="border: 1px solid #1a237e" href="{{route('orders.index')}}" class="dropdown-item all-notifications text-center">
                                                <strong>
                                                    <i class="fa fa-tasks fa-sm" style="font-size: 14px"></i>View All Orders</strong>
                                            </a>
                                        </li>
                                    @endif
                                @else
                                    <li>
                                        <a rel="nofollow" style="border: 1px solid #1a237e" class="dropdown-item all-notifications text-center">
                                            <strong><i class="fa fa-tasks fa-sm" style="font-size: 14px"></i>No notifications yet</strong>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link" style="margin-left: 15px;">
                                @if(LaravelLocalization::getCurrentLocale() == 'ru')
                                    <span class="flag-icon flag-icon-ru"></span>
                                @elseif(LaravelLocalization::getCurrentLocale() == 'et')
                                    <span class="flag-icon flag-icon-ee"></span>
                                @else
                                    <span class="flag-icon flag-icon-gb"></span>
                                @endif
                                <i class="zmdi zmdi-chevron-down"></i>
                            </a>
                            <ul aria-labelledby="notifications" class="dropdown-menu" style="width: 240px;">
                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    @if(LaravelLocalization::getCurrentLocale() != $localeCode)
                                        <li>
                                            <a rel="alternate" rel="nofollow" class="dropdown-item" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                                @if($localeCode == 'ru')
                                                    <span class="flag-icon flag-icon-ru local_choice_span"></span>{{ $properties['native'] }} &nbsp; ( RUS )
                                                @elseif($localeCode == 'et')
                                                    <span class="flag-icon flag-icon-ee local_choice_span"></span>{{ $properties['native'] }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ( EST )
                                                @else
                                                    <span class="flag-icon flag-icon-gb local_choice_span"></span>{{ $properties['native'] }} &nbsp;&nbsp;&nbsp; ( ENG )
                                                @endif
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('logout')}}" class="nav-link logout">Logout<i class="fa fa-sign-out fa-lg"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <!-- Page Content -->

@yield('content')

<!-- Notification -->
    @if(Session::has('notification'))
        <div id="snackbar">
            {{session('notification')}}
        </div>
@endif

<!-- Footer -->
    <footer class="main-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <p>Silverest &copy; 2018</p>
                </div>
                <div class="col-sm-6 text-right">
                    <p>Design by <a href="https://bootstrapious.com" class="external">Bootstrapious</a></p>
                    <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
                </div>
            </div>
        </div>
    </footer>
</div>

<!-- Javascript files-->
<script src="{{asset('assets/admin/js/jquery.js')}}"></script>
<script src="{{asset('assets/admin/js/tether.min.js')}}"></script>
<script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/admin/js/jquery.cookie.js')}}"></script>
<script src="{{asset('assets/admin/js/grasp_mobile_progress_circle-1.0.0.min.js')}}"></script>
<script src="{{asset('assets/admin/js/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('assets/admin/js/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/admin/js/sweetalert2.min.js')}}"></script>
<script src="{{asset('assets/admin/js/front.js')}}"></script>

<!-- Snackbar -->
<script>

    $(document).ready(function () {

        $("#snackbar").animate({bottom: 60, opacity: 1});

        setTimeout(function () {
            $("#snackbar").animate({bottom: 0, opacity: 0}, 'fast');
        }, 3000);


        $("#ajax_loader").hide();
    });

</script>

<!-- Ajax loader -->
<script>
    $(document).ajaxStart(function () {
        $("#ajax_loader").show();
    });

    $(document).ajaxStop(function () {
        $("#ajax_loader").hide();
    });
</script>

@yield('scripts')

<!-- Licenses -->
<script>

    /* Licenses */

    /*!
    * Sweetalert (https://sweetalert.js.org/docs/)
    * Copyright (c) 2014-present Tristan Edwards
    * Licensed under MIT (https://github.com/t4t5/sweetalert/blob/master/LICENSE.md)
    */

    /*!
    * Dropzone.js (http://www.dropzonejs.com/)
    * Copyright (c) 2012 Matias Meno <m@tias.me>
    * Logo & Website Design (c) 2015 "1910" www.weare1910.com
    * Licensed under MIT (https://github.com/enyo/dropzone/blob/master/LICENSE)
    */

    /*!
    * Select2 (https://select2.org/)
    * Copyright (c) 2012-2017 Kevin Brown, Igor Vaynberg, and Select2 contributors
    * Licensed under MIT (https://github.com/select2/select2/blob/develop/LICENSE.md)
    */

    /*!
    * Jquery Spinner (https://jqueryui.com/spinner/)
    * Copyright Vasily A., 2015-2017
    * Copyright xixilive, 2013-2015
    * Licensed under MIT (https://github.com/vsn4ik/jquery.spinner/blob/master/LICENSE)
    */

    /*!
    * rateYo (http://rateyo.fundoocode.ninja/)
    * Copyright (c) 2014 prashanth pamidi
    * Licensed under MIT (https://github.com/prrashi/rateYo/blob/master/LICENSE)
    */

    /*!
    * Bootstrap-toggle (http://www.bootstraptoggle.com/)
    * Copyright (c) 2011-2014 Min Hur, The New York Times Company
    * Licensed under MIT (https://github.com/minhur/bootstrap-toggle/blob/master/LICENSE)
    */

    /*!
    * Bootstrap Colorpicker v2.3.3 (http://mjolnic.github.io/bootstrap-colorpicker/)
    * Copyright (c) 2017 Javi Aguilar
    * Licensed under MIT (https://github.com/farbelous/bootstrap-colorpicker/blob/master/LICENSE)
    */

    /*!
    * Animate.css (https://daneden.github.io/animate.css/)
    * Copyright (c) 2018 Daniel Eden
    * Licensed under MIT (https://github.com/daneden/animate.css/blob/master/LICENSE)
    */

    /*!
    * Lightbox (https://lokeshdhakar.com/projects/lightbox2/)
    * Copyright (c) 2015 Lokesh Dhakar
    * Licensed under MIT (https://github.com/lokesh/lightbox2/blob/dev/LICENSE)
    */

    /*!
    * Bootstrap FileStyle v2.1.0 (https://markusslima.github.io/bootstrap-filestyle/index.html)
    * Copyright (c) 2017 Markus Lima
    * Licensed under MIT (https://github.com/markusslima/bootstrap-filestyle/blob/master/LICENSE-MIT)
    */

    /*!
    * Chart.js (https://www.chartjs.org/)
    * Copyright (c) 2018 Chart.js Contributors
    * Licensed under MIT (https://github.com/chartjs/Chart.js/blob/master/LICENSE.md)
    */

    /*!
    * Vue.js (https://vuejs.org/)
    * Copyright (c) 2013-present, Yuxi (Evan) You
    * Licensed under MIT (https://github.com/vuejs/vue/blob/dev/LICENSE)
    */

    /*!
    * Vue Sortable (https://github.com/sagalbot/vue-sortable)
    * Copyright (c) 2016 Jeff Sagal & vue-sortable contributors
    * Licensed under MIT (https://github.com/sagalbot/vue-sortable/blob/master/LICENSE.md)
    */

    /*!
    * Sortable (https://github.com/SortableJS/Vue.Draggable)
    * Copyright (c) 2016 David Desmaisons
    * Licensed under MIT (https://github.com/SortableJS/Vue.Draggable/blob/master/LICENSE)
    */

    /*!
    * # Icons: CC BY 4.0 License (https://creativecommons.org/licenses/by/4.0/)
    * In the Font Awesome Free download, the CC BY 4.0 license applies to all icons
    * packaged as SVG and JS file types.

    * # Fonts: SIL OFL 1.1 License (https://scripts.sil.org/OFL)
    * In the Font Awesome Free download, the SIL OLF license applies to all icons
    * packaged as web and desktop font files.

    * # Code: MIT License (https://opensource.org/licenses/MIT)
    * In the Font Awesome Free download, the MIT license applies to all non-font and
    * non-icon files.
    */

    /*!
    * elevateZoom (http://www.elevateweb.co.uk/image-zoom)
    */
</script>

</body>
</html>
