@extends('layouts.admin')

@section('styles')

    <style>
        a.link-order-status {
            text-decoration: none;
        }

        .dropdown-toggle::after {
            margin-left: 0;
            content: none;
            border-top: 0;
            border-right: 0;
            border-left: 0;
        }

        .dropdown-menu {
            box-shadow: 0px 1px 1px 1px rgba(0, 0, 0, 0.1);
        }
    </style>

    <style>
        .product-list {
            margin-bottom: 20px;
            width: 376px;
            height: 111px;
            /*flex: 0 0 33.333333%;*/
            /*max-width: 33.333333%;*/
            padding-right: 15px;
            padding-left: 15px;
        }

        .product-thumbnail {
            border: 1px solid #dfdfdf;
            width: 100%;
            height: 107px;
        }

        .pro-thumbnail-img {
            float: left;
            width: 32%;
        }

        .pro-thumbnail-info {
            float: left;
            padding-left: 20px;
            width: 68%;
        }

        .product-title-2 {
            color: #666666;
            font-weight: 500;
            margin-top: 15px;
            overflow: hidden;
            text-overflow: ellipsis;
            text-transform: uppercase;
            white-space: nowrap;
            margin-bottom: 5px;
        }

        p.small-title {
            margin-bottom: 0px;
            font-size: 14px !important;
        }

        img {
            width: 105px;
            height: auto;
            border-right: 1px solid #dfdfdf;
        }

        .pro-thumbnail-info span {
            font-size: 14px !important;
            color: #1a237e !important;
        }

    </style>

    <style>
        .payment-details td {
            padding: 5px 0;
        }

        .td-title-2 {
            color: #999;
            font-family: roboto;
            font-weight: 500;
            text-align: right;
        }

        .td-title-1 {
            color: #999;
            font-size: 14px;
            text-align: left;
        }

        .payment-details tr {
            border-bottom: 1px solid #eee;
        }

        .order-total {
            color: #1a237e;
            font-weight: 500;
            text-align: left;
        }

        .order-total-price {
            color: #1a237e;
            font-family: roboto;
            font-weight: 700;
            text-align: right;
        }

        .payment-details {
            width: 100%;
        }

    </style>

    <style>
        .dropdown-item {
            padding-left: 20px;
            padding-right: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .dropdown-header {
            padding-left: 20px;
            padding-right: 10px;
        }

        .tracking_number {
            max-width: 200px;
            float: right;
        }

        .tracking_number input, .tracking_number input:focus {
            color: #999;
            font-family: roboto;
            font-weight: 500;
            font-size: 14px;
            text-align: center;
            outline: 0;
        }

        .tracking_number input:focus {
            border-color: #1a237e;
            box-shadow: none;
        }

        .tracking_number button, .tracking_number button:hover, .tracking_number button:focus {
            background-color: #1a237e !important;
            border-color: #1a237e !important;
            outline: 0;
            box-shadow: none !important;
        }

    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('statuses.index')}}">Statuses</a></li>
                <li class="breadcrumb-item active">Show</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="charts">
        <div class="container-fluid">
            <header>
                <h1 style="margin-bottom: 0px">STATUS CRUD</h1>
            </header>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header" style="height: auto">
                            <h1 class="float-left">Status Details</h1>
                            <a href="{{route('statuses.edit',$status->id)}}" class="btn btn-outline-primary float-right">Edit Status</a>
                        </div>
                        <div class="card-block">
                            <div class="form-group row">
                                <label class="col-sm-4">Value</label>
                                <h2 class="col-sm-4">{{$status->value}}</h2>
                                <div class="col-sm-4">
                                    <div style="height: 25px;width: 25px;background-color: {{$status->value}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="float-left">Orders:</h1>
                                </div>
                            </div>
                        </div>
                        @foreach($status->orders as $order)
                            <div class="col-sm-12" style="margin-bottom: 15px">
                                <div class="card">
                                    <div class="card-header" style="padding: 0px;height: 44px;">
                                        <div class="row">
                                            <div class="col-md-1" style="margin: auto 0;">
                                                <p id="order_viewed{{$order->id}}" style="margin-left: 20px;margin-bottom: 0px;font-size: 16px;{{$order->is_viewed?'':'color:#043180;'}}">
                                                    {{$order->id}}
                                                </p>
                                            </div>
                                            <div class="col-md-5" style="margin: auto 0;">
                                                <div class="row" style="padding: 0 auto;">
                                                    <p class="col-sm-7" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                                        {{$order->delivery ? $order->delivery->customer_email : 'Error'}}
                                                    </p>
                                                    <p class="col-sm-5" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                                        Delivery:
                                                        <span style="font-size: 16px;">{{ucfirst($order->delivery ? $order->delivery->method: 'Error')}}</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-4" style="margin: auto 0;">
                                                <div class="row" style="padding: 0 auto;">
                                                    <p class="col-sm-5" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                                        Total:
                                                        <span style="font-size: 16px;">{{$order->payment ? $order->payment->subtotal . '&euro;' : 'Error'}}</span>
                                                    </p>

                                                    <p class="col-sm-7" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                                        Placed:
                                                        <span style="font-size: 16px">{{($order->created_at  ? $order->created_at->diffForHumans():'No Date')}} </span>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="margin: auto 0;">
                                                <div style="float: right;margin-right: 35px;margin-top: 0px">
                                                    <div class="row" style=" height: 24px;">
                                                        <div class="col-xs-4" style="margin: 0">
                                                        </div>
                                                        <div class="col-xs-4" style="margin: 0">
                                                        </div>
                                                        <div class="col-xs-4" style="margin: 0">
                                                            @if($order->status && ($order->status->id == config('constants.STATUS.DISPATCHED') || $order->status->id == config('constants.STATUS.PREPARATION')))
                                                                <a>
                                                                    <i class="fa fa-square" style="color: {{$order->status->value}}"></i>
                                                                </a>
                                                            @else
                                                                <a class="dropdown-toggle link-order-status">
                                                                    <i class="fa fa-square" style="color: white"></i>
                                                                </a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

