@extends('layouts.admin')

@section('title',"Manage | Gems")

@section('styles')

    <style>
        .sweet-alert button{
            margin: 26px 5px 15px 5px !important;
        }
    </style>

    <style>
        .counter {
            font-size: 17px;
            padding: 8px 10px;
            margin-bottom: 0;
            min-width: 250px;
            float: right;
            font-weight: initial;
            background: #fff;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
            text-align: center
        }

        .create-btn {
            margin-right: 30px;
            padding: 6px 20px;
            margin-top: 1px;
            border-radius: 0;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.2);
        }
    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item active">Gems</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1 style="float: left;margin-bottom: 0px">GEM CRUD</h1>
                <h2 class="counter">Gem Number :
                    <span style="font-size: 17px;color: #0275d8;">{{\App\Gem::all()->count()}}</span>
                </h2>
                <a href="{{route('gems.create')}}" class="btn btn-outline-primary float-right create-btn">Create Gem</a>
            </header>
            <div class="row" style="margin-top: 50px">

                @foreach($gems as $gem)
                    <div class="col-lg-2 col-md-3 col-sm-6">
                        <div class="card">
                            <div class="card-header" style="height: auto">
                                <h1>{{$gem->name}}</h1>
                                <p style="color: #7a8080;margin-bottom: 0px">{{$gem->type}}</p>
                            </div>
                            <div class="card-block" style="text-align: center">
                                <img src="{{$gem->image}}" style="height: 125px; width: 125px; border-radius: 125px;" alt="" class="img-thumbnail">
                                <div class="line" style="margin: 19px 0 18px"></div>
                                <div class="row">
                                    <div class="col-xs-4" style="margin: 0">
                                        <a href="{{route('gems.show',$gem->id)}}" style="margin-left: 20px;margin-right: 20px;color: #515354;"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col-xs-4" style="margin: 0">
                                        <a href="{{route('gems.edit',$gem->id)}}" style="margin-right: 20px;color: #515354;"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col-xs-4" style="margin: 0">
                                        {!! Form::open(['method'=>'DELETE','route'=>['gems.destroy', $gem->id],'class'=>'delete_gem']) !!}
                                        <button type="submit" style="border: none;background: transparent;padding: 0">
                                            <i class="fa fa-trash-o fa-lg" aria-hidden="true" style="color: red"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div style="margin-bottom: 20px">
                {{$gems->links()}}
            </div>
        </div>
    </section>

@endsection

@section('scripts')

    <script>
        $(".delete_gem").submit(function (e) {
            var form = this;
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this gem!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Gem Deleted!',
                            text: 'Gem is successfully deleted!',
                            type: 'success'
                        }, function () {
                            form.submit();
                        });

                    } else {
                        swal("Cancelled", "Your gem is safe", "error");
                    }
                }
            );
        });
    </script>

@endsection


