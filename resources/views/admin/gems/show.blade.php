@extends('layouts.admin')

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('gems.index')}}">Gems</a></li>
                <li class="breadcrumb-item active">Show</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="charts">
        <div class="container-fluid">
            <header>
                <h1>GEM CRUD</h1>
            </header>
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header" style="height: auto">
                            <h1 class="float-left">Gem Details</h1>
                            <a href="{{route('gems.edit',$gem->id)}}" class="btn btn-outline-primary float-right">Edit Gem</a>
                        </div>
                        <div class="card-block" style="padding-bottom: 0px">
                            <div class="form-group row">
                                <label class="col-sm-4">Value</label>
                                <h2 class="col-sm-4">{{$gem->name}}</h2>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4">Type</label>
                                <h2 class="col-sm-4">{{$gem->type}}</h2>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4">Photo</label>
                                <h2 class="col-sm-4">
                                    <img src="{{$gem->image}}" style="height: 50px; width: 50px;" alt="" class="img-thumbnail">
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h1>Products:</h1>
                        </div>
                        <div class="card-block" style="margin-left: 20px">
                            <ul>
                                @foreach($gem->products as $product)
                                    <li>{{$product->name}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

