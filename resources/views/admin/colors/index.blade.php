@extends('layouts.admin')

@section('title',"Manage | Colors")

@section('styles')

    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-colorpicker.min.css')}}">

    <style>
        .sweet-alert button{
            margin: 26px 5px 15px 5px !important;
        }
    </style>

    <style>
        .counter {
            font-size: 17px;
            padding: 8px 10px;
            margin-bottom: 0;
            min-width: 250px;
            float: right;
            font-weight: initial;
            background: #fff;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
            text-align: center
        }
    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item active">Colors</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="charts">
        <div class="container-fluid">
            <header>
                <h1 style="margin-bottom: 0px;float: left">COLOR CRUD</h1>
                <h2 class="counter">Color Number :
                    <span style="font-size: 17px;color: #0275d8;">{{\App\Color::all()->count()}}</span>
                </h2>
            </header>
            <div class="row" style="margin-top: 30px">
                <div class="col-lg-7">
                    <div class="card">
                        <div class="card-header" style="height: auto">
                            <h1 class="float-left">All Colors</h1>
                        </div>
                        <div class="card-block">
                            <table class="table table-lg table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 34px !important;">ID</th>
                                    <th style="width: 34px !important;">Hex</th>
                                    <th>Name</th>
                                    <th>Updated</th>
                                    <th style="width: 130px">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($colors as $color)
                                    <tr>
                                        <th>{{$color->id}}</th>
                                        <td><div style="height: 25px;width: 25px;background-color: {{$color->value}}"></div></td>
                                        <td>{{$color->name}}</td>
                                        <td>{{$color->updated_at ? $color->updated_at->diffForHumans():'No Date'}}</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-xs-4" style="margin: 0">
                                                    <a href="{{route('colors.show',$color->id)}}" style="margin-left: 20px;margin-right: 20px"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="col-xs-4" style="margin: 0">
                                                    <a href="{{route('colors.edit',$color->id)}}" style="margin-right: 20px"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="col-xs-4" style="margin: 0">
                                                    {!! Form::open(['method'=>'DELETE','route'=>['colors.destroy', $color->id],'class'=>'delete_color']) !!}
                                                    <button type="submit" style="border: none;background: transparent;padding: 0">
                                                        <i class="fa fa-trash-o fa-lg" aria-hidden="true" style="color: red"></i>
                                                    </button>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5" style="max-height: 290px">
                    <div class="card">
                        {!! Form::open(['method'=>'POST','route'=>'colors.store','class'=>'form-horizontal']) !!}
                        <div class="card-header">
                            <h1>New Color</h1>
                        </div>
                        <div class="card-block">
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-5">Color Value</label>
                                <div class="col-sm-7">
                                    <div id="color_picker" class="input-group colorpicker-component">
                                        <input name="value" value="#000000" type="text" class="form-control" />
                                        <span class="input-group-addon"><i></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-5">Color Name</label>
                                <div class="col-sm-7">
                                    <div id="color_picker" class="input-group colorpicker-component">
                                        <input name="name" type="text" class="form-control" placeholder="Enter Name..."/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top: 10px;margin-left: 0px">
                                <div class="col-sm-10">
                                    <input type="submit" value="Add Color" class="btn btn-outline-primary">
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div style="margin-bottom: 20px">
                {{$colors->links()}}
            </div>
        </div>
    </section>
@endsection

@section('scripts')

    <!-- Javascript Files -->
    <script src="{{asset('assets/admin/js/bootstrap-colorpicker.min.js')}}"></script>

    <script>
        $(function() {
            $('#color_picker').colorpicker();
        });
    </script>

    <script>
        $(".delete_color").submit(function (e) {
            var form = this;
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this color!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Color Deleted!',
                            text: 'Color is successfully deleted!',
                            type: 'success'
                        }, function () {
                            form.submit();
                        });

                    } else {
                        swal("Cancelled", "Your color is safe", "error");
                    }
                }
            );
        });
    </script>

@endsection
