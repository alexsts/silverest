@extends('layouts.admin')

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('colors.index')}}">Colors</a></li>
                <li class="breadcrumb-item active">Show</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="charts">
        <div class="container-fluid">
            <header>
                <h1 style="margin-bottom: 0px">COLOR CRUD</h1>
            </header>
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header" style="height: auto">
                            <h1 class="float-left">Color Details</h1>
                            <a href="{{route('colors.edit',$color->id)}}" class="btn btn-outline-primary float-right">Edit Color</a>
                        </div>
                        <div class="card-block">
                            <div class="form-group row">
                                <label class="col-sm-4">Value</label>
                                <h2 class="col-sm-4">{{$color->value}}</h2>
                                <div class="col-sm-4">
                                    <div style="height: 25px;width: 25px;background-color: {{$color->value}}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h1>Products:</h1>
                        </div>
                        <div class="card-block" style="margin-left: 20px">
                            <ul>
                                @foreach($color->products as $product)
                                    <li>{{$product->name}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

