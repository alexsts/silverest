@extends('layouts.admin')

@section('styles')

    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-colorpicker.min.css')}}">

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('colors.index')}}">Colors</a></li>
                <li class="breadcrumb-item active">Edit</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1 style="margin-bottom: 0px">COLOR CRUD</h1>
            </header>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        {!! Form::model($color,['method'=>'PATCH','route'=>['colors.update',$color->id],'class'=>'form-horizontal']) !!}
                        <div class="card-header">
                            <h1>Color Details</h1>
                        </div>

                        <div class="card-block">
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Value</label>
                                <div class="col-sm-9">
                                    <div id="color_picker" class="input-group colorpicker-component">
                                        <input name="value" value="{{$color->value}}" type="text" class="form-control" />
                                        <span class="input-group-addon"><i></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Name</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <input name="name" value="{{$color->name}}" type="text" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top: 30px;margin-left: 0px">
                                <div class="col-sm-10">
                                    <input type="submit" value="Update Color" class="btn btn-outline-primary">
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')

    <!-- Javascript Files -->
    <script src="{{asset('assets/admin/js/bootstrap-colorpicker.min.js')}}"></script>

    <script>
        $(function() {
            $('#color_picker').colorpicker();
        });
    </script>

@endsection