@extends('layouts.admin')

@section('title',"Manage | Products")

@section('styles')

    <style>
        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }

        .android {
            margin-left: 30px;
        }

        .dropdown-menu a {
            text-decoration: none;
            color: gray;
            padding: 5px 10px;
        }

        .dropdown-menu a:hover {
            text-decoration: none;
            color: #000000;
        }

        .sweet-alert button {
            margin: 26px 5px 15px 5px !important;
        }

        a.product-code:hover {
            text-decoration: none;
        }

        #snackbar {
            margin-left: -120px;
        }

    </style>

    <style>
        .counter {
            font-size: 17px;
            padding: 8px 10px;
            margin-bottom: 0;
            min-width: 250px;
            float: right;
            font-weight: initial;
            background: #fff;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
            text-align: center
        }
    </style>

    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-toggle.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/lightbox.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('admin/orders.home')}}</a></li>
                <li class="breadcrumb-item active">{{trans('admin/layout.customers')}}</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1 style="float: left;margin-bottom: 0px">{{trans('admin/layout.customers_crud')}}</h1>
                <h2 class="counter">{{trans('admin/layout.customers_number')}} :
                    <span style="font-size: 17px;color: #0275d8;">{{\App\User::all()->count()}}</span>
                </h2>
            </header>

            <div class="row" style="margin-top: 37px">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block" style="text-align: center">
                            <div class="row">
                                <div class="col-md-6" style="margin-bottom: 0px">
                                    <!-- Sort -->
                                    <div class="btn-group" style="float: left">
                                        <a href="{{route('customers.index', ['search'=>request('search')])}}"
                                           class="btn btn-primary">{{trans('admin/orders.sort')}}</a>
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 0px;    background-color: #cbcbcb;">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{route('customers.index', ['search'=>request('search'),'sort'=>'asc'])}}">{{trans('admin/orders.asc')}}</a>
                                            </li>
                                            <li>
                                                <a href="{{route('customers.index', ['search'=>request('search'),'sort'=>'desc'])}}">{{trans('admin/orders.desc')}}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-6" style="margin-bottom: 0px">
                                    <div class="text-right">
                                        {!! Form::open(['method'=>'GET','route'=>'customers.index']) !!}
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="search" placeholder="{{trans('admin/layout.client_name')}}"
                                                   value="{{isset($search_field) ? $search_field : ''}}">
                                            <a href="{{route('customers.index')}}" class="btn btn-secondary" style="height: 38px;border-left: none;display: none"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            <span class="input-group-btn">
                                                   {!! Form::submit('Go!',['class'=>'btn btn-primary']) !!}
                                            </span>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                @foreach($customers as $customer)
                    <div class="col-lg-12" style="margin-bottom: 10px">
                        <div class="card">
                            <div class="card-header" style="padding: 0px;height: 44px;">
                                <div class="row">
                                    <div class="col-md-1" style="margin: auto 0;">
                                        <p style="color: #043180;margin-left: 20px;margin-bottom: 0px;font-size: 16px">
                                            {{$customer->id}}
                                        </p>
                                    </div>
                                    <div class="col-md-2" style="margin: auto 0;">
                                        <p style="color: #043180;margin-bottom: 0px;margin-top:0px;font-size: 16px">
                                            {{str_limit($customer->first_name,9)}} {{$customer->last_name}}
                                        </p>
                                    </div>
                                    <div class="col-md-7" style="margin: auto 0;">
                                        <div class="row" style="padding: 0 auto;">
                                            <p class="col-sm-4" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                                <span style="font-size: 16px;">{{$customer->email}}</span>
                                            </p>
                                            <p class="col-sm-3" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                                Orders:
                                                <span style="font-size: 16px;">{{$customer->orders->count()}}</span>
                                            </p>
                                            <p class="col-sm-2" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                                Wishlist:
                                                <span style="font-size: 16px;">{{$customer->wishlists->count()}} </span>
                                            </p>
                                            <p class="col-sm-3" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 16px">
                                                Joined:
                                                <span style="font-size: 16px">{{($customer->created_at  ? str_limit($customer->created_at->diffForHumans(),8):'No Date')}} </span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="margin: auto 0;">
                                        <div style="float: right;margin-right: 35px;margin-top: 0px">
                                            <div class="row" style=" height: 24px;">
                                                <div class="col-xs-4" style="margin: 0">
                                                    <a href="{{route('customers.show',$customer->id)}}" style="margin-left: 20px;margin-right: 20px;color: #0275d8;cursor: pointer"><i class="fa fa-eye  fa-lg" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="col-xs-4" style="margin: 0">
                                                    {!! Form::open(['method'=>'DELETE','route'=>['customers.destroy', $customer->id],'class'=>'delete_customer']) !!}
                                                    <button type="submit" style="border: none;background: transparent;padding: 0;outline: none;color: red;margin-right: 20px">
                                                        <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
                                                    </button>
                                                    {!! Form::close() !!}
                                                </div>
                                                <div class="col-xs-4" style="margin: 0">
                                                    <a style="color: #515354;" data-toggle="collapse" href="#product-{{$customer->id}}-details" aria-expanded="true" aria-controls="test-block"><i class="fa fa-expand fa-lg" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="product-{{$customer->id}}-details" class="collapse">
                                <div class="card-block" style="border-bottom: 1px solid #f2f2f2; padding-top: 0px;padding-bottom: 10px">
                                    <div class="row">
                                        <div class="col-md-12" style="margin-bottom: 0px">
                                            <h3 style="margin-left: 10px;margin-bottom: 0px;margin-top: 20px;text-decoration: underline">Personal Details:</h3>
                                        </div>
                                        <div class="col-lg-6" style="margin-bottom: 0px;padding-top: 20px">
                                            <div class="row" style="margin-bottom: 20px;">
                                                <div class="col-md-5" style="margin-bottom: 0px">
                                                    <h4 style="margin-bottom: 0px;margin-left: 10px;color: #1a237e">First Name:</h4>
                                                </div>
                                                <div class="col-md-7" style="margin-bottom: 0px">
                                                    <p style="font-size: 17px;margin-bottom: 0px">
                                                        {{$customer->first_name}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom: 20px">
                                                <div class="col-md-5" style="margin-bottom: 0px">
                                                    <h4 style="margin-bottom: 0px;margin-left: 10px;color: #1a237e">Phone:</h4>
                                                </div>
                                                <div class="col-md-7" style="margin-bottom: 0px">
                                                    <p style="font-size: 17px;margin-bottom: 0px">
                                                        {{$customer->phone_number}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom: 20px">
                                                <div class="col-md-5" style="margin-bottom: 0px">
                                                    <h4 style="margin-bottom: 0px;margin-left: 10px;color: #1a237e">Email:</h4>
                                                </div>
                                                <div class="col-md-7" style="margin-bottom: 0px">
                                                    <p style="font-size: 17px;margin-bottom: 0px">
                                                        {{$customer->email}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom: 20px">
                                                <div class="col-md-5" style="margin-bottom: 0px">
                                                    <h4 style="margin-bottom: 0px;margin-left: 10px;color: #1a237e">IP Address:</h4>
                                                </div>
                                                <div class="col-md-7" style="margin-bottom: 0px">
                                                    <p style="font-size: 17px;margin-bottom: 0px">
                                                        {{$customer->ip_address}}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6" style="margin-bottom: 0px;padding-top: 20px">
                                            <div class="row" style="margin-bottom: 20px;">
                                                <div class="col-md-5" style="margin-bottom: 0px">
                                                    <h4 style="margin-bottom: 0px;margin-left: 10px;color: #1a237e">Last Name:</h4>
                                                </div>
                                                <div class="col-md-7" style="margin-bottom: 0px">
                                                    <p style="font-size: 17px;margin-bottom: 0px">
                                                        {{$customer->last_name}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom: 20px">
                                                <div class="col-md-5" style="margin-bottom: 0px">
                                                    <h4 style="margin-bottom: 0px;margin-left: 10px;color: #1a237e">DOB:</h4>
                                                </div>
                                                <div class="col-md-7" style="margin-bottom: 0px">
                                                    <p style="font-size: 17px;margin-bottom: 0px">
                                                        {{date('j F Y', strtotime($customer->dob))}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom: 20px;">
                                                <div class="col-md-5" style="margin-bottom: 0px">
                                                    <h4 style="margin-bottom: 0px;margin-left: 10px;color: #1a237e">Company:</h4>
                                                </div>
                                                <div class="col-md-7" style="margin-bottom: 0px">
                                                    <p style="font-size: 17px;margin-bottom: 0px">
                                                        {{$customer->company_name}}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 0px;margin-top: 0px;border-top: 1px solid #d4d4d4;padding-top: 20px ">
                                            <div class="row">
                                                <div class="col-lg-6" style="margin-bottom: 0px">
                                                    <div class="row" style="margin-bottom: 20px">
                                                        <div class="col-md-12" style="margin-bottom: 0px">
                                                            <h3 style="margin-left: 10px;margin-bottom: 0px;text-decoration: underline">Delivery Address:</h3>
                                                        </div>
                                                    </div>

                                                    @if($customer->address)

                                                        <div class="row" style="margin-bottom: 20px">
                                                            <div class="col-md-5" style="margin-bottom: 0px">
                                                                <h4 style="margin-left: 10px;margin-bottom: 0px;color: #1a237e">Address line:</h4>
                                                            </div>
                                                            <div class="col-md-7" style="margin-bottom: 0px">
                                                                <p style="font-size: 17px;margin-bottom: 0px">
                                                                    {{$customer->address->address_line}}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-bottom: 20px">
                                                            <div class="col-md-5" style="margin-bottom: 0px">
                                                                <h4 style="margin-left: 10px;margin-bottom: 0px;color: #1a237e">Country:</h4>
                                                            </div>
                                                            <div class="col-md-7" style="margin-bottom: 0px">
                                                                <p style="font-size: 17px;margin-bottom: 0px">
                                                                    {{$customer->address->country}}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-bottom: 20px">
                                                            <div class="col-md-5" style="margin-bottom: 0px">
                                                                <h4 style="margin-left: 10px;margin-bottom: 0px;color: #1a237e">Town/City:</h4>
                                                            </div>
                                                            <div class="col-md-7" style="margin-bottom: 0px">
                                                                <p style="font-size: 17px;margin-bottom: 0px">
                                                                    {{$customer->address->city}}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-bottom: 20px">
                                                            <div class="col-md-5" style="margin-bottom: 0px">
                                                                <h4 style="margin-left: 10px;margin-bottom: 0px;color: #1a237e">State:</h4>
                                                            </div>
                                                            <div class="col-md-7" style="margin-bottom: 0px">
                                                                <p style="font-size: 17px;margin-bottom: 0px">
                                                                    {{$customer->address->state}}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-bottom: 20px">
                                                            <div class="col-md-5" style="margin-bottom: 0px">
                                                                <h4 style="margin-left: 10px;margin-bottom: 0px;color: #1a237e">ZIP:</h4>
                                                            </div>
                                                            <div class="col-md-7" style="margin-bottom: 0px">
                                                                <p style="font-size: 17px;margin-bottom: 0px">
                                                                    {{$customer->address->zip}}
                                                                </p>
                                                            </div>
                                                        </div>

                                                    @else

                                                        <div class="row" style="margin-bottom: 20px">
                                                            <div class="col-md-12" style="margin-bottom: 0px">
                                                                <h4 style="margin-left: 10px;margin-bottom: 0px;color: #1a237e">Customer don't have an address</h4>
                                                            </div>
                                                        </div>

                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div style="margin-bottom: 20px;margin-top: 20px">
                {{$customers->links()}}
            </div>
        </div>
    </section>
@endsection


@section('scripts')
    <script src="{{asset('assets/admin/js/lightbox.js')}}"></script>
    <script src="{{asset('assets/admin/js/bootstrap-toggle.js')}}"></script>
    <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.min.js"></script>
    <script>
        $(".delete_customer").submit(function (e) {
            var form = this;
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this customer!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Customer Deleted!',
                            text: 'Customer is successfully deleted!',
                            type: 'success'
                        }, function () {
                            form.submit();
                        });

                    } else {
                        swal("Cancelled", "Your customer is safe", "error");
                    }
                }
            );
        });
    </script>

@endsection


