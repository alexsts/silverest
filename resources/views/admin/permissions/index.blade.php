@extends('layouts.admin')

@section('title',"Manage | Categories")

@section('styles')

    <style>
        .sweet-alert button{
            margin: 26px 5px 15px 5px !important;
        }
    </style>

    <style>
        .counter {
            font-size: 17px;
            padding: 8px 10px;
            margin-bottom: 0;
            min-width: 250px;
            float: right;
            font-weight: initial;
            background: #fff;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
            text-align: center
        }

        .create-btn {
            margin-right: 30px;
            padding: 6px 20px;
            margin-top: 1px;
            border-radius: 0;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.2);
        }
    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item active">Permissions</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="charts">
        <div class="container-fluid">
            <header>
                <h1>PERMISSION CRUD</h1>
                <h2 class="counter">Permission Number :
                    <span style="font-size: 17px;color: #0275d8;">{{\App\Permission::all()->count()}}</span>
                </h2>
                <a href="{{route('permissions.create')}}" class="btn btn-outline-primary float-right create-btn">New Permission</a>
            </header>
            <div class="row" style="margin-top: 30px">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header" style="height: auto">
                            <h1 class="float-left">All Permissions</h1>
                        </div>
                        <div class="card-block">
                            <table class="table table-lg table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Description</th>
                                    <th style="width: 130px">Actions</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach ($permissions as $permission)
                                    <tr>
                                        <th>{{$permission->display_name}}</th>
                                        <td>{{$permission->name}}</td>
                                        <td>{{$permission->description}}</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-xs-4" style="margin: 0">
                                                    <a href="{{route('permissions.show',$permission->id)}}" style="margin-left: 20px;margin-right: 20px"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="col-xs-4" style="margin: 0">
                                                    <a href="{{route('permissions.edit',$permission->id)}}" style="margin-right: 20px"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="col-xs-4" style="margin: 0">
                                                    {!! Form::open(['method'=>'DELETE','route'=>['permissions.destroy', $permission->id],'class'=>'delete_permission']) !!}
                                                    <button type="submit" style="border: none;background: transparent;padding: 0">
                                                        <i class="fa fa-trash-o fa-lg" aria-hidden="true" style="color: red"></i>
                                                    </button>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div style="margin-bottom: 20px">
                {{$permissions->links()}}
            </div>
        </div>
    </section>

@endsection

@section('scripts')

    <script>
        $(".delete_permission").submit(function (e) {
            var form = this;
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this permission!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Permission Deleted!',
                            text: 'Permission is successfully deleted!',
                            type: 'success'
                        }, function () {
                            form.submit();
                        });

                    } else {
                        swal("Cancelled", "Your permission is safe", "error");
                    }
                }
            );
        });
    </script>

@endsection