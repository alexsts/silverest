@extends('layouts.admin')

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('permissions.index')}}">Permissions</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1>PERMISSION CRUD</h1>
            </header>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h1>Create Permission</h1>
                        </div>

                        <div class="card-block">
                            {!! Form::open(['method'=>'POST','route'=>'permissions.store','class'=>'form-horizontal']) !!}
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Name</label>
                                <div class="col-sm-9">
                                    <input name="display_name" type="text" placeholder="Name" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Slug</label>
                                <div class="col-sm-9">
                                    <input name="name" type="text" placeholder="Slug" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Description</label>
                                <div class="col-sm-9">
                                    <input name="description" type="text" placeholder="Description" class="form-control form-control-success">
                                </div>
                            </div>

                            <div class="form-group row" style="margin-top: 30px">
                                <div class="col-sm-10">
                                    <input type="submit" value="Create Permission" class="btn btn-outline-primary">
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

