@extends('layouts.admin')

@section('title',"Manage | Products")


@section('styles')

    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-toggle.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/lightbox.min.css')}}">

    <style>
        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }

        .android {
            margin-left: 30px;
        }

        .dropdown-menu a {
            text-decoration: none;
            color: gray;
            padding: 5px 10px;
        }

        .dropdown-menu a:hover {
            text-decoration: none;
            color: #000000;
        }

        .sweet-alert button {
            margin: 26px 5px 15px 5px !important;
        }

        a.product-code:hover {
            text-decoration: none;
        }

        #snackbar {
            margin-left: -120px;
        }

    </style>

    <style>
        .counter {
            font-size: 17px;
            padding: 8px 10px;
            margin-bottom: 0;
            min-width: 250px;
            float: right;
            font-weight: initial;
            background: #fff;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
            text-align: center
        }

        .create-btn {
            margin-right: 30px;
            padding: 6px 20px;
            margin-top: 1px;
            border-radius: 0;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.2);
        }
    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">{{trans('admin/products.home')}}</a></li>
                <li class="breadcrumb-item active">{{trans('admin/products.products')}}</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1 style="float: left;margin-bottom: 0px">{{trans('admin/products.crud')}}</h1>
                <h2 class="counter">{{trans('admin/products.product_number')}} :
                    <span style="font-size: 17px;color: #0275d8;">{{\App\Product::all()->count()}}</span>
                </h2>
                <a href="{{route('products.create')}}" class="btn btn-outline-primary float-right create-btn">{{trans('admin/products.create_product')}}</a>
            </header>

            <div class="row" style="margin-top: 37px">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block" style="text-align: center">
                            <div class="row">
                                <div class="col-md-6" style="margin-bottom: 0px">
                                    <!-- Sort -->
                                    <div class="btn-group" style="float: left">
                                        <a href="{{route('products.index', ['search'=>request('search')])}}"
                                           class="btn btn-primary">{{trans('admin/products.date_sort')}}</a>
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 0px;    background-color: #cbcbcb;">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{route('products.index', ['search'=>request('search'),'sort_date'=>'asc'])}}">{{trans('admin/products.ascending')}}</a>
                                            </li>
                                            <li>
                                                <a href="{{route('products.index', ['search'=>request('search'),'sort_date'=>'desc'])}}">{{trans('admin/products.descending')}}</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="btn-group" style="float: left;margin-left: 30px">
                                        <a href="{{route('products.index', ['search'=>request('search')])}}"
                                           class="btn btn-primary">{{trans('admin/products.code_sort')}}</a>
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-radius: 0px;    background-color: #cbcbcb;">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{route('products.index', ['search'=>request('search'),'sort_code'=>'asc'])}}">{{trans('admin/products.ascending')}}</a>
                                            </li>
                                            <li>
                                                <a href="{{route('products.index', ['search'=>request('search'),'sort_code'=>'desc'])}}">{{trans('admin/products.descending')}}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-md-6" style="margin-bottom: 0px">
                                    <div class="text-right">
                                        {!! Form::open(['method'=>'GET','route'=>'products.index']) !!}
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="search" placeholder="{{trans('admin/products.keyword')}}..."
                                                   value="{{isset($search_field) ? $search_field : ''}}">
                                            <a href="{{route('products.index')}}" class="btn btn-secondary" style="height: 38px;border-left: none;display: none"><i class="fa fa-times" aria-hidden="true"></i></a>
                                            <span class="input-group-btn">
                                                   {!! Form::submit('Go!',['class'=>'btn btn-primary']) !!}
                                            </span>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                @foreach($products as $product)
                    <div class="col-lg-12" style="margin-bottom: 10px">
                        <div class="card">
                            <div class="card-header" style="padding: 0px">
                                <div class="row">
                                    <div class="col-md-1" style="margin: auto auto;">
                                        @if($product->photos->count() > 0)
                                            <img src="{{$product->photos->first()->getPreviewPath()}}" style="height: 50px; width: 50px; border-radius: 0px;border: none;border-right: 1px solid #f2f2f2;border-bottom: 1px solid #f2f2f2;" alt="" class="img-thumbnail">
                                        @else
                                            <img src="https://www.greatmats.com/images/placeholder-all.png" style="height: 50px; width: 50px; border-radius: 0px;border: none;border-right: 1px solid #f2f2f2;border-bottom: 1px solid #f2f2f2;" alt="" class="img-thumbnail">
                                        @endif
                                    </div>
                                    <div class="col-md-3" style="margin: auto 0;">
                                        <p style="color: #043180;margin-bottom: 0px;font-size: 1.2rem">
                                            <span style="color: #043180;">{{$product->name}}</span>
                                            <span>(<a class="product-code" href="{{route('catalogue.product',$product->id)}}" target="_blank">{{$product->code}}</a>)</span>
                                            @if($product->is_featured)
                                                <i class='fa fa-star-o' aria-hidden='true' style="color: #ccc725;"></i>
                                            @endif
                                        </p>
                                    </div>
                                    <div class="col-md-4" style="margin: auto 0;">
                                        <div class="row" style="padding: 0 auto;">
                                            @if($product->getDiscountPrice())
                                                <p class="col-sm-5" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 1.2rem">
                                                    <span style="color: red">{{$product->getDiscountPrice()}} &euro;</span>
                                                </p>
                                            @else
                                                <p class="col-sm-5" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;color: #043180;font-size: 1.2rem">
                                                    <span style="color: #043180;">{{$product->price}} &euro;</span>
                                                </p>
                                            @endif
                                            <p class="col-sm-4" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;font-size: 1.2rem">
                                                <span>Views:</span> <span style="color: #043180;">{{$product->views_number}}</span>
                                            </p>
                                            <p class="col-sm-3" style=" padding-top: 10px;padding-bottom: 10px;margin: 0px;font-size: 1.2rem">
                                                <span>Sold:</span> <span style="color: #043180;">{{$product->sold_number}}</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="margin: auto 0;text-align: right">
                                        <input class="spinner" id="{{$product->id}}" name="quantity{{$product->id}}" min="0" style="width: 35px;height: 23px;margin-right: 20px" value="{{$product->quantity}}" disabled>
                                        <input class="toggle-event" type="checkbox" {{$product->is_visible == 1 ? 'checked':''}} value="{{$product->id}}" data-toggle="toggle" data-style="android" data-on="&nbsp;<i class='fa fa-eye fa-lg' aria-hidden='true'></i>" data-off="<i class='fa fa-eye-slash fa-lg' aria-hidden='true'></i>&nbsp;" data-width="60" data-height="20" data-size="small" data-onstyle="primary">
                                    </div>
                                    <div class="col-md-2" style="margin: auto 0;">
                                        <div style="float: right;margin-right: 35px;margin-top: 0px">
                                            <div class="row" style=" height: 24px;">
                                                <div class="col-xs-4" style="margin: 0">
                                                    <a href="{{route('products.edit',$product->id)}}" style="margin-left: 20px;margin-right: 20px;color: #0275d8;cursor: pointer"><i class="fa fa-pencil  fa-lg" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="col-xs-4" style="margin: 0">
                                                    {!! Form::open(['method'=>'DELETE','route'=>['products.destroy', $product->id],'class'=>'delete_product']) !!}
                                                    <button type="submit" style="border: none;background: transparent;padding: 0;outline: none;color: red;margin-right: 20px">
                                                        <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
                                                    </button>
                                                    {!! Form::close() !!}
                                                </div>
                                                <div class="col-xs-4" style="margin: 0">
                                                    <a style="color: #515354;" data-toggle="collapse" href="#product-{{$product->id}}-details" aria-expanded="true" aria-controls="test-block"><i class="fa fa-expand fa-lg" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="product-{{$product->id}}-details" class="collapse">
                                <div class="card-block" style="border-bottom: 1px solid #f2f2f2; padding-top: 0px;padding-bottom: 10px">
                                    <div class="row">
                                        <div class="col-lg-6" style="margin-bottom: 0px;border-right: 1px solid #d4d4d4;padding-top: 20px">
                                            <div class="row">
                                                @if($product->photos->count() > 0)
                                                    <div class="col-md-6">
                                                        <a href="{{$product->photos->first()->getPreviewPath()}}" data-lightbox="gallery-{{$product->id}}">
                                                            <img src="{{$product->photos->first()->getPreviewPath()}}" alt="{{$product->photos->first()->id}}" style="height: 240px; width: 240px;border-radius: 0;" class="img-thumbnail">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="row" style="margin-left: 0px;">
                                                            @foreach ($product->photos as $photo)
                                                                @if ($loop->first)
                                                                @else
                                                                    <a href="{{$photo->getPreviewPath()}}" data-lightbox="gallery-{{$product->id}}">
                                                                        <img src="{{$photo->getPreviewPath()}}" alt="{{$photo->id}}" style="height: 110px; width: 110px; border-radius: 0;margin-right: 20px;margin-bottom: 21px" class="img-thumbnail">
                                                                    </a>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6" style="margin-bottom: 0px;padding-top: 20px">
                                            <div class="row" style="margin-bottom: 20px;">
                                                <div class="col-md-3" style="margin-bottom: 0px">
                                                    <h3 style="margin-left: 10px;margin-bottom: 0px">{{trans('admin/products.created')}}:</h3>
                                                </div>
                                                <div class="col-md-9" style="margin-bottom: 0px">
                                                    <p style="font-size: 17px;margin-bottom: 0px">
                                                        {{$product->created_at->format('d M Y h:m')}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom: 20px">
                                                <div class="col-md-3" style="margin-bottom: 0px">
                                                    <h3 style="margin-left: 10px;margin-bottom: 0px">{{trans('admin/products.material')}}:</h3>
                                                </div>
                                                <div class="col-md-9" style="margin-bottom: 0px">
                                                    <p style="font-size: 17px;margin-bottom: 0px">
                                                        {{$product->material ? $product->material->name : 'None'}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom: 20px;">
                                                <div class="col-md-3" style="margin-bottom: 0px">
                                                    <h3 style="margin-left: 10px;margin-bottom: 0px">{{trans('admin/products.weight')}}:</h3>
                                                </div>
                                                <div class="col-md-9" style="margin-bottom: 0px">
                                                    <p style="font-size: 17px;margin-bottom: 0px">
                                                        {{$product->weight}} {{trans('admin/products.grams')}}
                                                    </p>
                                                </div>
                                            </div>


                                            @if($product->getDiscountPrice())

                                                <div class="row" style="margin-bottom: 20px">
                                                    <div class="col-md-3" style="margin-bottom: 0px">
                                                        <h3 style="margin-left: 10px;margin-bottom: 0px">{{trans('admin/products.discount')}}:</h3>
                                                    </div>
                                                    <div class="col-md-9" style="margin-bottom: 0px">
                                                        <p style="font-size: 17px;margin-bottom: 0px">
                                                            <span style="border: 2px solid #1a237e;color: #1a237e;padding: 5px 10px;margin-right: 20px">- {{$product->discount_rate}} %</span> Ends in {{$product->discount_end_date->diffForHumans()}}
                                                        </p>
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="row" style="margin-bottom: 20px">
                                                <div class="col-md-3" style="margin-bottom: 0px">
                                                    <h3 style="margin-left: 10px;margin-bottom: 0px">{{trans('admin/products.sizes')}}:</h3>
                                                </div>
                                                <div class="col-md-9" style="margin-bottom: 0px">
                                                    <p style="font-size: 17px;margin-bottom: 0px">
                                                        @foreach($product->sizes as $size)
                                                            @if ($loop->last)
                                                                {{$size->value}}
                                                            @else
                                                                {{$size->value}}
                                                                <span style="color: #77797a;font-size: 19px">,</span>
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom: 20px">
                                                <div class="col-md-3" style="margin-bottom: 0px">
                                                    <h3 style="margin-left: 10px;margin-bottom: 0px">{{trans('admin/products.gems')}}:</h3>
                                                </div>
                                                <div class="col-md-9" style="margin-bottom: 0px">
                                                    <p style="margin-bottom: 0px">
                                                        @foreach($product->gems as $gem)
                                                            <img  src="{{$gem->image}}" style="height: 40px; width: 40px;padding: 0.1rem; border-radius: 50px;margin-right: 5px" alt="" class="img-thumbnail tippy-gem" title="{{$gem->name}}  ({{$gem->type}})">
                                                        @endforeach
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12" style="margin-bottom: 0px;margin-top: 0px;border-top: 1px solid #d4d4d4;padding-top: 20px ">
                                            <div class="row" style="margin-bottom: 0px">
                                                <div class="col-md-3" style="margin-bottom: 0px;">
                                                    <h3 style="margin-bottom: 20px">{{trans('admin/products.colors')}}:</h3>
                                                </div>
                                                <div class="col-md-9" style="margin-bottom: 0px">
                                                    <p style="margin-bottom: 0px">
                                                        @foreach($product->colors as $color)
                                                            <img style="height: 25px;width: 25px;margin-right:5px;margin-bottom: 10px; border: none; padding: 0px; background-color: {{$color->value}}" class="img-thumbnail"></img>
                                                        @endforeach
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom: 0px">
                                                <div class="col-md-3" style="margin-bottom: 0px">
                                                    <h3 style="margin-bottom: 20px">{{trans('admin/products.categories')}}:</h3>
                                                </div>
                                                <div class="col-md-9" style="margin-bottom: 0px">
                                                    <p style="margin-bottom: 0px">
                                                        @foreach($product->subcategories as $subcategory)
                                                            <span class="badge badge-primary" style="margin-right: 5px;margin-bottom: 10px; font-size: 15px; color: white;border-radius: 4px;padding: 5px 10px; background-color: #2f4ddb">{{$subcategory->category->name}} ( {{$subcategory->name}} )</span>
                                                        @endforeach
                                                        @foreach($product->categories as $category)
                                                            @if( !($category->subcategories()->count() > 0) )
                                                                <span class="badge badge-primary" style="margin-right: 5px;margin-bottom: 10px; font-size: 15px; color: white;border-radius: 4px;padding: 5px 10px; background-color: #2f4ddb">{{$category->name}}</span>
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom: 0px">
                                                <div class="col-md-3" style="margin-bottom: 0px">
                                                    <h3 style="margin-bottom: 20px">{{trans('admin/products.styles')}}:</h3>
                                                </div>
                                                <div class="col-md-9" style="margin-bottom: 0px">
                                                    <p style="margin-bottom: 0px">
                                                        @foreach($product->styles as $style)
                                                            <span class="badge badge-primary" style="margin-right: 5px;margin-bottom: 10px; font-size: 15px; color: white;border-radius: 4px;padding: 5px 10px; background-color: #5e7bcf">{{$style->name}}</span>
                                                        @endforeach
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom: 0px">
                                                <div class="col-md-3" style="margin-bottom: 0px">
                                                    <h3 style="margin-bottom: 0;margin-bottom: 10px">{{trans('admin/products.tags')}}:</h3>
                                                </div>
                                                <div class="col-md-9" style="margin-bottom: 0px">
                                                    <p style="margin-bottom: 0px">
                                                        @foreach($product->tags as $tag)
                                                            <span class="badge badge-primary" style="margin-right: 5px;margin-bottom: 10px; font-size: 15px; color: white;border-radius: 4px;padding: 5px 10px; background-color: #aaaaaa">{{$tag->name}}</span>
                                                        @endforeach
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div style="margin-bottom: 20px;margin-top: 20px">
                {{$products->links()}}
            </div>
        </div>
    </section>

@endsection


@section('scripts')

    <script src="{{asset('assets/admin/js/lightbox.js')}}"></script>
    <script src="{{asset('assets/admin/js/bootstrap-toggle.js')}}"></script>
    <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/tippy.all.min.js')}}"></script>

    <script>
        $(function () {
            $('.toggle-event').change(function () {
                var is_visible = $(this).is(":checked");
                var product_id = $(this).val();
                var data = {_token: '{!! csrf_token() !!}', product_id: product_id, is_visible: is_visible}

                console.log(data);

                $.ajax({
                    'url': "{{route('product.visible')}}",
                    'method': 'POST',
                    'data': data,
                    success: function (msg) {
                        console.log(msg)
                    }
                });
            });

            // disable visible switch if quantity less than 1
            $('input[name*="quantity"]').each(function () {

                var product_id = $(this).attr('id');
                var quantity = $(this).val();

                console.log(quantity);

                if (quantity < 1) {
                    $("input[value=" + product_id + "][class=toggle-event]").bootstrapToggle('disable');
                }
            });

            $('input[name*="quantity"]').spinner({
                spin: function (event, ui) {
                    updateQuantity(ui.value, $(this).attr("id"));
                }
            });

            // reset button:
            if ($("input[name = 'search']").val().length > 0) {
                $(".btn-secondary").show();
            }
        })

        function updateQuantity(quantity, product_id) {

            var data = {_token: '{!! csrf_token() !!}', product_id: product_id, quantity: quantity}

            if (quantity < 1) {

                $("input[value=" + product_id + "][class=toggle-event]").bootstrapToggle('off');
                $("input[value=" + product_id + "][class=toggle-event]").bootstrapToggle('disable');

                var is_visible = false;
                var data2 = {_token: '{!! csrf_token() !!}', product_id: product_id, is_visible: is_visible}

                console.log(data2);

                $.ajax({
                    'url': "{{route('product.visible')}}",
                    'method': 'POST',
                    'data': data2,
                    success: function (msg) {
                        console.log(msg)
                    }
                });
            } else {
                $("input[value=" + product_id + "][class=toggle-event]").bootstrapToggle('enable');
            }

            $.ajax({
                'url': "{{route('product.quantity')}}",
                'method': 'POST',
                'data': data,
                success: function (msg) {
                    console.log(msg)
                }
            });
        }
    </script>

    <script>
        $(".delete_product").submit(function (e) {
            var form = this;
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this product!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Product Deleted!',
                            text: 'Product is successfully deleted!',
                            type: 'success'
                        }, function () {
                            form.submit();
                        });

                    } else {
                        swal("Cancelled", "Your product is safe", "error");
                    }
                }
            );
        });
    </script>

    <script>
        tippy('.tippy-gem', {
            placement: 'bottom',
            animation: 'perspective',
            interactiveBorder: 5,
            animateFill: true,
            size: 'large',
        })
    </script>

@endsection


