@extends('layouts.admin')

@section('styles')

    <style>
        .sweet-alert button{
            margin: 26px 5px 15px 5px !important;
        }
    </style>

    <style>
        .counter {
            font-size: 17px;
            padding: 8px 10px;
            margin-bottom: 0;
            min-width: 250px;
            float: right;
            font-weight: initial;
            background: #fff;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
            text-align: center
        }
    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('categories.index')}}">Categories</a></li>
                <li class="breadcrumb-item active">Show</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="charts">
        <div class="container-fluid">
            <header>
                <h1 style="float: left">CATEGORY CRUD</h1>
                <h2 class="counter">Subcategory Number :
                    <span style="font-size: 17px;color: #0275d8;">{{$category->subcategories->count()}}</span>
                </h2>
            </header>
            <div class="row" style="margin-top: 30px">
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header" style="height: auto">
                                    <h1 class="float-left">Category Details</h1>
                                    <a href="{{route('categories.edit',$category->id)}}" class="btn btn-outline-primary float-right">Edit Category</a>
                                </div>
                                <div class="card-block">
                                    <div class="form-group row">
                                        <label class="col-sm-4">Name</label>
                                        <h2 class="col-sm-8">{{$category->name}}</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1>Subcategories:</h1>
                                </div>
                                <div class="card-block">
                                    <table class="table table-lg table-bordered">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Updated</th>
                                            <th style="width: 100px">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($category->subcategories as $subcategory)

                                            <tr>
                                                <th>{{$subcategory->id}}</th>
                                                <td>{{$subcategory->name}}</td>
                                                <td>{{$subcategory->updated_at ? $subcategory->updated_at->diffForHumans():'No Date'}}</td>
                                                <td>
                                                    <div class="row">
                                                        <div class="col-xs-4" style="margin: 0">
                                                            <a href="{{route('subcategories.edit',$subcategory->id)}}" style="margin-left: 20px;margin-right: 20px"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></a>
                                                        </div>
                                                        <div class="col-xs-4" style="margin: 0">
                                                            {!! Form::open(['method'=>'DELETE','route'=>['subcategories.destroy', $subcategory->id],'class'=>'delete_category']) !!}
                                                            <button type="submit" style="border: none;background: transparent;padding: 0">
                                                                <i class="fa fa-trash-o fa-lg" aria-hidden="true" style="color: red"></i>
                                                            </button>
                                                            {!! Form::close() !!}
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5" style="max-height: 465px">
                    <div class="card">
                        {!! Form::open(['method'=>'POST','route'=>'subcategories.store','class'=>'form-horizontal']) !!}
                        <div class="card-header">
                            <h1>New Subcategory</h1>
                        </div>
                        <div class="card-block">
                            <div class="form-group row" style="margin: 0;margin-top: 20px">
                                <label class="col-sm-5">Name (ENG)</label>
                                <div class="col-sm-7">
                                    <input name="name_en" type="text" placeholder="enter name..." value="{{old('name_en')}}" class="form-control form-control-success" required>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-5">Name (RUS)</label>
                                <div class="col-sm-7">
                                    <input name="name_ru" type="text" placeholder="enter name..." value="{{old('name_ru')}}" class="form-control form-control-success" required>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-5">Name (EST)</label>
                                <div class="col-sm-7">
                                    <input name="name_et" type="text" placeholder="enter name..." value="{{old('name_et')}}" class="form-control form-control-success" required>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-5">Code</label>
                                <div class="col-sm-7">
                                    <input name="code" type="text" placeholder="enter name..." value="{{old('code')}}" class="form-control form-control-success" required>
                                </div>
                            </div>
                            <input type="hidden" name="category_id" value="{{$category->id}}">
                            <div class="form-group row" style="margin-top: 30px;margin-left: 0px">
                                <div class="col-sm-10">
                                    <input type="submit" value="Add Subcategory" class="btn btn-outline-primary">
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

    <script>
        $(".delete_category").submit(function (e) {
            var form = this;
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this subcategory!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Subcategory Deleted!',
                            text: 'Subcategory is successfully deleted!',
                            type: 'success'
                        }, function () {
                            form.submit();
                        });

                    } else {
                        swal("Cancelled", "Your subcategory is safe", "error");
                    }
                }
            );
        });
    </script>

@endsection


