@extends('layouts.admin')

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('sets.index')}}">Sets</a></li>
                <li class="breadcrumb-item active">Show</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="charts">
        <div class="container-fluid">
            <header>
                <h1>SET CRUD</h1>
            </header>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header" style="height: auto">
                            <h1 class="float-left">Set Details</h1>
                            <a href="{{route('sets.edit',$set->id)}}" class="btn btn-outline-primary float-right">Edit Set</a>
                        </div>
                        <div class="card-block">
                            <div class="form-group row">
                                <label class="col-sm-4">Name</label>
                                <h2 class="col-sm-8">{{$set->name}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
                @foreach($set->products as $product)
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="card">
                            <div class="card-header" style="height: auto">
                                <h1>{{$product->name}}</h1>
                                <p>{{$product->code}}</p>
                            </div>
                            <div class="card-block" style="text-align: center">
                                <img src="{{$product->photos->first()->getPreviewPath()}}" style="" alt="" class="img-thumbnail">
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </section>

@endsection

