@extends('layouts.admin')

@section('title',"Manage | Categories")

@section('styles')

    <style>
        .sweet-alert button{
            margin: 26px 5px 15px 5px !important;
        }
    </style>

    <style>
        .counter {
            font-size: 17px;
            padding: 8px 10px;
            margin-bottom: 0;
            min-width: 250px;
            float: right;
            font-weight: initial;
            background: #fff;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
            text-align: center
        }
    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item active">Sizes</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="charts">
        <div class="container-fluid">
            <header>
                <h1 style="float:left">SIZE CRUD</h1>
                <h2 class="counter">Size Number :
                    <span style="font-size: 17px;color: #0275d8;">{{\App\Size::all()->count()}}</span>
                </h2>
            </header>
            <div class="row" style="margin-top: 30px">
                <div class="col-lg-7">
                    <div class="card">
                        <div class="card-header" style="height: auto">
                            <h1 class="float-left">All Sizes</h1>
                        </div>
                        <div class="card-block">
                            <table class="table table-lg table-bordered">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Value</th>
                                    <th>Updated</th>
                                    <th style="width: 130px">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sizes as $size)
                                    <tr>
                                        <th>{{$size->id}}</th>
                                        <td>{{$size->value}}</td>
                                        <td>{{$size->updated_at ? $size->updated_at->diffForHumans():'No Date'}}</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-xs-4" style="margin: 0">
                                                    <a href="{{route('sizes.show',$size->id)}}" style="margin-left: 20px;margin-right: 20px"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="col-xs-4" style="margin: 0">
                                                    <a href="{{route('sizes.edit',$size->id)}}" style="margin-right: 20px"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="col-xs-4" style="margin: 0">
                                                    {!! Form::open(['method'=>'DELETE','route'=>['sizes.destroy', $size->id],'class'=>'delete_size']) !!}
                                                    <button type="submit" style="border: none;background: transparent;padding: 0">
                                                        <i class="fa fa-trash-o fa-lg" aria-hidden="true" style="color: red"></i>
                                                    </button>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5" style="max-height: 250px">
                    <div class="card">
                        {!! Form::open(['method'=>'POST','route'=>'sizes.store','class'=>'form-horizontal']) !!}
                        <div class="card-header">
                            <h1>New Size</h1>
                        </div>
                        <div class="card-block">
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-5">Size Value</label>
                                <div class="col-sm-7">
                                    <input name="value" type="number" step=0.5 placeholder="enter value..." value="{{old('value')}}" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top: 30px;margin-left: 0px">
                                <div class="col-sm-10">
                                    <input type="submit" value="Add Size" class="btn btn-outline-primary">
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div style="margin-bottom: 20px">
                {{$sizes->links()}}
            </div>
        </div>
    </section>

@endsection

@section('scripts')

    <script>
        $(".delete_size").submit(function (e) {
            var form = this;
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this size!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Size Deleted!',
                            text: 'Size is successfully deleted!',
                            type: 'success'
                        }, function () {
                            form.submit();
                        });

                    } else {
                        swal("Cancelled", "Your size is safe", "error");
                    }
                }
            );
        });
    </script>

@endsection