@extends('layouts.admin')

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('users.index')}}">Users</a></li>
                <li class="breadcrumb-item active">Show</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="charts">
        <div class="container-fluid">
            <header>
                <h1>USER CRUD</h1>
            </header>
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header" style="height: auto">
                            <h1 class="float-left">User Details</h1>
                            <a href="{{route('users.edit',$user->id)}}" class="btn btn-outline-primary float-right">Edit User</a>
                        </div>
                        <div class="card-block">
                            <div class="form-group row">
                                <label class="col-sm-4">First Name</label>
                                <h2 class="col-sm-8">{{$user->first_name}}</h2>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4">Last Name</label>
                                <h2 class="col-sm-8">{{$user->last_name}}</h2>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4">Email</label>
                                <h2 class="col-sm-8">{{$user->email}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h1>Roles Assigned:</h1>
                        </div>
                        <div class="card-block" style="margin-left: 20px">
                            <ul>
                                @foreach($user->roles as $role)
                                    <li>{{$role->display_name}} <em>({{$role->description}})</em></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('scripts')

    <script>
        $(function () {
            $('#checkboxCustom1').change(function () {
                $('#generate_password').toggle(!this.checked);
            }).change(); //ensure visible state matches initially
        });
    </script>

@endsection
