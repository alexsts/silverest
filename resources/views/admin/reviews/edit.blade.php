@extends('layouts.admin')

@section('styles')

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{asset('assets/admin/themes/classic.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/themes/classic.date.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/rangeslider.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-toggle.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/dropzone.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/select2.min.css')}}">

    <!-- Dropdown Elements -->
    <style>
        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }

        .dropdown-menu a {
            text-decoration: none;
            color: gray;
            padding: 5px 10px;
        }

        .dropdown-menu a:hover {
            text-decoration: none;
            color: #000000;
            font-size: 1rem;
            font-weight: bold;
            line-height: 1.25;
            color: #464a4c;
        }

        .card-header {
            background-color: #fcfcfc;
        }

        section.forms label {
            font-size: 1em;
        }

        section.forms textarea {
            font-size: 1.1em;
        }
    </style>

    <!-- Select Elements -->
    <style>
        section span {
            font-size: 0.98em !important;
            display: block;
        }

        span.select2-selection.select2-selection--multiple {
            width: 100% !important;
            border: 1px solid rgba(0, 0, 0, .15);
            border-radius: 0px;
        }

        .select2-container--default.select2-container--focus {
            border-radius: 0px !important;
            width: 100% !important;
        }

        .select2-container--open .select2-dropdown--below {
            border: 1px solid #aaa;
        }

        span.select2.select2-container.select2-container--default.select2-container--below.select2-container--focus.select2-container--open {
            width: 100% !important;
        }

        .select2-results__option[aria-selected=true] {
            display: none;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: solid #279aff 1px !important;
        }

        .select2 .select2-container .select2-container--default .select2-container--below {
            width: 100% !important;
        }

        .select2-container--default .select2-selection--single {
            border-radius: 0px !important;
            outline: none !important;
        }

        .select2-container .select2-selection--single {
            height: 38px;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            top: 5px;
            right: 10px;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            padding-top: 4px;
            margin-left: 4px;
            font-size: 16px !important;
        }

        span.select2-dropdown.select2-dropdown--below {
            border-top: none !important;
        }

        .select2-container--default .select2-selection--single {
            border: 1px solid rgba(170, 170, 170, 0.46);
        }

        .select2-search__field {
            height: 25px;
        }

        span.select2-selection__choice__remove {
            margin-left: 8px;
            float: right;
        }

        .color_select span.select2-selection__choice__remove {
            padding-top: 2px;
            margin-left: 22px;
        }

        .gem_select span.select2-selection__choice__remove {
            margin-left: 30px;
        }

        li.select2-selection__choice {
            padding: 3px !important;
            padding-left: 4px !important;
            padding-right: 4px !important;
        }

        .color_select li.select2-selection__choice {
            padding: 1px !important;
            padding-left: 4px !important;
            padding-right: 4px !important;

        }
    </style>

    <!-- File Upload Select Elements -->
    <style>

        .images ul {
            list-style-type: none;
        }

        .images li {
            display: inline-block;
        }

        .images input[type="checkbox"][id^="photo_"] {
            display: none;
        }

        .images label {
            border: 1px solid #dddddd;
            padding: 10px;
            display: block;
            position: relative;
            margin: 10px;
            cursor: pointer;
        }

        .images label:before {
            background-color: white;
            color: white;
            content: " ";
            display: block;
            border-radius: 50%;
            border: 1px solid grey;
            position: absolute;
            top: -5px;
            left: -5px;
            width: 25px;
            height: 25px;
            text-align: center;
            line-height: 28px;
            transition-duration: 0.4s;
            transform: scale(0);
        }

        .images label img {
            height: 94px;
            width: 94px;
            transition-duration: 0.2s;
            transform-origin: 50% 50%;
        }

        .images :checked + label {
            border-color: #ddd;
        }

        .images :checked + label:before {
            content: "✓";
            background-color: grey;
            transform: scale(1);
        }

        .images :checked + label img {
            transform: scale(0.9);
            box-shadow: 0 0 5px #333;
            z-index: -1;
        }
    </style>

    <!-- Discount -->
    <style>
        .form-control:disabled, .form-control[readonly] {
            background-color: #ffffff;
        }

        .add-on .input-group-btn > .btn {
            border-left-width: 0;
            left: -2px;
            border: 1px solid #cccccc;
            border-radius: 0px;
        }

        /* stop the glowing blue shadow */
        .add-on .form-control:focus {
            box-shadow: none;
            -webkit-box-shadow: none;
            border-color: #cccccc;
        }

        rangeslider-wrap {
            padding-top: 100px;
        }

        .rangeslider {
            position: relative;
            height: 4px;
            border-radius: 5px;
            width: 100%;
            background-color: gray;
        }

        .rangeslider__handle {
            transition: background-color .2s;
            box-sizing: border-box;
            width: 20px;
            height: 20px;
            border-radius: 100%;
            background-color: #0099FF;
            touch-action: pan-y;
            cursor: pointer;
            display: inline-block;
            position: absolute;
            z-index: 3;
            top: -8px;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.5), inset 0 0 0 2px white;
        }

        .rangeslider__handle__value {
            transition: background-color .2s, box-shadow .1s, transform .1s;
            box-sizing: border-box;
            width: 90px;
            text-align: center;
            padding: 10px;
            background-color: #0099FF;
            border-radius: 5px;
            color: white;
            left: -35px;
            top: -55px;
            position: absolute;
            white-space: nowrap;
            border-top: 1px solid #007acc;
            box-shadow: 0 -4px 1px rgba(0, 0, 0, 0.07), 0 -5px 20px rgba(0, 0, 0, 0.3);
        }

        .rangeslider__handle__value:before {
            transition: border-top-color .2s;
            position: absolute;
            bottom: -10px;
            left: calc(50% - 10px);
            content: "";
            width: 0;
            height: 0;
            border-left: 10px solid transparent;
            border-right: 10px solid transparent;
            border-top: 10px solid;
            border-top-color: #0099FF;
        }

        .rangeslider__handle__value:after {
            content: " %";
        }

        .rangeslider__fill {
            position: absolute;
            top: 0;
            z-index: 1;
            height: 100%;
            background-color: #0099FF;
            border-radius: 5px;
        }

        .rangeslider__labels {
            position: absolute;
            width: 100%;
            z-index: 2;
            display: flex;
            justify-content: space-between;
        }

        .rangeslider__labels__label {
            font-size: 0.75em;
            position: relative;
            padding-top: 15px;
            color: gray;
        }

        .rangeslider__labels__label:before {
            position: absolute;
            top: 0;
            left: 50%;
            transform: translateX(-50%);
            content: "";
            width: 1px;
            height: 9px;
            border-radius: 1px;
            background-color: rgba(128, 128, 128, 0.5);
        }

        .rangeslider__labels__label:first-child:before, .rangeslider__labels__label:last-child:before {
            height: 12px;
            width: 2px;
        }

        .rangeslider__labels__label:first-child:before {
            background-color: #0099FF;
        }

        .rangeslider__labels__label:last-child:before {
            background-color: gray;
        }

        .rangeslider__labels__label:first-child {
            transform: translateX(-48%);
        }

        .rangeslider__labels__label:last-child {
            transform: translateX(48%);
        }

        .rangeslider.rangeslider--active .rangeslider__handle, .rangeslider.rangeslider--active .rangeslider__handle * {
            background-color: #33adff;
        }

        .rangeslider.rangeslider--active .rangeslider__handle *:before {
            border-top-color: #33adff;
        }

        .rangeslider.rangeslider--active .rangeslider__handle__value {
            transform: translateY(-5px);
            box-shadow: 0 -3px 2px rgba(0, 0, 0, 0.04), 0 -9px 25px rgba(0, 0, 0, 0.15);
        }

        .separator {
            border-top: 1px solid #e4e4e4;
            height: 2px;
        }

    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('reviews.index')}}">Reviews</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ul>
        </div>
    </div>

    {{-- Form --}}
    {!! Form::model($review,['method'=>'PATCH','route'=>['reviews.update',$review->id],'class'=>'form-horizontal']) !!}

    {{-- Inputs --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1 style="margin-bottom: 0px">REVIEW CRUD</h1>
            </header>
            <div class="row">
                {{-- Main Details --}}
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h1>Review Details</h1>
                        </div>
                        <div class="card-block">

                            <div class="form-group row" style="margin: 0">
                                {!! Form::label('score','Score:',['class'=>'col-sm-3']) !!}
                                <div class="col-sm-9">
                                    <select class="form-control" id="materials" name="score">
                                        <option></option>
                                        <option value="0.5" {{ $review->score == 0.5 ? 'selected':''}} >0.5</option>
                                        <option value="1" {{ $review->score == 1 ? 'selected':''}} >1</option>
                                        <option value="1.5" {{ $review->score == 1.5 ? 'selected':''}} >1.5</option>
                                        <option value="2" {{ $review->score == 2 ? 'selected':''}} >2</option>
                                        <option value="2.5" {{ $review->score == 2.5 ? 'selected':''}} >2.5</option>
                                        <option value="3" {{ $review->score == 3 ? 'selected':''}} >3</option>
                                        <option value="3.5" {{ $review->score == 3.5 ? 'selected':''}} >3.5</option>
                                        <option value="4" {{ $review->score == 4 ? 'selected':''}} >4</option>
                                        <option value="4.5" {{ $review->score == 4.5 ? 'selected':''}} >4.5</option>
                                        <option value="5" {{ $review->score == 5 ? 'selected':''}} >5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Body</label>
                                <div class="col-sm-9">
                                    <textarea name="body" placeholder="Body" class="form-control" style="border-radius: 0">{{$review->body}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Visible</label>
                                <div class="col-sm-9">
                                    <input id="toggle-event" name="is_visible" type="checkbox" data-toggle="toggle" data-style="android" {{$review->is_visible == 1 ? 'checked':''}} data-on="&nbsp;<i class='fa fa-eye fa-lg' aria-hidden='true'></i>" data-off="<i class='fa fa-eye-slash fa-lg' aria-hidden='true'></i>&nbsp;" data-width="60" data-height="20" data-size="small" data-onstyle="primary">
                                </div>
                            </div>
                            <div class="form-group row" style="margin-bottom: 0px;margin-right: 0px">
                                <div class="col-sm-12">
                                    <input type="submit" value="Update Review" class="btn btn-outline-primary" style="float: right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {!! Form::close() !!}

@endsection

@section('scripts')

    <!-- Javascript Files -->
    <script src="{{asset('assets/admin/js/bootstrap-toggle.js')}}"></script>
    <script src="{{asset('assets/admin/js/picker.js')}}"></script>
    <script src="{{asset('assets/admin/js/picker.date.js')}}"></script>
    <script src="{{asset('assets/admin/js/vue.js')}}"></script>
    <script src="{{asset('assets/admin/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/dropzone.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/rangeslider.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/sortable.js')}}"></script>
    <script src="{{asset('assets/admin/js/vue-sortable.js')}}"></script>

    <!-- Select Elements -->
    <script>
        $(document).ready(function () {

            $('#sizes').select2({
                placeholder: "  Please select a size",
                allowClear: true

            });

            $('#categories').select2({
                placeholder: " Please select a category",
                allowClear: true

            });

            $('#styles').select2({
                placeholder: " Please select a style",
                allowClear: true

            });

            $('#tags').select2({
                placeholder: " Please select a tag",
                allowClear: true
            });

            $('#materials').select2({
                minimumResultsForSearch: -1,
                placeholder: "Please select a material",
            });

            function formatColorState(state) {
                if (!state.id) {
                    return state.text;
                }
                var $state = $(
                    '<span> <img style="height: 25px;width: 25px; border: none; padding: 0px; background-color:' + state.text + '" class="img-thumbnail"/></span>'
                );
                return $state;
            };


            function formatColorSelectState(state) {
                if (!state.id) {
                    return state.text;
                }
                var $state = $(
                    '<span> <img style="height: 15px;width: 15px; border: none; padding: 0px;margin-bottom: 5px;margin-top: 5px; background-color:' + state.text + '" class="img-thumbnail"/> &nbsp;</span>'
                );
                return $state;
            };


            $('#colors').select2({
                placeholder: "Please select a color",
                templateResult: formatColorState,
                templateSelection: formatColorSelectState,
                allowClear: true
            });

            function formatGemState(state) {
                if (!state.id) {
                    return state.text;
                }
                var $state = $(
                    '<span><img src="' + state.element.title.toLowerCase() + '" style="height: 40px; width: 40px;padding: 0; border-radius: 50px;margin-right: 5px" alt="" class="img-thumbnail" /> ' + state.text + '</span>'
                );
                return $state;
            };

            function formatGemSelectState(state) {
                if (!state.id) {
                    return state.text;
                }
                var $state = $(
                    '<span><img src="' + state.element.title.toLowerCase() + '" style="height: 50px; width: 50px;padding: 0; border-radius: 50px;margin-right: 5px" alt="" class="img-thumbnail" /> ' + '&nbsp; </span>'
                );
                return $state;
            };

            $('#gems').select2({
                placeholder: "  Please select a gem",
                templateResult: formatGemState,
                templateSelection: formatGemSelectState,
                allowClear: true
            });

            $('.select2').on('select2:open', function () {
                $('.select2-selection__choice__remove').addClass('select2-remove-right');
            });
        });
    </script>

    <!-- File Upload Elements -->
    <script>
        var brandPrimary = '#2b90d9';
        $(".pre-scrollable").niceScroll({
            cursorcolor: brandPrimary,
            cursorwidth: '5px',
            cursorborder: 'none',
            scrollspeed: 20,
            mousescrollstep: 60,
            autohidemode: true
        });
    </script>

    <!-- Date -->
    <script>
        var $input = $('.datepicker').pickadate({
            format: 'yyyy-mm-dd',
            formatSubmit: 'yyyy-mm-dd',
            hiddenName: true
        });
        var picker = $input.pickadate('picker');
        $('.calendar').click(function (e) {
            e.stopPropagation()
            e.preventDefault();
            picker.open();
        })
    </script>

    <!-- Range Slider -->
    <script>
        $('input[name=price]').on("input", function () {
            $('#original_price').val($(this).val());
        });

        $('input[type="range"]').rangeslider({
            // Feature detection the default is `true`.
            // Set this to `false` if you want to use
            // the polyfill also in Browsers which support
            // the native <input type="range"> element.
            polyfill: false,

            // Default CSS classes
            rangeClass: 'rangeslider',
            disabledClass: 'rangeslider--disabled',
            horizontalClass: 'rangeslider--horizontal',
            fillClass: 'rangeslider__fill',
            handleClass: 'rangeslider__handle',

            // Callback function
            onInit: function () {
                $rangeEl = this.$range;
                // add value label to handle
                var $handle = $rangeEl.find('.rangeslider__handle');
                var handleValue = '<div class="rangeslider__handle__value">' + this.value + '</div>';
                $handle.append(handleValue);

                // get range index labels
                var rangeLabels = this.$element.attr('labels');
                rangeLabels = rangeLabels.split(', ');

                // add labels
                $rangeEl.append('<div class="rangeslider__labels"></div>');
                $(rangeLabels).each(function (index, value) {
                    $rangeEl.find('.rangeslider__labels').append('<span class="rangeslider__labels__label">' + value + '</span>');
                })
            },

            // Callback function
            onSlide: function (position, value) {
                var $handle = this.$range.find('.rangeslider__handle__value');
                var $price = $('input[name=price]').val();
                var $discount = $price / 100 * value;
                $('#price_discount').val(($price - $discount).toFixed(2));
                $handle.text(this.value);
            },

            // Callback function
            onSlideEnd: function (position, value) {
            }
        });
    </script>

    <!-- Vue App -->
    <script>
        new Vue({
            el: 'body'
        });
    </script>

@endsection



