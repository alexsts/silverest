@extends('layouts.admin')

@section('title',"Manage | Categories")

@section('styles')

    <style>
        .sweet-alert button{
            margin: 26px 5px 15px 5px !important;
        }
    </style>

    <style>
        .counter {
            font-size: 17px;
            padding: 8px 10px;
            margin-bottom: 0;
            min-width: 250px;
            float: right;
            font-weight: initial;
            background: #fff;
            box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.3);
            text-align: center
        }
    </style>

@endsection

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item active">Tags</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="charts">
        <div class="container-fluid">
            <header>
                <h1 style="float: left">TAG CRUD</h1>
                <h2 class="counter">Tag Number :
                    <span style="font-size: 17px;color: #0275d8;">{{\App\Tag::all()->count()}}</span>
                </h2>

            </header>
            <div class="row" style="margin-top: 30px">
                <div class="col-lg-7">
                    <div class="card">
                        <div class="card-header" style="height: auto">
                            <h1 class="float-left">All Tags</h1>
                        </div>
                        <div class="card-block">
                            <table class="table table-lg table-bordered">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Updated</th>
                                    <th style="width: 130px">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tags as $tag)
                                    <tr>
                                        <th>{{$tag->id}}</th>
                                        <td>{{$tag->name}}</td>
                                        <td>{{$tag->updated_at ? $tag->updated_at->diffForHumans():'No Date'}}</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-xs-4" style="margin: 0">
                                                    <a href="{{route('tags.show',$tag->id)}}" style="margin-left: 20px;margin-right: 20px"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="col-xs-4" style="margin: 0">
                                                    <a href="{{route('tags.edit',$tag->id)}}" style="margin-right: 20px"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></a>
                                                </div>
                                                <div class="col-xs-4" style="margin: 0">
                                                    {!! Form::open(['method'=>'DELETE','route'=>['tags.destroy', $tag->id],'class'=>'delete_tag']) !!}
                                                    <button type="submit" style="border: none;background: transparent;padding: 0">
                                                        <i class="fa fa-trash-o fa-lg" aria-hidden="true" style="color: red"></i>
                                                    </button>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5" style="max-height: 400px">
                    <div class="card">
                        {!! Form::open(['method'=>'POST','route'=>'tags.store','class'=>'form-horizontal']) !!}
                        <div class="card-header">
                            <h1>New Tag</h1>
                        </div>
                        <div class="card-block">
                            <div class="form-group row" style="margin: 0;margin-top: 20px">
                                <label class="col-sm-5">Name (ENG)</label>
                                <div class="col-sm-7">
                                    <input name="name_en" type="text" placeholder="enter name..." value="{{old('name_en')}}" class="form-control form-control-success" required>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-5">Name (RUS)</label>
                                <div class="col-sm-7">
                                    <input name="name_ru" type="text" placeholder="enter name..." value="{{old('name_ru')}}" class="form-control form-control-success" required>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-5">Name (EST)</label>
                                <div class="col-sm-7">
                                    <input name="name_et" type="text" placeholder="enter name..." value="{{old('name_et')}}" class="form-control form-control-success" required>
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top: 30px;margin-left: 0px">
                                <div class="col-sm-10">
                                    <input type="submit" value="Add Tag" class="btn btn-outline-primary">
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div style="margin-bottom: 20px">
                {{$tags->links()}}
            </div>
        </div>
    </section>

@endsection

@section('scripts')

    <script>
        $(".delete_tag").submit(function (e) {
            var form = this;
            e.preventDefault();
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this tag!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Yes, I am sure!',
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                            title: 'Tag Deleted!',
                            text: 'Tag is successfully deleted!',
                            type: 'success'
                        }, function () {
                            form.submit();
                        });

                    } else {
                        swal("Cancelled", "Your tag is safe", "error");
                    }
                }
            );
        });
    </script>

@endsection