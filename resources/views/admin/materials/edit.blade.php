@extends('layouts.admin')

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('materials.index')}}">Materials</a></li>
                <li class="breadcrumb-item active">Edit</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1>MATERIAL CRUD</h1>
            </header>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        {!! Form::model($material,['method'=>'PATCH','route'=>['materials.update',$material->id],'class'=>'form-horizontal']) !!}
                        <div class="card-header">
                            <h1>Material Details</h1>
                        </div>

                        <div class="card-block">
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Name (ENG)</label>
                                <div class="col-sm-9">
                                    <input name="name_en" type="text" placeholder="enter name..." value="{{$material->getTranslation('name','en')}}" class="form-control form-control-success" required>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Name (RUS)</label>
                                <div class="col-sm-9">
                                    <input name="name_ru" type="text" placeholder="enter name..." value="{{$material->getTranslation('name','ru')}}" class="form-control form-control-success" required>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Name (EST)</label>
                                <div class="col-sm-9">
                                    <input name="name_et" type="text" placeholder="enter name..." value="{{$material->getTranslation('name','et')}}" class="form-control form-control-success" required>
                                </div>
                            </div>
                            <div class="form-group row" style="margin-top: 30px;margin-left: 0px">
                                <div class="col-sm-10">
                                    <input type="submit" value="Update Material" class="btn btn-outline-primary">
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
