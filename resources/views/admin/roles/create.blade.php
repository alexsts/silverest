@extends('layouts.admin')

@section('content')

    {{-- Breadcrumb Section --}}
    <div class="breadcrumb-holder">
        <div class="container-fluid">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Roles</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ul>
        </div>
    </div>

    {{-- Tables Section --}}
    <section class="forms">
        <div class="container-fluid">
            <header>
                <h1>ROLE CRUD</h1>
            </header>
            <div class="row">
                {!! Form::open(['method'=>'POST','route'=>'roles.store','class'=>'form-horizontal']) !!}
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h1>Edit Role</h1>
                        </div>
                        <div class="card-block">
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Name</label>
                                <div class="col-sm-9">
                                    <input name="display_name" type="text" placeholder="Name" value="{{old('display_name')}}" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Slug</label>
                                <div class="col-sm-9">
                                    <input name="name" type="text" placeholder="Slug" value="{{old('name')}}" class="form-control form-control-success">
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 0">
                                <label class="col-sm-3">Description</label>
                                <div class="col-sm-9">
                                    <input name="description" type="text" placeholder="Description" value="{{old('description')}}" class="form-control form-control-success">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <h1>Permissions Assigned:</h1>
                        </div>
                        <div class="card-block">
                            <div class="form-group row" style="margin: 0">
                                <div class="i-checks col-sm-5">
                                    @foreach($permissions as $permission)
                                        {!! Form::checkbox('permissions[]', $permission->id,false,['class' => 'form-control-custom','id'=>$permission->id]) !!}
                                        <label for="{{$permission->id}}" style="font-size: 16px">{{$permission->display_name}}<em>({{$permission->description}})</em></label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row" style="margin-top: 30px;margin-left: 0px">
                    <div class="col-sm-10">
                        <input type="submit" value="Create Role" class="btn btn-outline-primary">
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>

@endsection
