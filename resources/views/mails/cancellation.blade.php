<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Cancellation Email</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">

        * {
            margin: 0;
            padding: 0;
            font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
            box-sizing: border-box;
            font-size: 14px;
        }

        img {
            max-width: 100%;
        }

        body {
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            width: 100% !important;
            height: 100%;
            line-height: 1.6;
        }

        table td {
            vertical-align: top;
        }

        /* -------------------------------------
            BODY & CONTAINER
        ------------------------------------- */
        body {
            background-color: #f6f6f6;
        }

        .body-wrap {
            background-color: #f6f6f6;
            width: 100%;
        }

        .container {
            display: block !important;
            max-width: 600px !important;
            margin: 0 auto !important;
            /* makes it centered */
            clear: both !important;
        }

        .content {
            max-width: 600px;
            margin: 0 auto;
            display: block;
            padding: 20px;
        }

        /* -------------------------------------
            HEADER, FOOTER, MAIN
        ------------------------------------- */
        .main {
            background: #fff;
            border: 1px solid #e9e9e9;
            border-radius: 3px;
        }

        .content-wrap {
            padding: 20px;
        }

        .content-block {
            padding: 0 0 20px;
        }

        .header {
            width: 100%;
            margin-bottom: 20px;
        }

        .footer {
            width: 100%;
            clear: both;
            color: #999;
            padding: 20px;
        }

        .footer a {
            color: #999;
        }

        .footer p, .footer a, .footer unsubscribe, .footer td {
            font-size: 12px;
        }

        /* -------------------------------------
            TYPOGRAPHY
        ------------------------------------- */
        h1, h2, h3 {
            font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
            color: #1a237e;
            margin: 40px 0 0;
            line-height: 1.2;
            font-weight: 400;
        }

        h1 {
            font-size: 32px;
            font-weight: 500;
        }

        h2 {
            font-size: 24px;
        }

        h3 {
            font-size: 18px;
        }

        h4 {
            font-size: 14px;
            font-weight: 600;
        }

        p, ul, ol {
            margin-bottom: 10px;
            font-weight: normal;
        }

        p li, ul li, ol li {
            margin-left: 5px;
            list-style-position: inside;
        }

        /* -------------------------------------
            LINKS & BUTTONS
        ------------------------------------- */
        a {
            color: #1ab394;
            text-decoration: underline;
        }

        .btn-primary {
            text-decoration: none;
            color: #FFF;
            background-color: #1ab394;
            border: solid #1ab394;
            border-width: 5px 10px;
            line-height: 2;
            font-weight: bold;
            text-align: center;
            cursor: pointer;
            display: inline-block;
            border-radius: 5px;
            text-transform: capitalize;
        }

        /* -------------------------------------
            OTHER STYLES THAT MIGHT BE USEFUL
        ------------------------------------- */
        .last {
            margin-bottom: 0;
        }

        .first {
            margin-top: 0;
        }

        .aligncenter {
            text-align: center;
        }

        .alignright {
            text-align: right;
        }

        .alignleft {
            text-align: left;
        }

        .clear {
            clear: both;
        }

        /* -------------------------------------
            ALERTS
            Change the class depending on warning email, good email or bad email
        ------------------------------------- */
        .alert {
            font-size: 16px;
            color: #fff;
            font-weight: 500;
            padding: 20px;
            text-align: center;
            border-radius: 3px 3px 0 0;
        }

        .order {
            margin-bottom: 20px;
            margin-top: 20px;
            font-size: 14px;
        }

        .alert a {
            color: #fff;
            text-decoration: none;
            font-weight: 500;
            font-size: 16px;
        }

        .alert.alert-warning {
            background: #f8ac59;
        }

        .alert.alert-bad {
            background: #ed5565;
        }

        .alert.alert-good {
            background: #1ab394;
        }

        /* -------------------------------------
            INVOICE
            Styles for the billing table
        ------------------------------------- */
        .invoice {
            margin: 5px auto;
            text-align: left;
            width: 95%;
        }

        .invoice td {
            padding: 5px 0;
        }

        .invoice .invoice-items {
            width: 100%;
        }

        .img-logo {
            width: 150px !important;
        }

        .img-block {
            text-align: center;
            padding: 0px !important;
        }

        .dear {
            margin: 0px !important;
        }

        .invoice .invoice-items td {
            border-top: #eee 1px solid;
        }

        .invoice .invoice-items .total td {
            border-top: 2px solid #1a237e;
            font-weight: 700;
            color: #1a237e;
        }

        .filed {
            color: #5C5959;
        }

        .total-bottom td {
            border-bottom: 2px solid #1a237e;
        }

        .thank-you {
            font-size: 16px;
        }

        h6 {
            color: #1a237e;
        }

        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 640px) {
            h1, h2, h3, h4 {
                font-weight: 600 !important;
                margin: 20px 0 5px !important;
            }

            h1 {
                font-size: 22px !important;
            }

            h2 {
                font-size: 18px !important;
            }

            h3 {
                font-size: 16px !important;
            }

            .container {
                width: 100% !important;
            }

            .content, .content-wrap {
                padding: 5px !important;
            }

            .invoice {
                width: 100% !important;
            }
        }

    </style>
</head>
<body>
<table class="body-wrap">
    <tbody>
    <tr>
        <td></td>
        <td class="container" width="600">
            <div class="content">
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td class="content-wrap aligncenter">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tbody>
                                <tr>
                                    <table class="invoice">
                                        <tbody>
                                        <td class="content-block img-block">
                                            <img class="aligncenter img-logo" src="https://www.silverest.ee/img/banner/logo_silverest.png">
                                        </td>
                                        </tbody>
                                    </table>
                                </tr>
                                <tr>
                                    <table class="invoice">
                                        <tbody>
                                        <td class="content-block">
                                            <h3 class="dear">{{trans('mails/invoice.dear')}} {{$order->user->first_name}}</h3>
                                        </td>
                                        </tbody>
                                    </table>
                                </tr>
                                <tr>
                                    <table class="invoice">
                                        <tbody>
                                        <tr>
                                            <td class="content-block alignleft">
                                                {{trans('mails/invoice.we_are_sorry')}}: {{$order->id}}
                                                <br><br>
                                                {{trans('mails/invoice.please_take_this_email')}}<br>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        <table class="invoice">
                                            <tbody>

                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td class="order alignleft" width="40%">
                                                                <h6>
                                                                    {{trans('mails/invoice.order')}}:
                                                                </h6>
                                                            </td>
                                                            <td>
                                                                <span class="filed">{{$order->id}}</span><br>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="order alignleft" width="40%">
                                                                <h6>
                                                                    {{trans('mails/invoice.placed')}}:
                                                                </h6>
                                                            </td>
                                                            <td>
                                                                <span class="filed">{{date('j F Y', strtotime($order->created_at))}}</span><br>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="order alignleft" width="40%">
                                                                <h6>
                                                                    {{trans('mails/invoice.deliver_by')}}:
                                                                </h6>
                                                            </td>
                                                            <td>
                                                                <span class="filed">{{ucwords($order->delivery->method)}}</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="order alignleft" width="40%">
                                                                <h6>
                                                                    {{trans('mails/invoice.deliver_to')}}:
                                                                </h6>
                                                            </td>
                                                            <td>
                                                                <span class="filed">{{$order->delivery->customer_address}}, {{trans('mails/invoice.phone')}}: {{$order->delivery->customer_phone}}</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table class="invoice-items" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                        @foreach($order->items as $item)
                                                            @php
                                                                $product = \App\Product::withTrashed()->find($item->product_id);
                                                            @endphp
                                                            @if($product)
                                                                <tr>
                                                                    <td>{{$product->name}} ({{$product->code}})<br>
                                                                        @if($item->size)
                                                                            <span>{{trans('mails/invoice.size')}}
                                                                                <strong>: &nbsp;</strong></span>{{$item->size}}
                                                                            <br>
                                                                        @endif
                                                                        <span>{{trans('mails/invoice.q_ty')}}
                                                                            <strong>: &nbsp;</strong></span>{{$item->quantity}}
                                                                        <br>
                                                                    </td>
                                                                    <td class="alignright">
                                                                        @if($item->discount_price > 0)
                                                                            <span class="new-price">{{number_format ($item->discount_price * $item->quantity,2)}}&euro;</span>
                                                                        @else
                                                                            <span class="new-price">{{number_format ($product->price * $item->quantity,2)}}&euro;</span>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                        <tr class="total">
                                                            <td width="80%">{{trans('mails/invoice.cart_total')}}</td>
                                                            <td class="alignright"> {{number_format(($order->payment->cart_total),2)}}&euro;</td>
                                                        </tr>
                                                        <tr class="total">
                                                            <td width="80%">{{trans('mails/invoice.shipping_total')}}</td>
                                                            <td class="alignright"> {{number_format(($order->payment->shipping_total),2)}}&euro;</td>
                                                        </tr>
                                                        <tr class="total total-bottom">
                                                            <td width="80%">{{trans('mails/invoice.total')}}</td>
                                                            <td class="alignright"> {{number_format(($order->payment->subtotal),2)}}&euro;</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <table class="invoice">
                                        <tbody>
                                        <tr>
                                            <td class="content-block alignleft">
                                                {{trans('mails/invoice.should_any_questions')}}<br><br>
                                                {{trans('mails/invoice.thank_you_for_choosing')}}
                                                <br><br>
                                                {{trans('mails/invoice.kind_regards')}}<br>
                                                {{trans('mails/invoice.customer_team')}}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </tr>
                                <tr>
                                    <table class="invoice">
                                        <tbody>
                                        <tr>
                                            <td class="content-block alignleft">
                                                OÜ "SILVEREST"<br>
                                                {{trans('mails/invoice.phone')}} : 59020001<br>
                                                Fax: (35) 62620<br>
                                                Email:
                                                <a href="mailto:silverest@silverest.ee?Subject=Order:{{$order->id}}" target="_blank">silverest@silverest.ee</a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="footer">
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td class="aligncenter content-block">
                                {{trans('mails/invoice.trading_name')}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
        <td></td>
    </tr>
    </tbody>
</table>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html>
