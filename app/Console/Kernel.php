<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        Commands\SyncOrdersCommand::class,
        Commands\RemoveLogsCommand::class
    ];

    protected function schedule(Schedule $schedule)
    {
        $file = 'order_braintree.log';
        $schedule->command('sync:orders')->hourly()->sendOutputTo($file);
        $schedule->command('remove:logs')->monthly()->sendOutputTo($file);
    }

    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
