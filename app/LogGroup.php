<?php

namespace App;

use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class LogGroup extends Model
{
    protected $fillable = [
        'name'
    ];

    public function logs(){
        return $this->hasMany('App\Log');
    }
}
