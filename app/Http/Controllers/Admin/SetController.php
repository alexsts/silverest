<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Set;
use Session;
use DB;

class SetController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator|administrator');
    }

    public function index()
    {
        $sets = Set::orderBy('created_at', 'asc')->paginate(10);
        return view('admin.sets.index')->withSets($sets);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255|unique:sets,name',
        ]);

        $set = new Set();
        $set->name = $request->name;

        if ($set->save()) {
            //show notification:
            $message = 'Set added : ' .  $set->name;
            session()->flash('notification', $message);
            return redirect()->route('sets.index', $set->id);
        } else {
            Session::flash('notification', 'Error');
            return redirect()->route('sets.index');
        }
    }

    public function show($id)
    {
        $set = Set::where('id', $id)->with('products')->first();
        return view("admin.sets.show")->withSet($set);
    }

    public function edit($id)
    {
        $products = Product::orderBy('created_at', 'desc')->get();
        $set = Set::where('id', $id)->first();
        return view("admin.sets.edit", compact('products', 'set'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $set = Set::findOrFail($id);
        $set->name = $request->name;

        $products = Input::get('products');

        if ($set->save()) {
            $set->products()->sync($products);

            //show notification:
            $message = 'Set updated : ' .  $set->name;
            session()->flash('notification', $message);

            return redirect()->route('sets.index');

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('sets.index');
        }
    }

    public function destroy($id)
    {
        $set = Set::findOrFail($id);
        $set->delete();

        //show notification:
        $message = 'Set deleted : ' .  $set->name;
        session()->flash('notification', $message);

        return redirect()->route('sets.index');
    }
}
