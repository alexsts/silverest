<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Material;
use Session;
use DB;

class MaterialController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator|administrator');
    }

    public function index()
    {
        $materials = Material::orderBy('id','asc')->paginate(10);
        return view('admin.materials.index')->withMaterials($materials);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_en' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'name_et' => 'required|max:255'
        ]);

        $material = new Material();
        $material->setTranslation('name', 'en', $request->name_en);
        $material->setTranslation('name', 'ru', $request->name_ru);
        $material->setTranslation('name', 'et', $request->name_et);

        if ($material->save()) {

            //show notification:
            $message = 'Material added : ' .  $material->name;
            session()->flash('notification', $message);

            return redirect()->route('materials.index', $material->id);

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('materials.index');
        }
    }

    public function show($id)
    {
        $material = Material::where('id',$id)->with('products')->first();
        return view("admin.materials.show")->withMaterial($material);
    }

    public function edit($id)
    {
        $material = Material::where('id',$id)->first();
        return view("admin.materials.edit")->withMaterial($material);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name_en' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'name_et' => 'required|max:255'
        ]);

        $material = Material::findOrFail($id);
        $material->setTranslation('name', 'en', $request->name_en);
        $material->setTranslation('name', 'ru', $request->name_ru);
        $material->setTranslation('name', 'et', $request->name_et);

        if ($material->save()) {

            //show notification:
            $message = 'Material updated : ' .  $material->name;
            session()->flash('notification', $message);

            return redirect()->route('materials.index', $material->id);

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('materials.index');
        }
    }

    public function destroy($id)
    {
        $material = Material::findOrFail($id);
        $material->delete();

        //show notification:
        $message = 'Material deleted : ' .  $material->name;
        session()->flash('notification', $message);

        return redirect()->route('materials.index');
    }
}
