<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Permission;
use Session;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator');
    }


    public function index()
    {
        $permissions = Permission::orderBy('id', 'asc')->paginate(10);
        return view('admin.permissions.index')->withPermissions($permissions);
    }

    public function create()
    {
        return view('admin.permissions.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'display_name' => 'required|max:255',
            'name' => 'required|max:255|alphadash|unique:permissions,name',
            'description' => 'sometimes|max:255'
        ]);

        $permission = new Permission();
        $permission->name = $request->name;
        $permission->display_name = $request->display_name;
        $permission->description = $request->description;
        $permission->save();

        //show notification:
        $message = 'Permission added : ' .  $permission->display_name;
        session()->flash('notification', $message);

        return redirect()->route('permissions.index');
    }

    public function show($id)
    {
        $permission = Permission::findOrFail($id);
        return view('admin.permissions.show')->withPermission($permission);
    }

    public function edit($id)
    {
        $permission = Permission::findOrFail($id);
        return view('admin.permissions.edit')->withPermission($permission);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'display_name' => 'required|max:255',
            'description' => 'sometimes|max:255'
        ]);
        $permission = Permission::findOrFail($id);
        $permission->display_name = $request->display_name;
        $permission->description = $request->description;
        $permission->save();

        //show notification:
        $message = 'Permission updated : ' .  $permission->display_name;
        session()->flash('notification', $message);

        return redirect()->route('permissions.show', $id);
    }

    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);
        $permission->delete();

        //show notification:
        $message = 'Permission deleted : ' .  $permission->display_name;
        session()->flash('notification', $message);

        return redirect()->route('permissions.index');
    }
}
