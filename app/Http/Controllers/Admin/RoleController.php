<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Permission;
use App\Role;
use Session;

class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:superadministrator');
    }

    public function index()
    {
        $roles = Role::orderBy('id', 'asc')->paginate(10);
        return view('admin.roles.index')->withRoles($roles);
    }

    public function create()
    {
        $permissions = Permission::all();
        return view('admin.roles.create')->withPermissions($permissions);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'display_name' => 'required|max:255',
            'name' => 'required|max:100|alpha_dash|unique:permissions,name',
            'description' => 'sometimes|max:255'
        ]);

        $role = new Role();
        $role->display_name = $request->display_name;
        $role->name = $request->name;
        $role->description = $request->description;
        $role->save();

        $permissions = Input::get('permissions');
        $role->syncPermissions($permissions);

        //show notification:
        $message = 'Role added : ' .  $role->name;
        session()->flash('notification', $message);

        return redirect()->route('roles.show', $role->id);
    }

    public function show($id)
    {
        $role = Role::where('id', $id)->with('permissions')->first();
        return view('admin.roles.show')->withRole($role);
    }

    public function edit($id)
    {
        $role = Role::where('id', $id)->with('permissions')->first();
        $permissions = Permission::all();
        return view('admin.roles.edit')->withRole($role)->withPermissions($permissions);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'display_name' => 'required|max:255',
            'description' => 'sometimes|max:255'
        ]);

        $role = Role::findOrFail($id);
        $role->display_name = $request->display_name;
        $role->description = $request->description;
        $role->save();

        $permissions = Input::get('permissions');
        $role->syncPermissions($permissions);

        //show notification:
        $message = 'Role updated : ' .  $role->name;
        session()->flash('notification', $message);

        return redirect()->route('roles.show', $id);
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        //show notification:
        $message = 'Role deleted : ' .  $role->name;
        session()->flash('notification', $message);

        return redirect()->route('roles.index');
    }
}
