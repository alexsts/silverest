<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Role;
use App\User;
use Session;
use Alert;
use DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator');
    }

    public function index()
    {
        $users = User::orderBy('id', 'asc')->paginate(10);
        return view('admin.users.index')->withUsers($users);
    }

    public function create()
    {
        $roles = Role::all();
        return view('admin.users.create')->withRoles($roles);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|unique:users'
        ]);

        if (!empty($request->password)) {
            $password = trim($request->password);
        } else {
            # set the manual password
            $length = 10;
            $keyspace = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
            $str = '';
            $max = mb_strlen($keyspace, '8bit') - 1;
            for ($i = 0; $i < $length; ++$i) {
                $str .= $keyspace[random_int(0, $max)];
            }
            $password = $str;
        }

        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = bcrypt($password);

        if ($user->save()) {

            $roles = Input::get('roles');
            $user->syncRoles($roles);

            //show notification:
            $message = 'User added : ' .  $user->email;
            session()->flash('notification', $message);

            return redirect()->route('users.show', $user->id);

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('users.create');
        }
    }

    public function show($id)
    {
        $user = User::where('id', $id)->with('roles')->first();
        return view("admin.users.show")->withUser($user);
    }

    public function edit($id)
    {
        $roles = Role::all();
        $user = User::where('id', $id)->with('roles')->first();
        return view("admin.users.edit")->withUser($user)->withRoles($roles);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|unique:users,email,' . $id
        ]);

        $user = User::findOrFail($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        if ($request->password_options == 'auto') {
            $length = 10;
            $keyspace = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
            $str = '';
            $max = mb_strlen($keyspace, '8bit') - 1;
            for ($i = 0; $i < $length; ++$i) {
                $str .= $keyspace[random_int(0, $max)];
            }
            $user->password = bcrypt($str);
        } elseif ($request->password_options == 'manual') {
            $user->password = bcrypt($request->password);
        }

        if ($user->save()) {

            $roles = Input::get('roles');
            $user->syncRoles($roles);

            //show notification:
            $message = 'User updated : ' .  $user->email;
            session()->flash('notification', $message);

            return redirect()->route('users.show', $id);

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('users.edit', $id);
        }
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);

        //show notification:
        $message = 'User deleted : ' .  $user->email;
        session()->flash('notification', $message);

        $user->delete();
        return redirect()->route('users.index');
    }
}
