<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\Cancellation;
use Braintree_Transaction;
use Braintree_ClientToken;
use App\Mail\Dispatched;
use App\Subcategory;
use App\Category;
use App\Material;
use App\Product;
use App\Status;
use App\Order;
use App\Photo;
use App\Color;
use App\Style;
use App\Size;
use App\User;
use App\Gem;
use App\Tag;
use Session;
use DB;

class OrderController extends Controller
{
    public function index()
    {
        $orders = new Order();
        $statuses = Status::all();
        $queries = [];
        $search_field = '';

        //search by:
        if (request()->has('search')) {
            $search_field = request('search');
            $orders = $orders->search($search_field);
            $queries['search'] = $search_field;
        }

        //sort by:
        if (request()->has('sort')) {
            $orders = $orders->orderBy('id', request('sort'));
            $queries['sort'] = request('sort');
        } else {
            $orders = $orders->orderBy('id', 'desc');
        }

        $orders = $orders->with('items')->paginate(20)->appends($queries);
        return view('admin.orders.index', compact('orders', 'search_field', 'statuses'));

    }

    public function edit($id)
    {
        $order = Order::findOrFail($id);
        return view('admin.orders.edit', compact('order'));
    }

    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->notes = $request->notes;
        $order->save();
        $order->delivery->tracking_number = $request->tracking_number;
        $order->delivery->customer_name = $request->customer_name;
        $order->delivery->customer_address = $request->customer_address;
        $order->delivery->customer_email = $request->customer_email;
        $order->delivery->customer_phone = $request->customer_phone;
        $order->delivery->save();

        $message = 'Order updated : ' . $order->id;
        session()->flash('notification', $message);
        return redirect()->route('orders.index');
    }

    public function updateTrackingNumber(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        $order->delivery->tracking_number = $request->tracking_number;
        $order->delivery->save();

        $message = 'Order updated : ' . $order->id;
        session()->flash('notification', $message);
        return redirect()->route('orders.index');
    }

    public function orderViewed(Request $request)
    {
        $order = Order::findOrFail($request->order_id);

        if ($order->is_viewed != true) {
            $order->is_viewed = true;
            $order->save();
            return response('Order ' . $order->id . ' Viewed', 200); //return success
        } else {
            return response('Order already viewed', 200); //return success
        }

    }

    public function changeOrderStatus(Request $request)
    {
        $status = Status::find($request->status_id);
        $order = Order::find($request->order_id);
        $user = User::find($order->user_id);

        // update order status:
        if ($status && $order) {

            $order->status_id = $status->id;
            $order->save();

            switch ($status->id) {

                case config('constants.STATUS.PREPARATION'):
                    // only quote:
                    $payment = $order->payment;
                    $payment->is_settled = true;
                    $payment->save();
                    $message = 'Order: #' . $order->id . ' Preparation';
                    break;

                case config('constants.STATUS.REFUNDED'):
                    // only quote:
                    $message = 'Order: #' . $order->id . ' Refunded';
                    break;

                case config('constants.STATUS.PAYMENT_DECLINED'):
                    // only quote:
                    $message = 'Order: #' . $order->id . ' Payment Declined';
                    break;

                case config('constants.STATUS.DISPATCHED'):
                    // quote and braintree:
                    $delivery = $order->delivery;
                    $delivery->is_dispatched = true;
                    $delivery->save();
                    Mail::to($user->email)->send(new Dispatched($user, $order));
                    $message = 'Order: #' . $order->id . ' Dispatched';
                    break;

                case config('constants.STATUS.CANCELLED'):
                    // quote and braintree:
                    Mail::to($user->email)->send(new Cancellation($user, $order));
                    $message = 'Order: #' . $order->id . ' Cancelled';
                    break;

                case config('constants.STATUS.COMPLETED'):
                    // quote and braintree:
                    $this->finishOrder($order);
                    $message = 'Order: #' . $order->id . ' Completed';
                    break;
            }
        }

        // flash message:
        return session()->flash('notification', $message);
    }

    private function finishOrder($order)
    {
        foreach ($order->items as $item) {
            $product = Product::find($item->product_id);
            $product->sold_number = $product->sold_number + $item->quantity;
            $product->save();
        }
    }
}
