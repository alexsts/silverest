<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Subcategory;
use App\Category;
use App\Role;
use App\User;
use Session;
use DB;

class SubcategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator|administrator');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_en' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'name_et' => 'required|max:255',
            'code' => 'required|max:255'
        ]);

        $subcategory = new Subcategory();
        $subcategory->category_id = $request->category_id;
        $subcategory->setTranslation('name', 'en', $request->name_en);
        $subcategory->setTranslation('name', 'ru', $request->name_ru);
        $subcategory->setTranslation('name', 'et', $request->name_et);
        $subcategory->code = $request->code;

        if ($subcategory->save()) {

            //show notification:
            $message = 'Subcategory added : ' .  $subcategory->name;
            session()->flash('notification', $message);

            return redirect()->route('categories.show', $subcategory->category->id);

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('categories.index');
        }
    }

    public function edit($id)
    {
        $subcategory = Subcategory::where('id',$id)->first();
        return view("admin.categories.edit_subcategory")->withSubcategory($subcategory);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name_en' => 'required|max:255',
            'name_ru' => 'required|max:255',
            'name_et' => 'required|max:255',
            'code' => 'required|max:255'
        ]);

        $subcategory = Subcategory::findOrFail($id);
        $subcategory->setTranslation('name', 'en', $request->name_en);
        $subcategory->setTranslation('name', 'ru', $request->name_ru);
        $subcategory->setTranslation('name', 'et', $request->name_et);
        $subcategory->code = $request->code;

        if ($subcategory->save()) {

            //show notification:
            $message = 'Subcategory updated : ' .  $subcategory->name;
            session()->flash('notification', $message);

            return redirect()->route('categories.show', $subcategory->category->id);

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('categories.index');
        }
    }

    public function destroy($id)
    {
        $subcategory = Subcategory::findOrFail($id);
        $subcategory->delete();

        //show notification:
        $message = 'Subcategory deleted : ' .  $subcategory->name;
        session()->flash('notification', $message);

        return redirect()->route('categories.show',$subcategory->category->id);
    }
}
