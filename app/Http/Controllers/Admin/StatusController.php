<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Color;
use App\Status;
use Session;
use DB;

class StatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:superadministrator|administrator');
    }

    public function index()
    {
        $statuses = Status::orderBy('id','asc')->paginate(10);
        return view('admin.statuses.index')->withStatuses($statuses);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'value' => 'required|max:255|unique:statuses,value',
            'name' => 'required|max:255|unique:statuses,name'
        ]);

        $status = new Status();
        $status->value = $request->value;
        $status->name = $request->name;

        if ($status->save()) {

            //show notification:
            $message = 'Status added : ' .  $status->name;
            session()->flash('notification', $message);

            return redirect()->route('statuses.index');

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('statuses.index');
        }
    }

    public function show($id)
    {
        $status = Status::where('id',$id)->with('orders')->first();
        return view("admin.statuses.show")->withStatus($status);
    }

    public function edit($id)
    {
        $status = Status::where('id',$id)->first();
        return view("admin.statuses.edit")->withStatus($status);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'value' => 'required|max:255',
            'name' => 'required|max:255'
        ]);

        $status = Status::findOrFail($id);
        $status->value = $request->value;
        $status->name = $request->name;

        if ($status->save()) {

            //show notification:
            $message = 'Status updated : ' .  $status->name;
            session()->flash('notification', $message);

            return redirect()->route('statuses.index');

        } else {

            Session::flash('notification', 'Error');
            return redirect()->route('statuses.index');
        }
    }

    public function destroy($id)
    {
        $status = Status::findOrFail($id);
        $status->delete();

        //show notification:
        $message = 'Status deleted : ' .  $status->name;
        session()->flash('notification', $message);

        return redirect()->route('statuses.index');
    }
}
