<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Product;

class CartController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $cart_subtotal = 0;
        $cart_amount = 0;
        $cart_products = $user->carts()->get()->keyBy('id');

        foreach ($cart_products as $cart_product) {

            // Check if product is in stock and visible:
            if ($cart_product->quantity < $cart_product->pivot->quantity || !$cart_product->is_visible) {
                Auth::user()->carts()->detach($cart_product);
            } else {
                $cart_subtotal += (($cart_product->getDiscountPrice()) ? $cart_product->getDiscountPrice() : $cart_product->price) * $cart_product->pivot->quantity;
                $cart_amount += $cart_product->pivot->quantity;
            }
        }

        $cart_products = $user->carts()->paginate(10);
        return view('front.account.cart', compact('user', 'cart_products', 'cart_amount', 'cart_subtotal'));
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'size' => 'numbers|nullable',
            'quantity' => 'integer|min:1',
            'product_id' => 'integer|min:1',
        ]);

        $product = Product::findOrFail($request->product_id);

        if ($product->sizes()->count() > 0 && $request->size || $product->sizes()->count() == 0 && !$request->size) {
            $size = $request->size ? $request->size : 0;
            $quantity = $request->quantity;
            $discount_price = ($product->getDiscountPrice()) ? $product->getDiscountPrice() : 0;
            $carts = Auth::user()->carts();

            if (!($quantity > $product->quantity)) {

                // Remove duplicate:
                if (Auth::user()->carts->contains($product->id)) {
                    $carts->detach($product->id);
                }
                $carts->attach($product, ['size' => $size, 'quantity' => $quantity, 'discount_price' => $discount_price]);
                return redirect()->route('account.cart');
            }else{
                $message = 'Quantity Exceeded';
            }
        } else {
            $message = 'Size not Selected';
        }
        session()->flash('notification', $message);
        return redirect()->back();
    }

    public function remove(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'integer|min:1'
        ]);

        $product = Product::findOrFail($request->product_id);
        Auth::user()->carts()->detach($product);

        $message = 'Cart Updated';
        session()->flash('notification', $message);
        return redirect()->back();
    }

    public function ajaxQuantity(Request $request)
    {
        if ($request->ajax() && Auth::check()) {

            $this->validate($request, [
                'product_id' => 'numbers',
                'quantity' => 'numbers',
            ]);

            $product = Product::findOrFail($request->product_id);
            $quantity = $request->quantity;
            $old_size = Auth::user()->carts()->where('product_id', $product->id)->first()->pivot->size;
            $discount_price = ($product->getDiscountPrice()) ? $product->getDiscountPrice() : 0;
            $carts = Auth::user()->carts();

            // Remove old, replace by new state:
            if (Auth::user()->carts->contains($product->id) && !($quantity > $product->quantity)) {
                $carts->detach($product->id);

                // Attach new state:
                if ($quantity > 0) {
                    $carts->attach($product, ['size' => $old_size, 'quantity' => $quantity, 'discount_price' => $discount_price]);
                }

                $message = 'Cart Updated';

            } else {
                $message = 'Quantity Exceeded';
            }

            session()->flash('notification', $message);

        } else {
            return 'error';
        }
    }
}
