<?php

namespace App\Http\Controllers\Front;

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;

class FrontController extends Controller
{
    private $products_per_page = 15;

    public function index()
    {
        $new_products = Product::where([['is_visible', '=', '1']])->orderBy('created_at', 'desc')->limit(12)->get();
        $discount_offer_product = Product::where([['is_visible', '=', '1'], ['discount_end_date', '>=', date('Y-m-d')]])->orderBy('discount_rate', 'desc')->first();
        $featured_products = Product::where([['is_visible', '=', '1'], ['is_featured', '=', '1']])->orderBy('created_at', 'desc')->limit(4)->get();

        return view('front.home', compact('new_products', 'discount_offer_product', 'featured_products'));
    }

    public function warranty()
    {
        return view('front.info.warranty');
    }

    public function delivery()
    {
        return view('front.info.delivery');
    }

    public function about_us()
    {
        return view('front.info.about_us');
    }

    public function contacts()
    {
        return view('front.info.contacts');
    }

    public function show404()
    {
        return view('errors.404');
    }
}
