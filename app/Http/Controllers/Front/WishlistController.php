<?php

namespace App\Http\Controllers\Front;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;

class WishlistController extends Controller
{
    public function index()
    {
        $wishlist_products = Auth::user()->wishlists()->where('is_visible', '1')->paginate(10);
        return view('front.account.wishlist', compact('wishlist_products'));
    }

    public function change(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'integer|min:1'
        ]);

        $product = Product::findOrFail($request->product_id);
        $wishlists = Auth::user()->wishlists();
        if (Auth::user()->wishlists->contains($product->id)) {
            $wishlists->detach($product->id);
        } else {
            $wishlists->attach($product->id);
        }
        $message = 'Wishlist Updated';
        session()->flash('notification', $message);
        return redirect()->back();
    }
}
