<?php

namespace App\Mail;

use App\Order;
use App\User;
use PDF;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Invoice extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order = $this->order;
        $user = $this->user;
        $pdf = PDF::loadView('pdfs.invoice', compact('order','user'));

        return $this->view('mails.invoice')->with([
            'user' => $user,
            'order' => $order,
        ])->attachData($pdf->output(), 'invoice_'.$order->id.'.pdf', [
            'mime' => 'application/pdf',
        ]);
    }
}
