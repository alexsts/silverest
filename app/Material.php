<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Material extends Model
{
    use HasTranslations;

    public $translatable = ['name'];

    protected $fillable = [
        'name'
    ];

    public function products(){
        return $this->hasMany('App\Product');
    }

}
