<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
        'name', 'path'
    ];

    public $prefix = '/photos/';
    public $preview_prefix = '/preview/';

    public function getPreviewPath()
    {
        if ($this->path) {
            return $this->preview_prefix . $this->path;
        } else {
            return null;
        }
    }

    public function getPhotoPath()
    {
        if ($this->path) {
            return $this->prefix . $this->path;
        } else {
            return null;
        }
    }

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function scopeSearch($query, $str)
    {
        return $query->where('name', 'like', '%' . $str . '%');
    }
}
