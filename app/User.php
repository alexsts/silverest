<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable;
    use LaratrustUserTrait; // enable laratrust for user

    protected $fillable = [
        'last_name', 'first_name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function reviews(){
        return $this->hasMany('App\Review');
    }

    public function questions(){
        return $this->hasMany('App\Question');
    }

    public function address(){
        return $this->hasOne('App\Address');
    }

    public function orders(){
        return $this->hasMany('App\Order');
    }

    public function wishlists(){
        return $this->belongsToMany('App\Product','wishlists')->withTimestamps();
    }

    public function carts(){
        return $this->belongsToMany('App\Product','carts')->withPivot('quantity','size','discount_price')->withTimestamps();
    }

    public function recent_products(){
        return $this->belongsToMany('App\Product','recent_products');
    }

}
