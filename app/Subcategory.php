<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Subcategory extends Model
{
    use HasTranslations;

    protected $fillable = [
        'name'
    ];

    public $translatable = ['name'];

    public function products() {
        return $this->belongsToMany('App\Product');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }

}
