<?php

namespace App;

use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Style extends Model
{
    use HasTranslations;

    public $translatable = ['name'];

    protected $fillable = [
        'name'
    ];

    public function products(){
        return $this->belongsToMany('App\Product');
    }

}
