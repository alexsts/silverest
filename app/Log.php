<?php

namespace App;

use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
       'key','description','type','log_group_id'
    ];
}
