<?php

// Localisation:
Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect']], function () {

    // Front:
    Route::group(['namespace' => 'Front'], function () {

        // Main:
        Route::get('/', 'FrontController@index')->name('root');
        Route::get('/404-page-not-found', 'FrontController@show404')->name('show404');
        Route::get('/warranty', 'FrontController@warranty')->name('warranty');
        Route::get('/delivery', 'FrontController@delivery')->name('delivery');
        Route::get('/about-us', 'FrontController@about_us')->name('about-us');
        Route::get('/contacts', 'FrontController@contacts')->name('contacts');

        // Catalogue:
        Route::group(['prefix' => 'catalogue'], function () {
            Route::get('/', 'CatalogueController@index')->name('catalogue');
            Route::post('/', 'CatalogueController@ajaxCatalogue')->name('catalogue.ajax');
            Route::get('/product/{id}', 'CatalogueController@product')->name('catalogue.product');
            Route::post('/product/preview', 'CatalogueController@ajaxPreviewProduct')->name('catalogue.product.preview.ajax');
        });

        // Account:
        Route::group(['prefix' => 'account', 'middleware' => 'role:superadministrator|administrator|editor|user', 'middleware_handling' => 'redirect'], function () {

            // Main:
            Route::get('/', 'AccountController@index')->name('account');
            Route::get('/edit', 'AccountController@editAccount')->name('account.edit');
            Route::post('/update', 'AccountController@updateAccount')->name('account.update');

            // Cart:
            Route::get('/cart', 'CartController@index')->name('account.cart');
            Route::post('/cart/quantity', 'CartController@ajaxQuantity')->name('account.cart.quantity.ajax');
            Route::post('/cart/add', 'CartController@add')->name('account.cart.add');
            Route::post('/cart/remove', 'CartController@remove')->name('account.cart.remove');

            // Wishlist:
            Route::get('/wishlist', 'WishlistController@index')->name('account.wishlist');
            Route::post('/wishlist/change', 'WishlistController@change')->name('account.wishlist.change');

            // Review:
            Route::post('/review/publish', 'ReviewController@publish')->name('account.review.publish');

            // Order:
            Route::get('/orders', 'OrderController@index')->name('account.orders');
            Route::get('/orders/{id}', 'OrderController@order')->name('account.order');
            Route::post('/order/cancel', 'OrderController@cancelOrder')->name('account.order.cancel');
            Route::post('/order/complete', 'OrderController@completeOrder')->name('account.order.complete');
            Route::get('/order/checkout', 'OrderController@checkoutOrder')->name('account.order.checkout');
            Route::get('/order/invoice/download/{id}', 'OrderController@downloadInvoice')->name('account.order.invoice.download');
            Route::post('/order/notes', 'OrderController@ajaxOrderNotes')->name('account.order.notes.ajax');
            Route::post('/order/notes/update', 'OrderController@updateOrderNotes')->name('account.order.notes.update');
        });

    });

    // Admin:
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'role:superadministrator|administrator|editor', 'middleware_handling' => 'redirect', 'middleware_params' => '/home',], function () {

        // Main:
        Route::get('/', 'AdminController@index')->name('admin');
        Route::get('/system/info', 'AdminController@info')->name('system.info');
        Route::get('/system/logs', 'AdminController@logs')->name('system.logs');

        // User:
        Route::resource('/users', 'UserController');
        Route::resource('/permissions', 'PermissionController');
        Route::resource('/roles', 'RoleController');

        // Product:
        Route::resource('/products', 'ProductController');
        Route::post('/products/visible', 'ProductController@productVisibleChange')->name('product.visible');
        Route::post('/products/quantity', 'ProductController@productQuantityChange')->name('product.quantity');

        // Review:
        Route::resource('/reviews', 'ReviewController');
        Route::post('/reviews/visible', 'ReviewController@reviewVisibleChange')->name('review.visible');

        // Order:
        Route::resource('/orders', 'OrderController');
        Route::post('/orders', 'OrderController@changeOrderStatus')->name('order.status.ajax');
        Route::post('/orders/update/tracking-number', 'OrderController@updateTrackingNumber')->name('order.tracking.number.update');
        Route::post('/order/viewed', 'OrderController@orderViewed')->name('order.viewed');

        // Extra:
        Route::resource('/categories', 'CategoryController');
        Route::resource('/subcategories', 'SubcategoryController');
        Route::resource('/materials', 'MaterialController');
        Route::resource('/styles', 'StyleController');
        Route::resource('/tags', 'TagController');
        Route::resource('/sets', 'SetController');
        Route::resource('/sizes', 'SizeController');
        Route::resource('/colors', 'ColorController');
        Route::resource('/gems', 'GemController');
        Route::resource('/photos', 'PhotoController');
        Route::resource('/customers', 'CustomerController');
        Route::resource('/statuses', 'StatusController');
    });

    // Authentication:
    Auth::routes();
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
});





