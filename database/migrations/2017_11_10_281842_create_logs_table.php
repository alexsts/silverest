<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('log_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
        });

        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('log_group_id')->unsigned()->index();
            $table->foreign('log_group_id')->references('id')->on('log_groups')->onDelete('cascade');
            $table->enum('type', ['info', 'error', 'success', 'warning'])->default('info');
            $table->string('key')->nullable();
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('gems', function (Blueprint $table) {
            Schema::dropIfExists('log_groups');
            Schema::dropIfExists('logs');
        });
    }
}
