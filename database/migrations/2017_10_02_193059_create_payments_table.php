<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->index();
            $table->string('braintree_id')->nullable();
            $table->enum('status', ['pending', 'accepted', 'declined', 'refunded', 'cancelled'])->default('pending');
            $table->string('method')->nullable();
            $table->string('card_type')->nullable();
            $table->string('card_last_digits')->nullable();
            $table->float('shipping_total', 8, 2)->unsigned();
            $table->float('cart_total', 8, 2)->unsigned();
            $table->float('subtotal', 8, 2)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
