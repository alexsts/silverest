<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->string('code')->nullable();
            $table->float('price',8,2)->unsigned();
            $table->float('weight',8,2)->unsigned();
            $table->float('discount_rate',8,2)->unsigned();
            $table->date('discount_end_date')->nullable();
            $table->integer('material_id')->unsigned()->index();
            $table->integer('quantity')->unsigned();
            $table->integer('sold_number')->unsigned();
            $table->integer('views_number')->unsigned();
            $table->boolean('in_stock')->default(false);
            $table->boolean('is_visible')->default(false);
            $table->boolean('is_featured')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_related');
    }
}
