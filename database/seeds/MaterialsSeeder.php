<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaterialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Truncating Materials Table');
        $this->truncateStyles();

        $materials_arr = array(
            array('серебро','silver','hõbe'),
            array('золото','gold','kuld'),
            array('белое золото','white gold','valge kuld')
        );

        foreach ($materials_arr as $material_name) {
            $material = new \App\Material();
            $material->setTranslation('name', 'ru', $material_name[0]);
            $material->setTranslation('name', 'en', $material_name[1]);
            $material->setTranslation('name', 'et', $material_name[2]);
            $material->save();
        }

        $this->command->info('Creating Materials Table');
    }

    public function truncateStyles()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        \App\Material::truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
