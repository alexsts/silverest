<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Truncating Category and Subcategory Tables');
        $this->truncateCategories();

        $levels = array(

            '1' => array(
                array('Кольца', 'Rings', 'Sõrmused', 'rings'),
                array('Обручальные', 'Wedding', 'Pulmasõrmused', 'wedding'),
                array('Помолвочные', 'Engagement', 'Kihlasõrmused', 'engagement'),
                array('Трансформеры', 'Transformers', 'Transformerid', 'transformers'),
                array('Печатки', 'Stamps', 'Klotserid', 'stamps')
            ),
            '2' => array(
                array('Серьги', 'Earrings', 'Kõrvarõngad', 'earrings'),
                array('Пусеты', 'Pouches', 'Poussette', 'pouches'),
                array('Длинные', 'Long', 'Pikad', 'long'),
                array('Конго', 'Congo', 'Kongo', 'congo'),
                array('Зажимы', 'Clamps', 'Klambrid', 'clamps'),
                array('Одиночные', 'Single', 'Üksik', 'single')
            ),
            '3' => array(
                array('Подвески', 'Pendants', 'Ripatsid', 'pendants'),
                array('Кресты', 'Crosses', 'Ristid', 'crosses'),
                array('Шармы', 'Charms', 'Karamellid', 'charms'),
                array('Иконки', 'Icons', 'Ikoonid', 'icons'),
                array('Знаки зодиака', 'Zodiac Signs', 'Sodiaagi Märgid', 'zodiac-signs')
            ),
            '4' => array(
                array('Броши', 'Brooches', 'Prossid', 'brooches')
            ),
            '5' => array(
                array('Браслеты', 'Bracelets', 'Käevõrud', 'bracelets'),
                array('Жёсткие', 'Hard', 'Kõva', 'hard'),
                array('Слейв-браслеты', 'Slave Bracelets', 'Slavekärud', 'slave-bracelets'),
                array('Браслет-цепь', 'Chain Bracelet', 'Kett-käevõru', 'chain-bracelet')
            ),
            '6' => array(
                array('Цепи', 'Chains', 'Ketid', 'chains'),
                array('Колье', 'Necklace', 'Kaelakee', 'necklace'),
                array('Зажимы для галстука', 'Clips for tie', 'Lipsuklambrid', 'clips-for-tie')
            ),
            '7' => array(
                array('Запонки', 'Cufflinks', 'Mansetinööbid', 'cufflinks'),
            ),
            '8' => array(
                array('Пирсинг', 'Piercing', 'Piercing', 'piercing'),
                array('В пупок', 'In the navel', 'Nabarõngad', 'in-the-navel'),
                array('В губу', 'In the lip', 'Huule-piercing', 'in-the-lip'),
                array('В нос', 'Into the nose', 'Nina-piercing', 'into-the-nose'),
                array('В бровь', 'In the eyebrow', 'Kulmu-piercing', 'in-the-eyebrow')
            ),
            '9' => array(
                array('Пишущие принадлежности', 'Writing accessories', 'Kirjutusvahendid', 'writing-accessories'),
            ),
            '10' => array(
                array('Сувенирная продукция', 'Souvenir production', 'Suveniiritooded', 'souvenir-production'),
            )
        );

        foreach ($levels as $level => $level2) {

            $category = new \App\Category();

            for ($i = 0; $i < sizeof($level2); $i++) {
                if ($i == 0) {
                    $category->setTranslation('name', 'ru', $level2[$i][0]);
                    $category->setTranslation('name', 'en', $level2[$i][1]);
                    $category->setTranslation('name', 'et', $level2[$i][2]);
                    $category->code =  $level2[$i][3];
                    $category->save();
                } else {
                    $subcategory = new \App\Subcategory();
                    $subcategory->category_id = $category->id;
                    $subcategory->setTranslation('name', 'ru', $level2[$i][0]);
                    $subcategory->setTranslation('name', 'en', $level2[$i][1]);
                    $subcategory->setTranslation('name', 'et', $level2[$i][2]);
                    $subcategory->code =  $level2[$i][3];
                    $subcategory->save();
                }
            }
        }

        $this->command->info('Creating Category and Subcategory Tables');

    }

    public function truncateCategories()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        \App\Category::truncate();
        \App\Subcategory::truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
