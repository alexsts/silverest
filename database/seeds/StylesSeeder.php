<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StylesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Truncating Styles Table');
        $this->truncateStyles();

        $styles_arr = array(
            array('Любовь', 'Love', 'Armastus','love'),
            array('Ажур', 'Ajour', 'Ajour','ajour'),
            array('Ангел', 'Angel', 'Ingel','angel'),
            array('Бабочка', 'Butterfly', 'Liblikas','butterfly'),
            array('Бантики', 'Bows', 'Lehvid','bows'),
            array('Геометрия', 'Geometry', 'Geomeetria','geometry'),
            array('Животные', 'Animals', 'Loomad','animals'),
            array('Классика', 'Classics', 'Klassika','classics'),
            array('Ключи', 'Keys', 'Võtmed','keys'),
            array('Корона', 'Crown', 'Kroon','crown'),
            array('Кошка', 'Cat', 'Kass','cat'),
            array('Лист', 'Leaf', 'Leht','leaf'),
            array('Насекомые', 'Insects', 'Putukad','insects'),
            array('Цветы', 'Flowers', 'Lilled','flowers'),
            array('Звезда', 'Star', 'Täht','star'),
            array('Подкова', 'Horseshoe', 'Hobuseraud','horseshoe'),
            array('Птицы', 'Birds', 'Linnud','birds'),
            array('Путешествия', 'Travels', 'Reisimine','travels'),
            array('Семья', 'Family', 'Perekond','family'),
            array('Талисманы', 'Talismans', 'Talismanid','talismans')
        );

        foreach ($styles_arr as $style_name) {
            $style = new \App\Style();
            $style->setTranslation('name', 'ru', $style_name[0]);
            $style->setTranslation('name', 'en', $style_name[1]);
            $style->setTranslation('name', 'et', $style_name[2]);
            $style->code = $style_name[3];
            $style->save();
        }

        $this->command->info('Creating Styles Table');
    }

    public function truncateStyles()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        \App\Style::truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
