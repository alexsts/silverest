<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        //create faker object:
        $faker = Faker\Factory::create();

        //clear the table:
        $this->command->info('Truncating Products Table');
        $this->truncateProductTables();

        foreach (range(1, 1000) as $index) {
            // Create a new product
            $product = \App\Product::create([
                'code' => strtoupper($faker->randomLetter()) . $faker->numberBetween($min = 1000, $max = 9000),
                'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 5, $max = 500),
                'weight' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 50),
                'discount_rate' => $faker->numberBetween($min = 0, $max = 90),
                'quantity' => $faker->numberBetween($min = 0, $max = 100),
                'material_id' => $faker->numberBetween($min = 1, $max = 3),
                'views_number' => $faker->numberBetween($min = 0, $max = 10000),
                'sold_number' => $faker->numberBetween($min = 0, $max = 10),
                'is_visible' => $faker->numberBetween($min = 0, $max = 1)
            ]);

            $product->setTranslation('name', 'en', ('en ' . ucfirst($faker->word(10)) . ' ' . ucfirst($faker->word(9))));
            $product->setTranslation('name', 'ru', ('рус ' . ucfirst($faker->word(10)) . ' ' . ucfirst($faker->word(9))));
            $product->setTranslation('name', 'et', ('est ' . ucfirst($faker->word(10)) . ' ' . ucfirst($faker->word(9))));
            $product->save();

            //attach subcategories:
            $subcategories = array();
            $categories = array();

            for ($i = 0; $i <= 3; $i++) {
                array_push($categories, mt_rand(1, \App\Category::all()->count()));
            }

            for ($i = 0; $i <= 3; $i++) {
                array_push($subcategories, mt_rand(1, \App\Subcategory::all()->count()));
            }

            $product->subcategories()->attach(array_unique($subcategories));
            $product->categories()->attach(array_unique($categories));

            //attach styles:
            $styles = array();
            for ($i = 0; $i <= 6; $i++) {
                array_push($styles, mt_rand(1, \App\Style::all()->count()));
            }
            $product->styles()->attach(array_unique($styles));

            //attach sizes:
            $sizes = array();
            for ($i = 0; $i <= 5; $i++) {
                array_push($sizes, mt_rand(1, \App\Size::all()->count()));
            }
            $product->sizes()->attach(array_unique($sizes));

            //attach tags:
            $tags = array();
            for ($i = 0; $i <= 12; $i++) {
                array_push($tags, mt_rand(1, \App\Tag::all()->count()));
            }
            $product->tags()->attach(array_unique($tags));

            //attach colors:
            $colors = array();
            for ($i = 0; $i <= 4; $i++) {
                array_push($colors, mt_rand(1, \App\Color::all()->count()));
            }
            $product->colors()->attach(array_unique($colors));

            //attach photos:
            $photos = array();
            for ($i = 0; $i <= 4; $i++) {
                array_push($photos, mt_rand(1, \App\Photo::all()->count()));
            }
            $product->photos()->attach(array_unique($photos));

            //attach gems:
            $gems = array();
            for ($i = 0; $i <= 1; $i++) {
                array_push($gems, mt_rand(1, \App\Gem::all()->count()));
            }
            $product->gems()->attach(array_unique($gems));

        }
        $this->command->info('Creating Products Table');
    }

    public function truncateProductTables()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        DB::table('product_size')->truncate();
        DB::table('product_style')->truncate();
        DB::table('product_tag')->truncate();
        DB::table('photo_product')->truncate();
        DB::table('color_product')->truncate();
        DB::table('category_product')->truncate();
        DB::table('product_subcategory')->truncate();
        DB::table('gem_product')->truncate();

        \App\Product::truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
